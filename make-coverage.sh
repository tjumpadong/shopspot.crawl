#!/bin/bash

rm -rf coverage
mkdir coverage

# make unit
mocha ./test/units/*.js --reporter xunit > ./coverage/xunit.xml

# make coverage
istanbul cover _mocha ./test/units/*.js
istanbul report cobertura