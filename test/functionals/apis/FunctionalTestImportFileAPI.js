var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');

describe('FunctionalTestImportFileAPI', function () {
	this.timeout(200000);
	before(function (done) {
		helper.setupDatabase(function (error) {
			if (error) {
				log.error('setup database error');
			}
			else {
				log.info('setup database success');
				done();
			}
		});
	});
/*
	describe('#importFileCSV', function () {
		this.timeout(10000);
		var csvPath = path.join(__dirname, '..', '..', 'resources', 'csvForTest.csv');
		it('should success', function (done) {
			var url = rootUrl + '/import/csv';
      var options = {
        data: {
          userId: '52d3b348a26835195d000001',
          title: 'test group name',
          description: 'test',
          files: {
            path: mockRequest.file(csvPath, 'csvFile.csv')//fs.createReadStream(csvPath).pipe(fs.createWriteStream('tmpCsv.csv'))  
          }
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;

					var stuffs = data.output;
					stuffs.length.should.equal(1);
					
					done();
				}
			]);
		});

		it('should success twice', function (done) {
			var url = rootUrl + '/import/csv';
		
      var options = {
        data: {
          userId: '52d3b348a26835195d000001',
          title: 'test group name',
          description: 'test',
          files: {
            path: mockRequest.file(csvPath, 'csvFile.csv')
          }
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var stuff = data.output;
					
					done();
				}
			]);
		});
	});

	describe('#ImportFileParserMap', function () {
		it('should success', function (done) {
			var url = rootUrl + '/parsers/map';

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, {}, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var mapping = data.output;
					
					done();
				}
			]);
		});
	});*/
	
	describe('#csv', function () {
		var csvPath = path.join(__dirname, '..', '..', 'resources', 'strawberrynet.csv');
		it('should create tmpStuff success', function (done) {
			var url = rootUrl + '/import/csv';
			var options = {
				data: {
          site: 'strawberrynet',
          files: {
            path: mockRequest.file(csvPath, 'strawberrynet.csv')//fs.createReadStream(csvPath).pipe(fs.createWriteStream('tmpCsv.csv'))  
          }
        }
			};

			async.waterfall([
				function callService (next) {
          mockRequest.postWithFile(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					log.info('data csv : '+util.inspect(data));
					data.action.should.be.true;
					var tmpStuffs = data.output;
					log.info('tmpStuffs csv : '+util.inspect(tmpStuffs));
					done();
				}
			]);
		});
	});

	describe('#xml', function () {
		it('should create tmpStuff success', function (done) {
			var url = rootUrl + '/import/xml';
			var options = {
				data: {
          site: 'zalora',
          uri: 'https://s3-ap-southeast-1.amazonaws.com/shopspot-test/test.crawl/fixture.xml'
        }
			};

			async.waterfall([
				function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					log.info('data xml : '+util.inspect(data));
					data.action.should.be.true;
					var tmpStuffs = data.output;
					log.info('tmpStuffs : '+util.inspect(tmpStuffs));
					done();
				}
			]);
		});
	});
});