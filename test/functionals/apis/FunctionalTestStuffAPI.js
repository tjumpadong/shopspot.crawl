var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = 'http://127.0.0.1:9002/crawl';//helper.config.host;
var log = require('winston');

describe('FunctionalTestStuffAPI', function () {
	this.timeout(10000);
	before(function (done) {
		helper.setupDatabase(function (error) {
			if (error) {
				log.error('setup database error');
			}
			else {
        setTimeout(function() {
          log.info('setup database success');
          done();  
        }, 1000);
        
			}
		});
	});

  describe('#syncTmpToStuff', function () {
    it('should create stuff', function (done) {
      var url = rootUrl + '/stuff/sync';
      var options = {
        data: {
          site: 'strawberrynet'
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data, next) {
          log.debug('test done')
          data.action.should.be.true;
          next();
        },
        function callCheckService (next) {
          var checkUrl = rootUrl + '/stuff/site'
          var checkOptions = {
            query: {
              site: 'strawberrynet',
              page: 0,
              limit: 10
            }
          };

          mockRequest.get(checkUrl, checkOptions, function (response, data) {
            next(null, data);
          });
        },
        function checkTestcase (data) {
          data.action.should.be.true;
          var stuffs = data.output;
          data.output.length.should.be.equal(3);

          for (var key in stuffs) {
            var stuff = stuffs[key];

            stuff.should.have.property('images');
            stuff.should.have.property('price');
            stuff.should.have.property('description');
            stuff.should.have.property('market');
            stuff.should.have.property('owner');
            stuff.should.have.property('categories');
          };

          done();
        }
      ]);
    })
  });

  describe('#updateProductionStuffId', function () {
    it('should success', function (done) {
      var url = rootUrl + '/stuff/update/productionId';
      var options = {
        data: {
          stuffId: '545b77010a2e60420b000003',
          productionStuffId: 'productionId'
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          data.action.should.be.true;
          var stuff = data.output;

          stuff._id.should.equal(options.data.stuffId);
          stuff.productionStuffId.should.equal(options.data.productionStuffId);
          stuff.status.should.equal('published');
          done();
        }
      ]);
    });
  });

  describe('#findFromSite', function () {
    it('should failed', function (done) {
      var url = rootUrl + '/stuff/site';
      var options = {
        query: {}
      };

      async.waterfall([
        function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          data.action.should.be.false;
          done();
        }
      ]);
    });

    it('should success', function (done) {
      var url = rootUrl + '/stuff/site';
      var options = {
        query: {
          site: 'zalora',
          page: 0,
          limit: 10
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          data.action.should.be.true;
          var stuffs = data.output;
          data.total.should.be.equal(3);

          for (var key in stuffs) {
            var stuff = stuffs[key];
            stuff.owner.should.equal('54080e9d257926952e00004a');
          }
          done();
        }
      ]);
    });
  });

  describe('#findFromId', function () {
    it('should success', function (done) {
      var id = '545b77010a2e60420b000001';
      var url = rootUrl + '/stuff/' + id;
      var options = {
        query: {}
      };

      async.waterfall([
        function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          data.action.should.be.true;
          done();
        }
      ]);
    });
  });

  describe('#updateFromRevision', function () {
    it('should success', function (done) {
      var url = rootUrl + '/stuff';
      var options = {
        data: {
          _id: '545b77010a2e60420b000001',
          price: 500
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.post(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          data.action.should.be.true;
          var stuff = data.output;

          stuff.price.should.be.equal('500');

          done();
        }
      ]);
    });
  });
});