var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var Datasources = require('../../../server/modules/datasources/DatasourceFactory');
var XMLDatasource = require('../../../server/modules/datasources/XMLDatasource');
var CSVDatasource = require('../../../server/modules/datasources/CSVDatasource');
var HTMLDatasource = require('../../../server/modules/datasources/HTMLDatasource');
var log = require('winston');
var test = {};

describe('FunctionalTestDatasourceFactory', function () {

	before(function (done) {
		test.datasources = new Datasources();
		done();
	});

	describe('#instance', function () {
		it('should return xml datasource', function (done) {
			var datasource = test.datasources.createXMLDatasource();

			datasource.should.instanceOf(XMLDatasource);
			done();

		});

		it('should return csv datasource', function (done) {
			var datasource = test.datasources.createCSVDatasource();

			datasource.should.instanceOf(CSVDatasource);
			done();

		});

		it('should return html datasource', function (done) {
			var datasource = test.datasources.createHTMLDatasource();

			datasource.should.instanceOf(HTMLDatasource);
			done();

		});
	});
});