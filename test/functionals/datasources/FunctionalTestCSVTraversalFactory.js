var should = require('should'),
    util = require('util'),
    fs = require('fs'),
    async = require('async'),
    path = require('path');

var CSVTraversalFactory = require('../../../server/modules/datasources/traversals/CSVTraversalFactory');
var StrawberrynetCSVTraversal = require('../../../server/modules/datasources/traversals/StrawberrynetCSVTraversal');
var log = require('winston');
var test;

describe('FunctionalTestCSVTraversalFactory', function () {
  
  before(function (done) {

    test = {
      csvTraversalFactory: new CSVTraversalFactory()
    };

    done();
  });

  describe('#createTraversal', function () {
    it('should success', function (done) {
      var site = 'strawberrynet';

      var traversal = test.csvTraversalFactory.createTraversal(site);
      traversal.should.instanceOf(StrawberrynetCSVTraversal).and.have.property('CSV_MAPPING');
      done();
    });

  });

});