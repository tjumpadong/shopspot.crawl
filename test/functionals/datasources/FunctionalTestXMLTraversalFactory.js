var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var XMLTraversalFactory = require('../../../server/modules/datasources/traversals/XMLTraversalFactory');
var ZaloraXMLTraversal = require('../../../server/modules/datasources/traversals/ZaloraXMLTraversal');
var log = require('winston');
var test;

describe('FunctionalTestXMLTraversalFactory', function () {
	
	before(function (done) {

		test = {
			xmlTraversalFactory: new XMLTraversalFactory()
		};

		done();
	});

	describe('#createTraversal', function () {
		it('should success', function (done) {
			var site = 'zalora';

			var traversal = test.xmlTraversalFactory.createTraversal(site);
			
			traversal.should.instanceOf(ZaloraXMLTraversal).and.have.property('uri');
			done();
		});

	});

});