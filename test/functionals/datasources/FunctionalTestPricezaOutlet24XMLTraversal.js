var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var PricezaOutlet24XMLTraversal = require('../../../server/modules/datasources/traversals/PricezaOutlet24XMLTraversal');

var log = require('winston');
var test;


describe('FunctionalTestPricezaOutlet24XMLTraversal', function () {
	this.timeout(10000);
	before(function (done) {
		var fixtureRowDatas = [{ 
			id: 4360405,
			name: 'VS Sassoon 25MM Tourmaline Ceramic Straightener',
			price: '1,850',
			description: 'เครื่องหนีบผมตรง VS Sassoon รุ่น VS2010PITผมตรงสวยง่ายๆ ทำได้เองที่บ้าน ไม่ต้องเสียเวลาไปร้านทำผม พกพาสะดวกสวยได้ทุกที ใช้เวลาเพียง 5 นาที เท่านี้คุณก็พร้อมออกจากบ้านได้อย่างมั่นใจแผ่นให้ความร้อนขนาด 25 มม. อุณหภูมิสูงสุด 230 องศาเซลเซียสร้อนเร็วภายใน 60 วินาทีเคลือบทัวมาลีน และเซรามิค-นาโนซิลเวอร์ปรับอุณหภูมิได้ 25 ระดับสวิชต์ล็อคตัวเครื่องเพื่อปิดแผ่นหนีบเข้าด้วยกันขนาดผลิตภัณฑ์ กว้าง x สูง (ซม.) 7.5 x 32รับประกัน 2 ปี',
			category_id: 130411,
			category_name: 'แปรงหวีผม',
			url: 'http://www.priceza.com/r/redirect?id=4360405&delay=true&utm_source=The+Outlet24&utm_campaign=ShopSpotItem&utm_medium=VS+Sassoon+25MM+Tourmaline+Ceramic+Straightener',
			image: 'http://img.priceza.com/img/product/2043/2043-20140613181506-443983.jpg',
			merchant_name: 'The Outlet24'
		},
		{
			id: 4360407,
			name: 'VS Sassoon 32MM Tourmaline Ceramic 2-IN-1 Straightener/Curling Iron',
			price: '1,950',
			description: 'เครื่องหนีบผมตรง/ทำลอนผม 2 ใน 1 VS Sassoon รุ่น VSI3270PITแกนม้วนสามารถแยกออกได้สำหรับแต่งทรงผมแบบหนีบตรงและทำลอนผมแกนความร้อนขนาด 32 มม. เคลือบทัวมาลีน และเซรามิคร้อนเร็วภายใน 60 วินาที ปรับอุณหภูมิได้ 25 ระดับระบบปิดเครื่องอัตโนมัติขนาดผลิตภัณฑ์ กว้าง x สูง (ซม.) 7.5 x 34รับประกัน 2 ปี',
			category_id: 130411,
			category_name: 'แปรงหวีผม',
			url: 'http://www.priceza.com/r/redirect?id=4360407&delay=true&utm_source=The+Outlet24&utm_campaign=ShopSpotItem&utm_medium=VS+Sassoon+32MM+Tourmaline+Ceramic+2-IN-1+Straightener%2FCurling+Iron',
			image: 'http://img.priceza.com/img/product/2043/2043-20140613181507-70551.jpg',
			merchant_name: 'The Outlet24'
		}];

		test = {
			fixture: fixtureRowDatas,
			traversal: new PricezaOutlet24XMLTraversal()
		}
		done();
	});

	describe('#mapping', function () {
		it('should success', function (done) {
			var stuffs = test.traversal.mapping(test.fixture);
			log.info('result : '+util.inspect(stuffs));
			stuffs.length.should.equal(2);

			for (var key in stuffs) {
				var stuff = stuffs[key];
				stuff.should.have.property('path1').and.not.equal(undefined);
				stuff.should.have.property('link').and.not.equal(undefined);
				stuff.should.have.property('name').and.not.equal(undefined);
				stuff.should.have.property('description').and.not.equal(undefined);
				stuff.should.have.property('price').and.not.equal(undefined);
				stuff.should.have.property('mainCategory').and.not.equal(undefined);
				stuff.should.have.property('sku').and.not.equal(undefined);
			}
			done();
		
		});
	});
});