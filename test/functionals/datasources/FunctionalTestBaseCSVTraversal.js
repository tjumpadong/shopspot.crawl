var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ErrorObject = require('../../../server/modules/Error').ErrorObject,
    ErrorCode =  require('../../../server/modules/Error').ErrorCode,
    ErrorDomain = require('../../../server/modules/Error').ErrorDomain;

var BaseCSVTraversal = require('../../../server/modules/datasources/traversals/BaseCSVTraversal');
var log = require('winston');
var test = {};

describe('FunctionalTestBaseCSVTraversal', function () {
	this.timeout(10000);
	before(function (done) {
		
		var stuffFixtures = {
			stuff1: {
				'Product image path1': 'path1',
				'Product image path2': 'path2',
				'Product image path3': 'path3',
				'Product link url': 'link',
				'Product name': 'name',
				'Product description': 'description',
				'Price': 'price',
				'Sale price': 'salePrice',
				'% of sale': 'discount',
				'Product main category': 'mainCategory',
				'Product sub category': 'subCategory',
				'Product sub category2': 'subCategory2',
				'Tag': 'tags',
				'sku': 'sku'
			},
			stuff2: {
				'product image path1': 'path1',
				'product image path2': 'path2',
				'Product name': 'name',
				'Product description': 'description',
				'Price': 'price',
				'aaa': 'salePrice',
				'% of sale': 'discount',
				'Product main category': 'mainCategory',
				'Product sub category': 'subCategory',
				'Product sub category2': 'subCategory2',
				'Tag': 'tags',
				'sku': 'sku'
			}
		}

		test = {
			traversal: new BaseCSVTraversal(),
			fixtures: stuffFixtures
		}

		done();
	});

	describe('#mapping', function () {
		it('should success', function (done) {
			var stuffs = [ test.fixtures.stuff1, test.fixtures.stuff2 ];
			var object = test.traversal.mapping(stuffs);
			
			for (var key in object) {
				var stuff = object[key];
				stuff.should.have.property('path1');
			}

			done();	
		})
		
	});

	describe('#validateHeader', function () {
		it('should invalid header', function (done) {
			var stuff = test.fixtures.stuff2;
			test.traversal.validateHeader(stuff, function (error, success) {
				should.exists(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_HEADER);

				done();
			});
		});

		it('should success', function (done) {
			var stuff = test.fixtures.stuff1;
			test.traversal.validateHeader(stuff, function (error, success) {
				should.not.exists(error);
				done();
			});
		});
	});
});