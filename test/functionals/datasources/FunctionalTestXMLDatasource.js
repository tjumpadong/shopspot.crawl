var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ErrorObject = require('../../../server/modules/Error').ErrorObject,
    ErrorCode =  require('../../../server/modules/Error').ErrorCode,
    ErrorDomain = require('../../../server/modules/Error').ErrorDomain;

var XMLDatasource = require('../../../server/modules/datasources/XMLDatasource'),
		XMLTraversalFactory = require('../../../server/modules/datasources/traversals/XMLTraversalFactory');
var log = require('winston');
var test;

describe('FunctionalTestXMLDatasource', function () {
	this.timeout(20000);
	before(function (done) {
    var site = 'zalora';
    var traversal = new XMLTraversalFactory().createTraversal(site);
    var uri = '';

		test = {
			uri: 'https://s3-ap-southeast-1.amazonaws.com/shopspot-test/test.crawl/fixture.xml',
			xmlDatasource: new XMLDatasource(traversal, uri)
		}
		done();
	});

	describe('#_fromURI', function () {
		it('should success', function (done) {

			test.xmlDatasource._fromURI(test.uri, function (error, xmlFile) {
				test.xmlFile = xmlFile;

				should.not.exists(error);
				done();
			});
		});
	});

	describe('#_parseXML', function () {
		it('should success', function (done) {
			test.xmlDatasource._parse(test.xmlFile, function (error, rowDatas) {
				should.not.exists(error);
				should.exists(rowDatas);
				//log.info('rowDatas : '+util.inspect(rowDatas));
				rowDatas.length.should.equal(2);

				done();
			});
		});
	});

	describe('#convert', function () {
		it('should success', function (done) {
			var site = 'zalora';

			var traversal = new XMLTraversalFactory().createTraversal(site);
			var datasource = new XMLDatasource(traversal, test.uri);

			datasource.convert(function (error, stuffs) {
				should.not.exists(error);
				stuffs.length.should.equal(2);
				done();
			});
		});
	})

});