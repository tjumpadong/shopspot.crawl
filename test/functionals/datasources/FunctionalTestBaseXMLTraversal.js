var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ErrorObject = require('../../../server/modules/Error').ErrorObject,
    ErrorCode =  require('../../../server/modules/Error').ErrorCode,
    ErrorDomain = require('../../../server/modules/Error').ErrorDomain;

var BaseXMLTraversal = require('../../../server/modules/datasources/traversals/BaseXMLTraversal');
var log = require('winston');
var test = {};

describe('FunctionalTestBaseXMLTraversal', function () {
	this.timeout(10000);
	before(function (done) {
		var uri = 'https://s3-ap-southeast-1.amazonaws.com/shopspot-test/test.crawl/fixture.xml';
		var traversal = new BaseXMLTraversal(uri);

		test.traversal = traversal;

		done();
	});

	describe('#mapping', function () {
		it('should success', function (done) {
			test.traversal.mapping({}, function (error, stuffs) {
				should.exists(error);
				//log.info('rowDatas : '+util.inspect(error));
				error.domain.should.equal(ErrorDomain.DOMAIN_TRAVERSAL);
				error.code.should.equal(ErrorCode.TRAVERSAL_NOT_DEFINE_MAP);

				done();
			});
		});
	});
});