var should = require('should'),
    util = require('util'),
    fs = require('fs'),
    async = require('async'),
    path = require('path');

var StrawberrynetCSVTraversal = require('../../../server/modules/datasources/traversals/StrawberrynetCSVTraversal');

var log = require('winston');
var test;


describe('FunctionalTestStrawberrynetTraversal', function () {
  this.timeout(10000);
  before(function (done) {
    var fixtureRowDatas = [{ 
      'sku': 'AL615AA70CSBTH',
      'product name': 'Alang pink',
      'price': '1250.00',
      'sale price': '0.00',
      '% of sale': '100',
      'product description': 'test description',
      'product link url': 'http://th.strawberrynet.com/mens-fragrances/ralph-lauren/polo-green-eau-de-toilette-spray/7589/?region=th&trackid=7098500001',
      'product main category': 'Men\'s Fragrance',
      'product image 1': 'http://img4.strawberrynetmedia.com/images/products/00758930505.jpg',
      'tag': 'tag1 tag2'
    }];

    test = {
      fixture: fixtureRowDatas,
      traversal: new StrawberrynetCSVTraversal()
    }
    done();
  });

  describe('#mapping', function () {
    it('should success', function (done) {
      var stuffs = test.traversal.mapping(test.fixture);
      //log.info('result : '+util.inspect(stuffs));
      stuffs.length.should.equal(1);

      for (var key in stuffs) {
        var stuff = stuffs[key];
        console.log(stuff);
        stuff.should.have.property('path1').and.not.equal(undefined);
        stuff.should.not.have.property('path2');
        stuff.should.have.property('link').and.not.equal(undefined);
        stuff.should.have.property('name').and.not.equal(undefined);
        stuff.should.have.property('description').and.not.equal(undefined);
        stuff.should.have.property('price').and.not.equal(undefined);
        stuff.should.have.property('mainCategory').and.not.equal(undefined);
        stuff.should.have.property('sku').and.not.equal(undefined);
        stuff.should.have.property('tags').and.not.equal(undefined);
      }
      done();
    
    });
  });
});