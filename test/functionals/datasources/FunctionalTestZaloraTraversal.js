var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ZaloraXMLTraversal = require('../../../server/modules/datasources/traversals/ZaloraXMLTraversal');

var log = require('winston');
var test;


describe('FunctionalTestZaloraTraversal', function () {
	this.timeout(10000);
	before(function (done) {
		var fixtureRowDatas = [{ 
			Product_Id: 'AL615AA70CSBTH',
	    Title: 'Alang pink',
	    Price: '1250.00',
	    Sale_Price: '',
	    Brand: 'ALANGIF',
	    URL: 'http://zalorasea.go2cloud.org/aff_c?offer_id=14&aff_id=1079&file_id=3579&url=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F276259003%3B103357794%3Bi%3Fhttp%3A%2F%2Fmarketplace.zalora.co.th%2FAlang-pink-142429.html%3Fwt_af%3Dth.Affiliate.HasOffers.%7Baffiliate_id%7D.ALANGIF.ProductFeed%26utm_source%3DHasOffers%26utm_medium%3DAffiliate%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3DProductFeed',
	    Category: 'ผู้หญิง>เสื้อผ้า>เสื้อเบลาส์',
	    Image_URL_1: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-1-product.jpg',
	    Image_URL_2: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-2-product.jpg',
	    Image_URL_3: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-3-product.jpg',
	    Image_URL_4: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-4-product.jpg' 
	  },
	  { 
	  	Product_Id: 'HA517AC75GCITH',
	    Title: 'Girl Bucket Hat',
	    Price: '490.00',
	    Sale_Price: '',
	    Brand: 'HAT GODOWN',
	    URL: 'http://zalorasea.go2cloud.org/aff_c?offer_id=14&aff_id=1079&file_id=3579&url=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F276259003%3B103357794%3Bi%3Fhttp%3A%2F%2Fmarketplace.zalora.co.th%2FGirl-Bucket-Hat-144724.html%3Fwt_af%3Dth.Affiliate.HasOffers.%7Baffiliate_id%7D.HAT+GODOWN.ProductFeed%26utm_source%3DHasOffers%26utm_medium%3DAffiliate%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3DProductFeed',
	    Category: 'ผู้หญิง>เครื่องประดับ>หมวก',
	    Image_URL_1: 'http://static-origin.zalora.co.th/p/Girl-Bucket-Hat-427441-1-product.jpg',
	    Image_URL_2: 'http://static-origin.zalora.co.th/p/Girl-Bucket-Hat-427441-2-product.jpg',
	    Image_URL_3: '',
	    Image_URL_4: '' 
		}];

		test = {
			fixture: fixtureRowDatas,
			traversal: new ZaloraXMLTraversal()
		}
		done();
	});

	describe('#mapping', function () {
		it('should success', function (done) {
			var stuffs = test.traversal.mapping(test.fixture);
			//log.info('result : '+util.inspect(stuffs));
			stuffs.length.should.equal(2);

			for (var key in stuffs) {
				var stuff = stuffs[key];
				stuff.should.have.property('path1').and.not.equal(undefined);
				stuff.should.have.property('path2').and.not.equal(undefined);
				stuff.should.have.property('link').and.not.equal(undefined);
				stuff.should.have.property('name').and.not.equal(undefined);
				stuff.should.have.property('description').and.not.equal(undefined);
				stuff.should.have.property('price').and.not.equal(undefined);
				stuff.should.have.property('mainCategory').and.not.equal(undefined);
				stuff.should.have.property('sku').and.not.equal(undefined);
				stuff.should.have.property('tags').and.not.equal(undefined);
			}
			done();
		
		});
	});
});