var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ErrorObject = require('../../../server/modules/Error').ErrorObject,
    ErrorCode =  require('../../../server/modules/Error').ErrorCode,
    ErrorDomain = require('../../../server/modules/Error').ErrorDomain;

var CSVDatasource = require('../../../server/modules/datasources/CSVDatasource');
var BaseCSVTraversal = require('../../../server/modules/datasources/traversals/BaseCSVTraversal');
var log = require('winston');
var test;

describe('FunctionalTestCSVDatasource', function () {
	this.timeout(20000);
	before(function (done) {
		test = {
			csvFilePath: path.join(__dirname, '..', '..', 'resources', 'csvForTest.csv'),
			traversal: new BaseCSVTraversal()
		};

		done();
	});

	describe('#convert', function () {
		it('should failed because of no csv path', function (done) {
      var datasource = new CSVDatasource(test.traversal, '');

      datasource.convert(function (error, stuffs) {
        should.exists(error);

        done();
      });
    });

    it('should success', function (done) {
			var datasource = new CSVDatasource(test.traversal, test.csvFilePath);
			
			datasource.convert(function (error, stuffs) {
				should.not.exists(error);
				stuffs.length.should.equal(1);
				for (var key in stuffs) {
					var stuff = stuffs[key];
					stuff.should.have.property('mainCategory').and.equal('Women')
				}
				
				done();
			});
		});
	});

});