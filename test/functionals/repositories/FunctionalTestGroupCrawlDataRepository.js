var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');
var CrawlDataRepository = require('../../../server/repositories/GroupCrawlDataRepository');

var repository = null,
		timeout = 1000;

describe('FunctionalTestCrawlDataRepository', function () {
	var offset = {
		page: 0,
		limit: 3
	}

	before(function (done) {
		helper.setupDatabase(function (error, client) {
			if (error) {
				log.error('setup database error');
			}
			else {
				repository = new CrawlDataRepository(client);
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#findFromUserId', function () {
		it('should not return groupCrawlData', function (done) {
			var userId = 'test';
			repository.findFromUserId(userId, offset, function (error, total, groups) {
				should.not.exist(error);
				should.exist(groups);
				total.should.equal(0);

				done();
			});
		});

		it('should return groupCrawlData of userId', function (done) {
			var userId = '4f48e3f18c47fd650700000a';
			repository.findFromUserId(userId, offset, function (error, total, groups) {
				should.not.exist(error);
				should.exist(groups);
				total.should.equal(2);

				for(var key in groups) {
					var group = groups[key];
					group.userId.should.equal(userId);
				}

				done();
			});
		});

		it('should return all groupCrawlData ', function (done) {
			var userId = '';
			repository.findFromUserId(userId, offset, function (error, total, groups) {
				should.not.exist(error);
				should.exist(groups);
				total.should.equal(3);

				done();
			});
		});
	});

	describe('countItem', function () {
		it('should return total item', function (done) {
			repository.countItem(function (error, totals) {
				should.not.exist(error);
				totals[0].total.should.equal(6);

				done();
			});
		});
	});
});