var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');
var TmpStuffRepository = require('../../../server/repositories/TmpStuffRepository');

var repository = null,
		timeout = 1000;

describe('FunctionalTestTmpStuffRepository', function () {
	var options = {
		page: 0,
		limit: 2
	}

	before(function (done) {
		helper.setupDatabase(function (error, client) {
			if (error) {
				log.error('setup database error');
			}
			else {
				repository = new TmpStuffRepository(client);
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#findFromSite', function () {
		it('should success', function (done) {
			var site = 'strawberrynet';

			repository.findFromSite(site, options, function (error, stuffs, total) {
				//log.debug('stuffs '+util.inspect(stuffs));
				should.not.exists(error);
				total.should.equal(-1);
				stuffs.length.should.equal(2);
				for (var key in stuffs) {
					var stuff = stuffs[key];
					stuff.site.should.equal(site);
				}
				
				done();
			});
		});

		it('should success and return count tmpStuff', function (done) {
			var site = 'strawberrynet';
			options.countTmpStuff = true;

			repository.findFromSite(site, options, function (error, stuffs, total) {
				//log.debug('stuffs '+util.inspect(stuffs));
				should.not.exists(error);
				total.should.equal(3);
				stuffs.length.should.equal(2);
				for (var key in stuffs) {
					var stuff = stuffs[key];
					stuff.site.should.equal(site);
				}
				
				done();
			});
		});
	});

	describe('#findFromSKU', function () {
		it('should success', function (done) {
			var sku = '0002';
			var site = 'strawberrynet';

			repository.findFromSKU(sku, site, function (error, stuff) {
				should.not.exists(error);
				
				stuff.sku.should.equal(sku);
				stuff.site.should.equal(site);
				done();
			});
		});
	});
});