var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');
var StuffRepository = require('../../../server/repositories/StuffRepository');

var repository = null,
		timeout = 1000;

describe('FunctionalTestStuffRepository', function () {
	var options = {
		page: 0,
		limit: 2
	}

	before(function (done) {
		helper.setupDatabase(function (error, client) {
			if (error) {
				log.error('setup database error');
			}
			else {
				repository = new StuffRepository(client);
				setTimeout(function() {
					log.info('setup database success');
					done();
				}, timeout);
			}
		});
	});

	describe('#findFromOwner', function () {
		it('should success', function (done) {
			// zalora
			var userId = '54080e9d257926952e00004a';
      options.countStuff = true;

      repository.findFromOwner(userId, options, function (error, stuffs, total) {
        //log.debug('stuffs '+util.inspect(stuffs));
        should.not.exists(error);
        stuffs.length.should.equal(2);
        total.should.be.equal(3);

        for (var key in stuffs) {
          var stuff = stuffs[key];
          stuff.owner.should.equal(userId);
        }

        done();
      });
    });
  });

  describe('#findFromProductionId', function () {
		it('should success', function (done) {
			var productionId = '544decdd49d313ae25000001';

			repository.findFromProductionId(productionId, function (error, stuff) {
				should.not.exists(error);

        stuff.productionId.should.be.equal('544decdd49d313ae25000001');
				
				done();
			});
		});
	});
});
