var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');
var CrawlDataRepository = require('../../../server/repositories/CrawlDataRepository');

var repository = null,
		timeout = 1000;

describe('FunctionalTestBaseRepository', function () {
	before(function (done) {
		
		helper.setupDatabase(function (error, client) {
			if (error) {
				log.error('setup database error');
			}
			else {
				repository = new CrawlDataRepository(client);
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#repository', function () {
		var tmpCreatedUser = null;
		it('should create user', function (done) {
			
			var user = {
				firstname: 'test',
				lastname: 'baseRepo',
				email: 'test@sample.com',
				password: '11111111'
			}

			repository.create(user, function (error, createdUser) {
				
				should.not.exist(error);
				createdUser.should.have.property('lastname', user.lastname);
				tmpCreatedUser = createdUser;
				done();
			});

		});

		it('should get user', function (done) {
			
			repository.get(tmpCreatedUser._id.toString(), function (error, user) {
				
				should.not.exist(error);
				user.should.be.a('object');
				user.should.have.property('email', 'test@sample.com');
				done();
			});

		});

		it('should count user collection', function (done) {
			
			repository.count(function (error, user) {
				
				should.not.exist(error);
				user.should.be.a('number');
				user.should.be.above(5);
				done();
			});

		});

		it('should update user', function (done) {
			
			tmpCreatedUser.ban = true;
			repository.update(tmpCreatedUser, function (error, user) {
				
				should.not.exist(error);
				user.should.be.a('object');
				user.should.have.property('ban');
				done();
			});

		});

		it('should remove user', function (done) {
			var removeUserId = tmpCreatedUser._id.toString();
			repository.remove(removeUserId, function (error, isRemoved) {
				
				should.not.exist(error);
				isRemoved.should.be.ok;
				done();
			});

		});		
	});
});