var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');
var CrawlDataRepository = require('../../../server/repositories/CrawlDataRepository');

var repository = null,
		timeout = 1000;

describe('FunctionalTestCrawlDataRepository', function () {
	var offset = {
		page: 0,
		limit: 3
	}

	before(function (done) {
		helper.setupDatabase(function (error, client) {
			if (error) {
				log.error('setup database error');
			}
			else {
				repository = new CrawlDataRepository(client);
				setTimeout(function() {
					log.info('setup database success');
					done();
				}, timeout);
			}
		});
	});

	describe('#findFromUserId', function () {
		it('should return crawl data of userId', function (done) {
			var userId = '4f48e3f18c47fd650700000a';
			repository.findFromUserId(userId, offset, function (error, countData, data) {
				should.not.exist(error);
				should.exist(data);

				countData.should.equal(4);
				data.length.should.equal(3);

				for(var key in data) {
					var stuff = data[key];
					stuff.userId.should.equal(userId);
				}

				done();
			});

			it('should return all crawl when blank userId', function (data) {
				var userId = '';
				repository.findFromUserId(userId, offset, function (error, countData, data) {
					should.not.exist(error);
					should.exist(data);

					countData.should.equal(6);
					data.length.should.equal(3);

					done();
				});				
			});
		});
	});

	describe('#findFromGroupId', function () {
		it('should return crawl data of groupId', function (done) {
			var groupId = '4f48e47c8c47fd6507000012';
			repository.findFromUserId(groupId, offset, function (error, countData, data) {
				should.not.exist(error);
				should.exist(data);

				for(var key in data) {
					var stuff = data[key];
					stuff.groupId.should.equal(groupId);
				}

				done();
			});
		});
	});

	describe('#removeFromGroupId', function () {
		it('should success', function (done) {
			var groupId = '4f48e47c8c47fd6507000012';

			repository.removeFromGroupId(groupId, function (error, data) {
				should.not.exist(error);
				should.exist(data);
				
				for (var i = 0; i < data.length; i++) {
					data[i].groupId.should.equal(groupId);
				};

				done();
			});
		});
	});
});