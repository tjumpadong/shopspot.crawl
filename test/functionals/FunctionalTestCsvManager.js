var should = require('should'),
		util = require('util'),
		rest = require('restler'),
		async = require('async'),
		path = require('path');

var CsvManager = require('../../server/modules/CsvManager');
var test = null;

describe('FunctionalTestCsvManager', function () {
	before(function () {
		test = {
			csvManager: new CsvManager()
		}
	});

	describe('#readFromPath', function () {
		it('should success', function (done) {
			//var csvPath = path.join(__dirname, '..', 'resources', 'csvFile.csv');
			//var csvPath = path.join(__dirname, '..', 'resources', 'shopspot-products.csv');
			var csvPath = path.join(__dirname, '..', 'resources', 'wywCate.csv');
			test.csvManager.readFromPath(csvPath, function (error, result) {
				should.not.exist(error);
				done();
			});
		});

		it('should success call twice', function (done) {
			done();
		});
	});
});