var should = require('should'),
    util = require('util'),
    fs = require('fs'),
    async = require('async'),
    path = require('path');

var BaseParser = require('../../../server/parsers/BaseParser');
var log = require('winston');
var test = {};

describe('FunctionalTestBaseParser', function () {
  before(function (done) {
    test = {
      parser: new BaseParser()
    }

    done();
  });

  describe('#_convertPrice', function () {
    it('should success', function (done) {
      var input = '100';
      var price = test.parser._convertPrice(input);
      isNaN(price).should.be.false;
      price.should.be.equal(100);
      done();
    });

    it('should success', function (done) {
      var input = '1,000';
      var price = test.parser._convertPrice(input);
      isNaN(price).should.be.false;
      price.should.be.equal(1000);
      done();
    });

    it('should success', function (done) {
      var input = '1,000,000';
      var price = test.parser._convertPrice(input);
      isNaN(price).should.be.false;
      price.should.be.equal(1000000);
      done();
    });

    it('should success', function (done) {
      var input = '1,000,000 baht';
      var price = test.parser._convertPrice(input);
      isNaN(price).should.be.false;
      price.should.be.equal(1000000);
      done();
    });
  });

  describe('#_convertTagStringToArray', function () {
    it('should success', function (done) {
      var stuff = {
        tags: "aaaa,ssdfsadf .sssdf sa,sdfasdf"
      };
      var tags = test.parser._convertTagStringToArray(stuff);
      tags.length.should.equal(5);

      done();
    });
  });
});