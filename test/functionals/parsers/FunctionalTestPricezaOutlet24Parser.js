var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var PricezaOutlet24Parser = require('../../../server/parsers/PricezaOutlet24Parser');
var log = require('winston');
var test = {};

describe('FunctionalTestPricezaOutlet24Parser', function () {
	before(function (done) {
		var fixture = [{ 
			path1: 'http://img.priceza.com/img/product/2043/2043-20140613181506-443983.jpg',
	    name: 'VS Sassoon 25MM Tourmaline Ceramic Straightener',
	    price: '1,850',
	    description: 'เครื่องหนีบผมตรง VS Sassoon รุ่น VS2010PITผมตรงสวยง่ายๆ ทำได้เองที่บ้าน ไม่ต้องเสียเวลาไปร้านทำผม พกพาสะดวกสวยได้ทุกที ใช้เวลาเพียง 5 นาที เท่านี้คุณก็พร้อมออกจากบ้านได้อย่างมั่นใจแผ่นให้ความร้อนขนาด 25 มม. อุณหภูมิสูงสุด 230 องศาเซลเซียสร้อนเร็วภายใน 60 วินาทีเคลือบทัวมาลีน และเซรามิค-นาโนซิลเวอร์ปรับอุณหภูมิได้ 25 ระดับสวิชต์ล็อคตัวเครื่องเพื่อปิดแผ่นหนีบเข้าด้วยกันขนาดผลิตภัณฑ์ กว้าง x สูง (ซม.) 7.5 x 32รับประกัน 2 ปี',
	    mainCategory: 'แปรงหวีผม',
	    link: 'http://www.priceza.com/r/redirect?id=4360405&delay=true&utm_source=The+Outlet24&utm_campaign=ShopSpotItem&utm_medium=VS+Sassoon+25MM+Tourmaline+Ceramic+Straightener',
	    sku: 4360405,
	    site: 'The Outlet24' 
	  },
	  { 
	  	path1: 'http://img.priceza.com/img/product/2043/2043-20140613181507-70551.jpg',
	    name: 'VS Sassoon 32MM Tourmaline Ceramic 2-IN-1 Straightener/Curling Iron',
	    price: '1,950',
	    description: 'เครื่องหนีบผมตรง/ทำลอนผม 2 ใน 1 VS Sassoon รุ่น VSI3270PITแกนม้วนสามารถแยกออกได้สำหรับแต่งทรงผมแบบหนีบตรงและทำลอนผมแกนความร้อนขนาด 32 มม. เคลือบทัวมาลีน และเซรามิคร้อนเร็วภายใน 60 วินาที ปรับอุณหภูมิได้ 25 ระดับระบบปิดเครื่องอัตโนมัติขนาดผลิตภัณฑ์ กว้าง x สูง (ซม.) 7.5 x 34รับประกัน 2 ปี',
	    mainCategory: 'แปรงหวีผม',
	    link: 'http://www.priceza.com/r/redirect?id=4360407&delay=true&utm_source=The+Outlet24&utm_campaign=ShopSpotItem&utm_medium=VS+Sassoon+32MM+Tourmaline+Ceramic+2-IN-1+Straightener%2FCurling+Iron',
	    sku: 4360407,
	    site: 'The Outlet24' 
	  }];

		test = {
			fixture: fixture,
			parser: new PricezaOutlet24Parser()
		}

		done();
	});

	describe('#transform', function () {
		it('should success', function (done) {
			var fixture = test.fixture[0];
			var stuff = test.parser.transform(fixture);
			log.debug('stuff transform strawberrynet : '+util.inspect(stuff));
			stuff.images.should.be.instanceof(Array);
			stuff.categories.should.be.instanceof(Array);
			stuff.should.have.property('externalLink')
			stuff.should.have.property('images');
			
			isNaN(stuff.price).should.be.false;
			done();
		});
	});
});