var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var StrawberrynetParser = require('../../../server/parsers/StrawberrynetParser');
var log = require('winston');
var test = {};

describe('FunctionalTestStrawberrynetParser', function () {
	before(function (done) {
		var fixture = [{
			'path1': 'http://img4.strawberrynetmedia.com/images/products/00758930505.jpg',
			'link': 'http://th.strawberrynet.com/mens-fragrances/ralph-lauren/polo-green-eau-de-toilette-spray/7589/?region=th&trackid=7098500001',
			'name': 'Ralph Lauren สเปรย์น้ำหอม Polo Green EDT 59ml/2oz',
			'description': 'ตำนานของน้ำหอม Ralph Lauren  ผสมด้วยกลิ่นที่ดีที่สุดของ leather และแมกไม้  เติมเต็มบุคคลิกให้เท่ห์สมชายชาตรี',
			'price': '2586.5',
			'salePrice': '2488',
			'discount': 0,
			'mainCategory': 'Men\'s Fragrance',
			'tags': 'Ralph Lauren สเปรย์น้ำหอม Polo Green EDT .',
			'sku': '00758930505'
		},
		{
			'path1': 'http://img4.strawberrynetmedia.com/images/products/00759030505.jpg',
			'link': 'http://th.strawberrynet.com/mens-fragrances/ralph-lauren/polo-green-eau-de-toilette-spray/7590/?region=th&trackid=7098500001',
			'name': 'Ralph Lauren สเปรย์น้ำหอม Polo Green EDT 118ml/4oz',
			'description': 'ตำนานของน้ำหอม Ralph Lauren  ผสมด้วยกลิ่นที่ดีที่สุดของ leather และแมกไม้  เติมเต็มบุคคลิกให้เท่ห์สมชายชาตรี',
			'price': '3593.5',
			'salePrice': '3080',
			'discount': 0,
			'mainCategory': 'Men\'s Fragrance',
			'tags': 'Ralph Lauren สเปรย์น้ำหอม Polo Green EDT',
			'sku': '00759030505'
		}];

    var categoriesPath = path.join(__dirname, '..', '..', '..', 'server', 'resources', 'categories', 'strawberrynet.json');
    var categoriesData = JSON.parse(fs.readFileSync(categoriesPath));
    var categories = {
      strawberrynet: categoriesData
    };
    var parser = {
      site: 'strawberrynet'
    };

		test = {
			fixture: fixture,
			parser: new StrawberrynetParser(categories, parser)
		};

		done();
	});

	describe('#transform', function () {
		it('should success', function (done) {
			var fixture = test.fixture[0];
			var stuff = test.parser.transform(fixture);

			//log.debug('stuff transform strawberrynet : '+util.inspect(stuff));
			stuff.should.have.property('externalLink')
			stuff.should.have.property('images');
			stuff.categories.should.instanceof(Array);
			
			isNaN(stuff.price).should.be.false;
			done();
		});
	});
});