var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var ZaloraParser = require('../../../server/parsers/ZaloraParser');
var log = require('winston');
var test = {};
describe('FunctionalTestZaloraParser', function () {
	before(function (done) {
		var fixture = [{ 
			path1: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-1-product.jpg',
     	path2: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-2-product.jpg',
	    path3: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-3-product.jpg',
	    path4: 'http://static-origin.zalora.co.th/p/Alang-pink-924241-4-product.jpg',
	    link: 'http://zalorasea.go2cloud.org/aff_c?offer_id=14&aff_id=1079&file_id=3579&url=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F276259003%3B103357794%3Bi%3Fhttp%3A%2F%2Fmarketplace.zalora.co.th%2FAlang-pink-142429.html%3Fwt_af%3Dth.Affiliate.HasOffers.%7Baffiliate_id%7D.ALANGIF.ProductFeed%26utm_source%3DHasOffers%26utm_medium%3DAffiliate%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3DProductFeed',
	    name: 'Alang pink',
	    description: 'Alang pink ALANGIF',
	    price: '1250.00',
      mainCategory: 'ผู้หญิง',
      subCategory1: 'เสื้อผ้า',
      subCategory2: 'เสื้อเบลาส์',
	    sku: 'AL615AA70CSBTH',
	    tags: 'ALANGIF'
		},
   	{ 
   		path1: 'http://static-origin.zalora.co.th/p/Girl-Bucket-Hat-427441-1-product.jpg',
     	path2: 'http://static-origin.zalora.co.th/p/Girl-Bucket-Hat-427441-2-product.jpg',
     	link: 'http://zalorasea.go2cloud.org/aff_c?offer_id=14&aff_id=1079&file_id=3579&url=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F276259003%3B103357794%3Bi%3Fhttp%3A%2F%2Fmarketplace.zalora.co.th%2FGirl-Bucket-Hat-144724.html%3Fwt_af%3Dth.Affiliate.HasOffers.%7Baffiliate_id%7D.HAT+GODOWN.ProductFeed%26utm_source%3DHasOffers%26utm_medium%3DAffiliate%26utm_campaign%3D%7Baffiliate_id%7D%26utm_content%3DProductFeed',
     	name: 'Girl Bucket Hat',
     	description: 'Girl Bucket Hat HAT GODOWN',
     	price: '490.00',
      mainCategory: 'ผู้หญิง',
      subCategory1: 'เครื่องประดับ',
     	subCategory2: 'หมวก',
     	sku: 'HA517AC75GCITH',
     	tags: 'HAT GODOWN'
    }];
    
    var categoriesPath = path.join(__dirname, '..', '..', '..', 'server', 'resources', 'categories', 'zalora.json');
    var categoriesData = JSON.parse(fs.readFileSync(categoriesPath));
    var categories = {
      zalora: categoriesData
    };
    var parser = {
      site: 'zalora'
    };
		test = {
			fixture: fixture,
			parser: new ZaloraParser(categories, parser)
		}
		done();
	});

	describe('#transform', function () {
		it('should success', function (done) {
			var fixture = test.fixture[0];
			var stuff = test.parser.transform(fixture);
			
			stuff.should.have.property('externalLink')
			stuff.should.have.property('images');
			stuff.images.length.should.equal(4);
			stuff.categories.should.instanceof(Array);
			isNaN(stuff.price).should.be.false;
			done();
		});
	});

	describe('#mapCategory', function () {
		it('should success', function (done) {
			done();
		});
	});

});