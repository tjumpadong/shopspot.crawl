var should = require('should'),
		sinon = require('sinon'),
		util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var CrawlDataService = require('../../server/services/CrawlDataService');
var test = null;

describe('TestCrawlDataService', function () {
	before(function (done) {
		var crawlDataRepository = {
			get: function () {},
			update: function () {},
			remove: function () {},
			findFromUserId: function () {},
			findFromGroupId: function () {},
			removeFromGroupId: function () {}
		};

		var repositories = {
			crawlData: crawlDataRepository
		};

		var crawlDataService = new CrawlDataService(repositories);

		test = {
			repositories: repositories,
			crawlDataService: crawlDataService
		};

		done();
	});

	describe('#get', function () {
		it('should fail coz get cralwData fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var getStub = sinon.stub(crawlDataRepository, 'get')
													.callsArgWith(1, 'error', null);

			test.crawlDataService.get(input, function (error, crawlData) {
				should.exist(error);

				getStub.called.should.be.true;
				getStub.restore();
			});
		});

		it('should success', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var getStub = sinon.stub(crawlDataRepository, 'get')
													.callsArgWith(1, null, {});

			test.crawlDataService.get(input, function (error, crawlData) {
				should.not.exist(error);

				getStub.called.should.be.true;
				getStub.restore();
			});
		});
	});

	describe('#update', function () {
		it('should fail coz validate fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var validator = test.crawlDataService.crawlDataTranformValidator;
			var validatorStub = sinon.stub(validator, 'validate')
																.callsArgWith(1, 'error', null);

			test.crawlDataService.update(input, function (error, crawlData) {
				should.exist(error);

				validatorStub.called.should.be.true;
				validatorStub.restore();
			});
		});

		it('should fail coz get crawlData fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var validator = test.crawlDataService.crawlDataTranformValidator;
			var validatorStub = sinon.stub(validator, 'validate')
																.callsArgWith(1, null, true);
			var getStub = sinon.stub(crawlDataRepository, 'get')
													.callsArgWith(1, 'error', null);

			test.crawlDataService.update(input, function (error, crawlData) {
				should.exist(error);

				validatorStub.called.should.be.true;
				validatorStub.restore();

				getStub.called.should.be.true;
				getStub.restore();
			});
		});

		it('should fail coz update crawlData fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var validator = test.crawlDataService.crawlDataTranformValidator;
			var validatorStub = sinon.stub(validator, 'validate')
																.callsArgWith(1, null, true);
			var getStub = sinon.stub(crawlDataRepository, 'get')
													.callsArgWith(1, null, {});
			var updateStub = sinon.stub(crawlDataRepository, 'update')
														.callsArgWith(1, 'error', null);											

			test.crawlDataService.update(input, function (error, crawlData) {
				should.exist(error);
				
				validatorStub.called.should.be.true;
				validatorStub.restore();

				getStub.called.should.be.true;
				getStub.restore();
				
				updateStub.called.should.be.true;
				updateStub.restore();
			});
		});

		it('should sucess', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var validator = test.crawlDataService.crawlDataTranformValidator;
			var validatorStub = sinon.stub(validator, 'validate')
																.callsArgWith(1, null, true);
			var getStub = sinon.stub(crawlDataRepository, 'get')
													.callsArgWith(1, null, {});
			var updateStub = sinon.stub(crawlDataRepository, 'update')
														.callsArgWith(1, null, {});											

			test.crawlDataService.update(input, function (error, crawlData) {
				should.not.exist(error);
				
				validatorStub.called.should.be.true;
				validatorStub.restore();

				getStub.called.should.be.true;
				getStub.restore();
				
				updateStub.called.should.be.true;
				updateStub.restore();
			});
		});
	});

  describe('#updateStatus', function () {
    it('should error because of invalid input', function () {
      var input = {
        status: 'posted'
      };

      test.crawlDataService.updateStatus(input, function (error, result) {
        should.exist(error);
      });
    });

    it('should success', function () {
      var input = {
        crawlDataId: '9999',
        status: 'posted'
      };

      var crawlDataRepository = test.repositories.crawlData;

      var getStub = sinon.stub(crawlDataRepository, 'get')
                          .callsArgWith(1, null, {});
      var updateStub = sinon.stub(crawlDataRepository, 'update')
                            .callsArgWith(1, null, {});

      test.crawlDataService.updateStatus(input, function (error, result) {
        should.not.exist(error);

        getStub.called.should.be.true;
        updateStub.called.should.be.true;
      });
    });
  });

	describe('#removeDatas', function () {
		it('should coz blank crawlDataIds', function () {
			var input = {
				crawlDataIds: ''
			};

			test.crawlDataService.removeDatas(input, function (error, result) {
				should.exist(error);
			});
		});

		it('should success', function () {
			var input = {
				crawlDataIds: 'test'
			};
			var crawlDataRepository =test.repositories.crawlData;

			var removeDatasStub = sinon.stub(crawlDataRepository, 'remove')
																	.callsArgWith(1, null, true);

			test.crawlDataService.removeDatas(input, function (error, result) {
				should.not.exist(error);

				removeDatasStub.called.should.be.true;
				removeDatasStub.restore();
			});
		});
	}); 

	describe('#findFromUserId', function () {
		it('should error coz find fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var findStub = sinon.stub(crawlDataRepository, 'findFromUserId')
													.callsArgWith(2, 'error', null);
			test.crawlDataService.findFromUserId(input, function (error, crawlDatas) {
				should.exist(error);

				findStub.called.should.be.true;
				findStub.restore();
			});
		});

		it('should success', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var findStub = sinon.stub(crawlDataRepository, 'findFromUserId')
													.callsArgWith(2, null, []);
			test.crawlDataService.findFromUserId(input, function (error, crawlDatas) {
				should.not.exist(error);

				findStub.called.should.be.true;
				findStub.restore();
			});
		});
	});

	describe('#findFromGroupId', function () {
		it('should error coz find fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var findStub = sinon.stub(crawlDataRepository, 'findFromGroupId')
													.callsArgWith(2, 'error', null);
			test.crawlDataService.findFromGroupId(input, function (error, crawlDatas) {
				should.exist(error);

				findStub.called.should.be.true;
				findStub.restore();
			});
		});

		it('should success', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var findStub = sinon.stub(crawlDataRepository, 'findFromGroupId')
													.callsArgWith(2, null, []);
			test.crawlDataService.findFromGroupId(input, function (error, crawlDatas) {
				should.not.exist(error);

				findStub.called.should.be.true;
				findStub.restore();
			});
		});
	});

	describe('#removeFromGroupId', function () {
		it('should error coz remove fail', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var removeStub = sinon.stub(crawlDataRepository, 'removeFromGroupId')
													.callsArgWith(1, 'error', null);
			test.crawlDataService.removeFromGroupId(input, function (error, crawlDatas) {
				should.exist(error);

				removeStub.called.should.be.true;
				removeStub.restore();
			});
		});

		it('should success', function () {
			var input = {};
			var crawlDataRepository = test.repositories.crawlData;
			var removeStub = sinon.stub(crawlDataRepository, 'removeFromGroupId')
													.callsArgWith(1, null, []);
			test.crawlDataService.removeFromGroupId(input, function (error, crawlDatas) {
				should.not.exist(error);

				removeStub.called.should.be.true;
				removeStub.restore();
			});
		});
	});
});