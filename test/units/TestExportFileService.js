var util = require('util'),
		path = require('path'),
		fs = require('fs');

var ExportFileService = require('../../server/services/ExportFileService');
var test = {};
describe('TestExportFileService', function () {
	before(function (done) {
		test = {
			service: new ExportFileService()
		}
		done();
	});

	describe('#ExportToJson', function () {
    it('should success', function (done) {
      var pathFile = path.join(__dirname, '..', 'resources', 'zaloraCate.csv');
      var input = {
        path: pathFile
      };

      test.service.categoryToJson(input, function (error, data) {
        done();
      });
    });
  });
});