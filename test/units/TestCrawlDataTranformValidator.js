var should = require('should'),
		sinon = require('sinon'),
		util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var CrawlDataTranformValidator = require('../../server/validator/CrawlDataTranformValidator');
var test = null;


describe('TestCrawlDataTranformValidator', function () {
	before(function (done) {
		var validator = new CrawlDataTranformValidator();
		
		test = {
			validator: validator
		};
		done();
	});

	describe('#validate', function () {
		it('should fail coz blank categories', function (done) {
			var input = {};
			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz empty array categories', function (done) {
			var input = {
				categories: []
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank description', function (done) {
			var input = {
				categories: ['men'],
				description: ''
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank externalLink', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: ''
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank images', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz empty array images', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: []
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank market', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: ['path1'],
				market: ''
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank owner', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: ['path1'],
				market: 'TH',
				owner: ''
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz blank price', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: ['path1'],
				market: 'TH',
				owner: 'user1',
				price: ''
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should fail coz invalid price', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: ['path1'],
				market: 'TH',
				owner: 'user1',
				price: 'hello'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);

				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_INVALID_INPUT);

				done();
			});
		});

		it('should success', function (done) {
			var input = {
				categories: ['men'],
				description: 'test',
				externalLink: 'link',
				images: ['path1'],
				market: 'TH',
				owner: 'user1',
				price: '100',
				tags: 'test'
			};

			test.validator.validate(input, function (error, data) {
				should.not.exist(error);
				should.exist(data);

				done();
			});
		});
	});
});