var should = require('should'),
		sinon = require('sinon'),
		util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var GroupCrawlDataService = require('../../server/services/GroupCrawlDataService');
var test = null;

describe('TestGroupCrawlDataService', function () {
	before(function (done) {
		var groupCrawlDataRepository = {
			findFromUserId: function () {},
			remove: function () {},
			countItem: function () {}
		};

		var repositories = {
			groupCrawlData: groupCrawlDataRepository
		};

		var groupCrawlDataService = new GroupCrawlDataService(repositories);

		test = {
			repositories: repositories,
			groupCrawlDataService: groupCrawlDataService
		};

		done();
	});

	describe('#findFromUserId', function () {
		it('should success', function () {
			var input = {};
			var groupCrawlDataRepository = test.repositories.groupCrawlData;
			var findStub = sinon.stub(groupCrawlDataRepository, 'findFromUserId')
													.callsArgWith(2, null, 0, []);

			test.groupCrawlDataService.findFromUserId(input, function (error, groups) {
				should.not.exist(error);

				findStub.called.should.be.true;
				findStub.restore();
			});
		});
	});

	describe('#removeFromGroupId', function () {
		it('should success', function () {
			var input = {};
			var groupCrawlDataRepository = test.repositories.groupCrawlData;
			var removeStub = sinon.stub(groupCrawlDataRepository, 'remove')
													.callsArgWith(1, null, true);

			test.groupCrawlDataService.removeFromGroupId(input, function (error, groups) {
				should.not.exist(error);

				removeStub.called.should.be.true;
				removeStub.restore();
			});
		});
	});

	describe('#countItem', function () {
		it('should error coz can not get count items', function (done) {
			var groupCrawlDataRepository = test.repositories.groupCrawlData;
			var countItemStub = sinon.stub(groupCrawlDataRepository, 'countItem')
													.callsArgWith(0, 'error', null);

			test.groupCrawlDataService.countItem(function (error, result) {
				should.exist(error);

				countItemStub.called.should.be.true;
				countItemStub.restore();

				done();
			});
		});

		it('should success', function (done) {
			var groupCrawlDataRepository = test.repositories.groupCrawlData;
			var countItemStub = sinon.stub(groupCrawlDataRepository, 'countItem')
													.callsArgWith(0, null, [{ total: 0}]);

			test.groupCrawlDataService.countItem(function (error, result) {
				should.not.exist(error);

				countItemStub.called.should.be.true;
				countItemStub.restore();

				done();
			});
		});
	});
});