var should = require('should'),
    sinon = require('sinon'),
    async = require('async'),
    util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var importFileController = require('../../server/controllers/ImportFileController');
var test = null,
		tmpInput = {
			session: { user: { id: 'user1' } },
			data: {}
		};
var res = { json: function () {} };
describe('TestImportFileController', function () {
	before(function (done) {
		importFileController.json = function () {};
		done();
	});

	describe('#csv', function () {
		it('should success', function (done) {
			var req = {
				files: {
					path: 'path'
				},
				body: {
					title: 'title',
					description: 'desc'
				},
				session: {
					userId: 'userId'
				}
			}
			

			var importFileService = importFileController.importFileService;
			var csvStub = sinon.stub(importFileService, 'csv')
												.callsArgWith(1, null, {});

			async.waterfall([
				function callFunction (next) {
					importFileController.csv(req, res);

					next();
				},
				function testcase () {
					csvStub.called.should.be.true;
					csvStub.restore();

					done();
				}
			]);
		});
	});

	describe('#xml', function () {
		it('should success', function (done) {
			var req = {
				body: {
					site: 'zalora',
					uri: 'path'
				},
				session: {
					userId: 'userId'
				}
			}
			

			var importFileService = importFileController.importFileService;
			var xmlStub = sinon.stub(importFileService, 'xml')
												.callsArgWith(1, null, {});

			async.waterfall([
				function callFunction (next) {
					importFileController.xml(req, res);

					next();
				},
				function testcase () {
					xmlStub.called.should.be.true;
					xmlStub.restore();

					done();
				}
			]);
		});
	});
});