var should = require('should'),
    sinon = require('sinon'),
    util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var StuffService = require('../../server/services/StuffService');

var test;

describe('TestStuffService', function () {
  before(function (done) {
    var stuffRepository = {
      create: function () {},
      update: function () {},
      findFromSKU: function () {},
      get: function () {},
      findFromOwner: function () {},
      findFromProductionId: function () {},
      updateFromRevision: function () {}
    };

    var tmpStuffRepository = {
      findFromSite: function () {},
      update: function () {}
    };

    var repositories = {
      stuff: stuffRepository,
      tmpStuff: tmpStuffRepository
    };

    var config = {
      parsers: {
        zalora: "54080e9d257926952e00004a",
        strawberrynet: "543decdd49d313ae250000a3" 
      }
    }
    var stuffService = new StuffService(repositories, config);

    test = {
      repositories: repositories,
      stuffService: stuffService
    };

    done();
  });

  describe('#syncTmpToStuff', function () {
    it('should fail because there is no tmpStuff', function () {
      var input = { site: 'shopspot' };

      var tmpStuffRepository = test.repositories.tmpStuff;
      var tmpStuffFindStub = sinon.stub(tmpStuffRepository, 'findFromSite')
                              .callsArgWith(2, 'error', null);

      test.stuffService.syncTmpToStuff(input, function (error, success) {
        should.exist(error);

        tmpStuffFindStub.called.should.be.true;
        tmpStuffFindStub.restore();
      });
    });

    it('should success with non-existing stuffs', function () {
      var input = { site: 'shopspot' };
      var tmpStuffFixture = {
        transform: {
          price: 50
        }
      };

      var tmpStuffRepository = test.repositories.tmpStuff;
      var stuffRepository = test.repositories.stuff;

      var tmpStuffFindStub = sinon.stub(tmpStuffRepository, 'findFromSite')
                                  .callsArgWith(2, null, [tmpStuffFixture]);
      var stuffFindSKUStub = sinon.stub(stuffRepository, 'findFromSKU')
                                  .callsArgWith(2, null, null);
      var stuffCreateStub = sinon.stub(stuffRepository, 'create')
                                  .callsArgWith(1, null, {});
      var tmpStuffUpdateStub = sinon.stub(tmpStuffRepository, 'update')
                                                                .callsArgWith(1, null, {});

      test.stuffService.syncTmpToStuff(input, function (error, success) {
        should.not.exist(error);

        tmpStuffFindStub.called.should.be.true;
        tmpStuffFindStub.restore();

        stuffFindSKUStub.called.should.be.true;
        stuffFindSKUStub.restore();

        stuffCreateStub.called.should.be.true;
        stuffCreateStub.restore();

        tmpStuffUpdateStub.called.should.be.true;
        tmpStuffUpdateStub.restore();
      });
    });

    it('should success with existing and publish stuffs', function () {
      var input = { site: 'shopspot' };
      var tmpStuffFixture = {
        transform: {
          price: 50
        }
      };
      var stuffFixture = {
        status: 'published'
      };

      var tmpStuffRepository = test.repositories.tmpStuff;
      var stuffRepository = test.repositories.stuff;

      var tmpStuffFindStub = sinon.stub(tmpStuffRepository, 'findFromSite')
                                  .callsArgWith(2, null, [tmpStuffFixture]);
      var stuffFindSKUStub = sinon.stub(stuffRepository, 'findFromSKU')
                                  .callsArgWith(2, null, stuffFixture);
      var updateDuplicateStub = sinon.stub(test.stuffService, '_updateDuplicate')
                                      .callsArgWith(2, null, {});
      var tmpStuffUpdateStub = sinon.stub(tmpStuffRepository, 'update')
                                    .callsArgWith(1, null, {});

      test.stuffService.syncTmpToStuff(input, function (error, success) {
        should.not.exist(error);

        tmpStuffFindStub.called.should.be.true;
        tmpStuffFindStub.restore();

        stuffFindSKUStub.called.should.be.true;
        stuffFindSKUStub.restore();

        updateDuplicateStub.called.should.be.true;
        updateDuplicateStub.restore();

        tmpStuffUpdateStub.called.should.be.true;
        tmpStuffUpdateStub.restore();
      });
    });

    it('should success with existing and unpublish stuffs', function () {
      var input = { site: 'shopspot' };
      var tmpStuffFixture = {
        transform: {
          price: 50
        }
      };
      var stuffFixture = {
        status: 'unpublished'
      };

      var tmpStuffRepository = test.repositories.tmpStuff;
      var stuffRepository = test.repositories.stuff;

      var tmpStuffFindStub = sinon.stub(tmpStuffRepository, 'findFromSite')
                                  .callsArgWith(2, null, [tmpStuffFixture]);
      var stuffFindSKUStub = sinon.stub(stuffRepository, 'findFromSKU')
                                  .callsArgWith(2, null, stuffFixture);
      var stuffUpdateStub = sinon.stub(stuffRepository, 'update')
                                      .callsArgWith(1, null, {});
      var tmpStuffUpdateStub = sinon.stub(tmpStuffRepository, 'update')
                                    .callsArgWith(1, null, {});

      test.stuffService.syncTmpToStuff(input, function (error, success) {
        should.not.exist(error);

        tmpStuffFindStub.called.should.be.true;
        tmpStuffFindStub.restore();

        stuffFindSKUStub.called.should.be.true;
        stuffFindSKUStub.restore();

        stuffUpdateStub.called.should.be.true;
        stuffUpdateStub.restore();

        tmpStuffUpdateStub.called.should.be.true;
        tmpStuffUpdateStub.restore();
      });
    });
  });

  describe('#_updateDuplicate', function () {
    it('should success duplicate stuffs', function () {
      var newStuff = { test: 'data1' };
      var oldStuff = { test: 'data1' };
      var stuffRepository = test.repositories.stuff;
      var stuffUpdateStub = sinon.stub(stuffRepository, 'update')
                              .callsArgWith(1, null, {});

      test.stuffService._updateDuplicate(newStuff, oldStuff, function (error, updatedStuff) {
        should.not.exist(error);

        stuffUpdateStub.called.should.be.false;
        stuffUpdateStub.restore();
      });
    });

    it('should success non-duplicate stuffs', function () {
      var newStuff = { test: 'data1' };
      var oldStuff = { test: 'data2' };
      var stuffRepository = test.repositories.stuff;
      var stuffUpdateStub = sinon.stub(stuffRepository, 'update')
                              .callsArgWith(1, null, {});

      test.stuffService._updateDuplicate(newStuff, oldStuff, function (error, updatedStuff) {
        should.not.exist(error);

        stuffUpdateStub.called.should.be.true;
        stuffUpdateStub.restore();
      });
    });
  });

  describe('#updateProductionStuffId', function () {
    it('should error because do not have productionStuffId', function (done) {
        
      var input = {
        stuffId: 'stuffId'
      };

      test.stuffService.updateProductionStuffId(input, function (error, stuff) {
        should.exists(error);

        error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
        error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);
        done();
      });

    });

    it('should error because can not get stuff', function (done) {
        
      var input = {
        productionStuffId: 'productionStuffId'
      };

      var stuffRepository = test.repositories.stuff;

      var getStub = sinon.stub(stuffRepository, 'get')
                        .callsArgWith(1, 'error', {});
      test.stuffService.updateProductionStuffId(input, function (error, stuff) {
        should.exists(error);

        getStub.called.should.be.true;
        getStub.restore();

        done();
      });

    });

    it('should success', function (done) {
        
      var input = {
        stuffId: 'stuffId',
        productionStuffId: 'productionStuffId'
      };

      var stuffRepository = test.repositories.stuff;

      var getStub = sinon.stub(stuffRepository, 'get')
                        .callsArgWith(1, null, {});
      var updateStub = sinon.stub(stuffRepository, 'update')
                            .callsArgWith(1, null, {});                        

      test.stuffService.updateProductionStuffId(input, function (error, stuff) {
        should.not.exists(error);

        getStub.called.should.be.true;
        getStub.restore();

        updateStub.called.should.be.true;
        updateStub.restore();
        
        done();
      });

    });
  });

  describe('#findFromSite', function () {
    it('should error because validate site error', function (done) {
      var input = {
        site: 'vee'
      };

      test.stuffService.findFromSite(input, function (error, stuff, total) {
        should.exists(error);

        error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
        error.code.should.equal(ErrorCode.VALIDATE_INVALID_INPUT);

        done();
      });
    });

    it('should error because can not get stuff from owner', function (done) {
      var input = {
        site: 'zalora'
      };

      var stuffRepository = test.repositories.stuff;
      var findFromOwnerStub = sinon.stub(stuffRepository, 'findFromOwner')
                                  .callsArgWith(2, 'error', {});
      test.stuffService.findFromSite(input, function (error, stuff, total) {
        should.exists(error);

        findFromOwnerStub.called.should.be.true;
        findFromOwnerStub.restore();
        done();
      });
    });

    it('should success', function (done) {
      var input = {
        site: 'zalora'
      };

      var stuffRepository = test.repositories.stuff;
      var findFromOwnerStub = sinon.stub(stuffRepository, 'findFromOwner')
                                  .callsArgWith(2, null, []);
      test.stuffService.findFromSite(input, function (error, stuff, total) {
        should.not.exists(error);
        
        findFromOwnerStub.called.should.be.true;
        findFromOwnerStub.restore();
        done();
      });
    });
  });

  describe('#findFromProductionId', function () {
    it('should success', function (done) {
      var input = {
        productionId: '112211'
      };

      var stuffRepository = test.repositories.stuff;
      var findStub = sinon.stub(stuffRepository, 'findFromProductionId')
                          .callsArgWith(1, null, {});

      test.stuffService.findFromProductionId(input, function (error, stuff) {
        should.not.exists(error);

        findStub.called.should.be.true;
        findStub.restore();

        done();
      });
    });
  });

  describe('#updateFromRevision', function () {
    it('should failed because there is no id', function (done) {
      var input = {};

      var stuffRepository = test.repositories.stuff;

      test.stuffService.updateFromRevision(input, function (error, stuff) {
        should.exists(error);

        done();
      });
    });

    it('should success', function (done) {
      var input = {
        _id: 'testid'
      };

      var stuffRepository = test.repositories.stuff;
      var findStub = sinon.stub(stuffRepository, 'get')
                          .callsArgWith(1, null, {});
      var updateStub = sinon.stub(stuffRepository, 'update')
                            .callsArgWith(1, null, {});

      test.stuffService.updateFromRevision(input, function (error, stuff) {
        should.not.exists(error);

        findStub.called.should.be.true;
        findStub.restore();

        updateStub.called.should.be.true;
        updateStub.restore();

        done();
      })
    });
  });

  describe('#findFromId', function () {
    it('should success', function (done) {
      var input = {
        _id: 'testid'
      };

      var stuffRepository = test.repositories.stuff;
      var findStub = sinon.stub(stuffRepository, 'get')
                          .callsArgWith(1, null, {});

      test.stuffService.findFromId(input, function (error, stuff) {
        should.not.exists(error);

        findStub.called.should.be.true;
        findStub.restore();

        done();
      })
    });
  });
});