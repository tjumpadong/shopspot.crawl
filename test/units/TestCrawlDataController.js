var should = require('should'),
    sinon = require('sinon'),
    async = require('async'),
    util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var crawlDataController = require('../../server/controllers/CrawlDataController');
var test = null
var res = { 
	json: function () {}
};

describe('TestCrawlDataController', function () {
	before(function (done) {
		crawlDataController.json = function () {};
		test = {
			crawlDataService: crawlDataController.crawlDataService
		}
		done();
	});

	describe('#get', function () {
		it('should success', function (done) {
			var req = {
				params: {
					crawlDataId : 'test'
				},
				query: {}
			};

			

			var crawlDataService = test.crawlDataService;
			var getStub = sinon.stub(crawlDataService, 'get')
													.callsArgWith(1, null, {});
			
			async.waterfall([
				function callFunction (next) {
					crawlDataController.get(req, res);

					next();
				},
				function testcase () {
					getStub.called.should.be.true;
					getStub.restore();

					done();
				}
			]);					
		});
	});

	describe('#update', function () {
		it('should success', function (done) {
			var req = {
				params: {},
				body: {}
			};

			var crawlDataService = test.crawlDataService;
			var updateStub = sinon.stub(crawlDataService, 'update')
														.callsArgWith(1, null, true);
			
			async.waterfall([
				function callFunction (next) {
					crawlDataController.update(req, res);

					next();
				},
				function testcase () {
					updateStub.called.should.be.true;
					updateStub.restore();

					done();
				}
			]);					
		});
	});

  describe('#updateStatus', function () {
    it('should success', function (done) {
      var req = {
        body: {
          status: 'posted'
        }
      };

      var crawlDataService = test.crawlDataService;
      var updateStub = sinon.stub(crawlDataService, 'updateStatus')
                            .callsArgWith(1, null, true);
      var jsonSpy = sinon.spy(crawlDataController, 'json');

      async.waterfall([
        function callFunction (next) {
          crawlDataController.updateStatus(req, res);

          next();
        },
        function testcase () {
          updateStub.called.should.be.true;
          updateStub.restore();

          done();
        }
      ]);   
    });
  });

	describe('#removeDatas', function () {
		it('should success', function (done) {
			var req = {
				query: {
					crawlDataIds : 'test'
				}
			};

			var crawlDataService = test.crawlDataService;
			var removeDatasStub = sinon.stub(crawlDataService, 'removeDatas')
													.callsArgWith(1, null, true);
			
			async.waterfall([
				function callFunction (next) {
					crawlDataController.removeDatas(req, res);

					next();
				},
				function testcase () {
					removeDatasStub.called.should.be.true;
					removeDatasStub.restore();

					done();
				}
			]);					
		});
	});

	describe('#findFromUserId', function () {
		it('should success', function (done) {
			var req = {
				params: {
					uid: 'userId'
				},
				query: {}
			};
			
			var output = {
				crawlDatas: []
			};

			var crawlDataService = test.crawlDataService;
			var findStub = sinon.stub(crawlDataService, 'findFromUserId')
												.callsArgWith(1, null, output);

			async.waterfall([
				function callFunction (next) {
					crawlDataController.findFromUserId(req, res);

					next();
				},
				function testcase () {
					findStub.called.should.be.true;
					findStub.restore();

					done();
				}
			]);
		});
	});

	describe('#findFromGroupId', function () {
		it('should success', function (done) {
			var req = {
				params: {
					groupId: 'groupId'
				},
				query: {}
			};
			
			var output = {
				crawlDatas: []
			};

			var crawlDataService = test.crawlDataService;
			var findStub = sinon.stub(crawlDataService, 'findFromGroupId')
												.callsArgWith(1, null, output);

			async.waterfall([
				function callFunction (next) {
					crawlDataController.findFromGroupId(req, res);

					next();
				},
				function testcase () {
					findStub.called.should.be.true;
					findStub.restore();

					done();
				}
			]);
		});
	});

	describe('#removeFromGroupId', function () {
		it('should success', function (done) {
			var req = {
				query: {
					groupId : 'test'
				}
			};

			var crawlDataService = test.crawlDataService;
			var removeStub = sinon.stub(crawlDataService, 'removeFromGroupId')
													.callsArgWith(1, null, true);
			
			async.waterfall([
				function callFunction (next) {
					crawlDataController.removeFromGroupId(req, res);

					next();
				},
				function testcase () {
					removeStub.called.should.be.true;
					removeStub.restore();

					done();
				}
			]);					
		});
	});
});