var should = require('should'),
    sinon = require('sinon'),
    async = require('async'),
    util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var groupCrawlDataController = require('../../server/controllers/GroupCrawlDataController');
var test = null
var res = { 
	json: function () {} 
};
describe('TestGroupCrawlDataController', function () {
	before(function (done) {
		groupCrawlDataController.json = function () {};
		test = {
			groupCrawlDataService: groupCrawlDataController.groupCrawlDataService
		}
		done();
	});

	describe('#findFromUserId', function () {
		it('should success', function (done) {
			var req = {
				params: {
					uid: 'userId'
				},
				query: {}
			}

			var groupCrawlDataService = test.groupCrawlDataService;
			var result = {
				total: 0,
				groups: []
			};

			var findStub = sinon.stub(groupCrawlDataService, 'findFromUserId')
												.callsArgWith(1, null, result);

			async.waterfall([
				function callFunction (next) {
					groupCrawlDataController.findFromUserId(req, res);

					next();
				},
				function testcase () {
					findStub.called.should.be.true;
					findStub.restore();

					done();
				}
			]);
		});
	});

	describe('#findFromUserId', function () {
		it('should success', function (done) {
			var req = {
				params: {
					uid: 'userId'
				},
				query: {}
			}

			var groupCrawlDataService = test.groupCrawlDataService;
			var result = {
				total: 0,
				groups: []
			};

			var findStub = sinon.stub(groupCrawlDataService, 'findFromUserId')
												.callsArgWith(1, null, result);

			async.waterfall([
				function callFunction (next) {
					groupCrawlDataController.findFromUserId(req, res);

					next();
				},
				function testcase () {
					findStub.called.should.be.true;
					findStub.restore();

					done();
				}
			]);
		});
	});

	describe('#removeFromGroupId', function () {
		it('should success', function (done) {
			var req = {
				query: {}
			}

			var groupCrawlDataService = test.groupCrawlDataService;
			var removeStub = sinon.stub(groupCrawlDataService, 'removeFromGroupId')
												.callsArgWith(1, null, true);

			async.waterfall([
				function callFunction (next) {
					groupCrawlDataController.removeFromGroupId(req, res);

					next();
				},
				function testcase () {
					removeStub.called.should.be.true;
					removeStub.restore();

					done();
				}
			]);
		});
	});

	describe('#countItem', function () {
		it('should success', function (done) {
			var req = {
				query: {}
			}
			
			var groupCrawlDataService = test.groupCrawlDataService;
			var countItemStub = sinon.stub(groupCrawlDataService, 'countItem')
												.callsArgWith(0, null, true);
			
			async.waterfall([
				function callFunction (next) {
					groupCrawlDataController.countItem(req, res);

					next();
				},
				function testcase () {
					countItemStub.called.should.be.true;
					countItemStub.restore();

					done();
				}
			]);
		});
	});
});