var should = require('should'),
		sinon = require('sinon'),
		util = require('util');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var ImportFileValidator = require('../../server/validator/ImportFileValidator');
var test = null;

describe('TestImportFileValidator', function () {
	before(function (done) {
		var validator = new ImportFileValidator();
		
		test = {
			validator: validator
		};
		done();
	});

	describe('#validate', function () {
		it('should fail coz blank path', function (done) {
			var input = {};
			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_PATH_FILE);
				done();
			});
		});

		it('should fail coz blank owner', function (done) {
			var input = {
				path: 'inputPath'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);
				done();
			});
		});

		it('should fail coz blank title', function (done) {
			var input = {
				path: 'path',
				userId: 'userId'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);
				done();
			});
		});

		it('should fail coz blank description', function (done) {
			var input = {
				path: 'path',
				userId: 'userId',
				title: 'test'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);
				done();
			});
		});

		it('should fail coz description out of length', function (done) {
			var input = {
				path: 'path',
				userId: 'userId',
				title: 'test',
				description: ''
			};

			for (var i = 0; i < 510;i++) {
				input.description += 'a';
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_TEXT_LENGTH);
				done();
			});
		});

		it('should fail coz blank parser', function (done) {
			var input = {
				path: 'path',
				userId: 'userId',
				title: 'groupname',
				description: 'test'
			};

			test.validator.validate(input, function (error, data) {
				should.exist(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);
				done();
			});
		});

		it('should success', function (done) {
			var input = {
				path: 'path',
				userId: 'userId',
				title: 'groupname',
				description: 'comment',
				parser: {}
			};

			test.validator.validate(input, function (error, data) {
				should.not.exist(error);
				should.exist(data);
				done();
			});
		});
	});
});