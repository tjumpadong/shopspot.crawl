var should = require('should'),
		sinon = require('sinon'),
		util = require('util');
var log = require('winston');

var ErrorCode = require('../../server/modules/Error').ErrorCode;
var ErrorDomain = require('../../server/modules/Error').ErrorDomain;
var ImportFileService = require('../../server/services/ImportFileService');
var test = null,
		tmpInput = {
			session: { user: { id: 'user1' } },
			data: {}
		};

describe('TestImportFileService', function () {
	before(function (done) {

		var repositories = {
			tmpStuff: {
				create: function () {}
			}
		};

    var repositories = {
      tmpStuff: {
        create: function () {}
      }
    };
/*
		var crawlParsers = {
			zalora: function () {
				parse: function () {}
			},
			get: function (site) {
				return this[site];
			}
		};
*/

		var importFileService = new ImportFileService(repositories);
    importFileService.csvManager = {
      readFromPath: function () {}
    };
		
		test = {
			importFileService: importFileService,
			mockDatasource: {
				convert: function () {}
			},
      repositories: repositories
		};

		importFileService.crawlParsers.setup({}, done);
	});

	describe('#csv', function () {
		it('should fail because validate site failed', function (done) {
      var input = {};

      test.importFileService.csv(input, function (error, data) {
        should.exist(error);
        error.message.should.be.equal('Site validate not match');
        done();
      });
    });

    it('should fail because converting datasource failed', function (done) {
      var input = {
        site: 'strawberrynet'
      };

      var datasourceFactory = test.importFileService.datasourceFactory;
      var mockDatasource = sinon.stub(datasourceFactory, 'createCSVDatasource')
                                .returns(test.mockDatasource);

      var csvDatasourceStub = sinon.stub(mockDatasource(), 'convert')
                                  .callsArgWith(0, 'error', null);

      test.importFileService.csv(input, function (error, data) {
        should.exist(error);

        mockDatasource.restore();

        csvDatasourceStub.called.should.be.true;
        csvDatasourceStub.restore();

        done();
      });
    });

    it('should success', function (done) {
      var input = {
        site: 'strawberrynet'
      };

      var datasourceFactory = test.importFileService.datasourceFactory;
      var parser = test.crawlParsers;

      var mockDatasource = sinon.stub(datasourceFactory, 'createCSVDatasource')
                                .returns(test.mockDatasource);
      
      var csvDatasourceStub = sinon.stub(mockDatasource(), 'convert')
                                  .callsArgWith(0, null, [{}]);
      var tmpStuffCreateStub = sinon.stub(test.repositories.tmpStuff, 'create')
                                    .callsArgWith(1, null, {});

      test.importFileService.csv(input, function (error, data) {
        should.not.exist(error);

        mockDatasource.restore();

        csvDatasourceStub.called.should.be.true;
        csvDatasourceStub.restore();

        tmpStuffCreateStub.called.should.be.true;
        tmpStuffCreateStub.restore();

        done();
      });
    });

		/*it('should fail coz can not create groupCrawlData', function (done) {
			var input = {};
			var validator = test.importFileService.importFileValidator;
			var csvManager = test.importFileService.csvManager;
			var parser = test.parserFactory.instance();
			var groupCrawlDataReposiotry = test.repositories.groupCrawlData;

			var validatorStub = sinon.stub(validator, 'validate')
															.callsArgWith(1, null, true);
			var csvStub = sinon.stub(csvManager, 'readFromPath')
												.callsArgWith(1, null, {});
			var validateHeaderStub = sinon.stub(parser, 'validateHeader')
																			.callsArgWith(1, null, true);
			var groupCrawlCreateStub = sinon.stub(groupCrawlDataReposiotry, 'create')
																			.callsArgWith(1, 'error', null);												
			test.importFileService.csv(input, function (error, data) {
				should.exist(error);

				validatorStub.called.should.be.true;
				validatorStub.restore();

				csvStub.called.should.be.true;
				csvStub.restore();

				validateHeaderStub.called.should.be.true;
				validateHeaderStub.restore();				

				groupCrawlCreateStub.called.should.be.true;
				groupCrawlCreateStub.restore();

				done();
			});
		});

		it('should fail coz can not update groupCrawlData', function (done) {
			var input = {};
			var validator = test.importFileService.importFileValidator;
			var csvManager = test.importFileService.csvManager;
			var groupCrawlDataReposiotry = test.repositories.groupCrawlData;
			var createdGroupFixture = test.fixtures.groupCrawlData['group1'];
			var parser = test.parserFactory.instance();

			var validatorStub = sinon.stub(validator, 'validate')
															.callsArgWith(1, null, true);
			var csvStub = sinon.stub(csvManager, 'readFromPath')
												.callsArgWith(1, null, {});
			var validateHeaderStub = sinon.stub(parser, 'validateHeader')
																			.callsArgWith(1, null, true);
			var groupCrawlCreateStub = sinon.stub(groupCrawlDataReposiotry, 'create')
																			.callsArgWith(1, null, createdGroupFixture);
			
			var parserStub = sinon.stub(parser, 'parse')
														.returns({});
			var groupCrawlUpdateStub = sinon.stub(groupCrawlDataReposiotry, 'update')
																			.callsArgWith(1, 'error', null);												
			test.importFileService.csv(input, function (error, data) {
				should.exist(error);

				validatorStub.called.should.be.true;
				validatorStub.restore();

				csvStub.called.should.be.true;
				csvStub.restore();

				validateHeaderStub.called.should.be.true;
				validateHeaderStub.restore();

				groupCrawlCreateStub.called.should.be.true;
				groupCrawlCreateStub.restore();
				
				parserStub.called.should.be.true;
				parserStub.restore();

				groupCrawlUpdateStub.called.should.be.true;
				groupCrawlUpdateStub.restore();
				done();
			});
		});

		it('should success', function (done) {
			var input = {};
			var validator = test.importFileService.importFileValidator;
			var csvManager = test.importFileService.csvManager;
			var groupCrawlDataReposiotry = test.repositories.groupCrawlData;
			var createdGroupFixture = test.fixtures.groupCrawlData['group1'];
			var parser = test.parserFactory.instance();

			var validatorStub = sinon.stub(validator, 'validate')
															.callsArgWith(1, null, true);
			var csvStub = sinon.stub(csvManager, 'readFromPath')
												.callsArgWith(1, null, {});
			var validateHeaderStub = sinon.stub(parser, 'validateHeader')
																			.callsArgWith(1, null, true);
			var groupCrawlCreateStub = sinon.stub(groupCrawlDataReposiotry, 'create')
																			.callsArgWith(1, null, createdGroupFixture);
			var groupCrawlUpdateStub = sinon.stub(groupCrawlDataReposiotry, 'update')
																			.callsArgWith(1, null, createdGroupFixture);												
			var parserStub = sinon.stub(parser, 'parse')
														.returns({});
														
			test.importFileService.csv(input, function (error, data) {
				should.not.exist(error);

				validatorStub.called.should.be.true;
				validatorStub.restore();

				csvStub.called.should.be.true;
				csvStub.restore();

				validateHeaderStub.called.should.be.true;
				validateHeaderStub.restore();

				groupCrawlCreateStub.called.should.be.true;
				groupCrawlCreateStub.restore();
				
				groupCrawlUpdateStub.called.should.be.true;
				groupCrawlUpdateStub.restore();

				done();
			});
		});*/

	});

	describe('#xml', function () {
		it('should error coz validate error', function (done) {
			var input = {
				site: 'xyz'
			};

			test.importFileService.xml(input, function (error, stuffs) {
				//log.debug('error '+util.inspect(error));
				should.exists(error);
				error.domain.should.equal(ErrorDomain.DOMAIN_VALIDATE);
				error.code.should.equal(ErrorCode.VALIDATE_BLANK_INPUT);

				done();
			});
		});

		it('should error coz Datasource convert error', function (done) {
			var input = {
				site: 'zalora',
				path: 'path'
			};

			var datasourceFactory = test.importFileService.datasourceFactory;
			var mockDatasource = sinon.stub(datasourceFactory, 'createXMLDatasource')
																.returns(test.mockDatasource);
			
			var xmlDatasourceStub	= sinon.stub(mockDatasource(), 'convert')
																	.callsArgWith(0, 'error', null);

			test.importFileService.xml(input, function (error, stuffs) {
				//log.debug('error '+util.inspect(error));
				should.exists(error);

				mockDatasource.restore();

				xmlDatasourceStub.called.should.be.true;
				xmlDatasourceStub.restore();

				done();
			});
		});

		it('should success', function (done) {
			var input = {
				site: 'zalora',
				path: 'path'
			};
			// MockDatasourceFactory
			var datasourceFactory = test.importFileService.datasourceFactory;
			var mockDatasource = sinon.stub(datasourceFactory, 'createXMLDatasource')
																.returns(test.mockDatasource);
			
			var xmlDatasourceStub	= sinon.stub(mockDatasource(), 'convert')
																	.callsArgWith(0, null, {'stuff': 'stuff1'});
			var repositoryStub = sinon.stub(test.importFileService.repositories.tmpStuff, 'create')
																.callsArgWith(1, null, {});

			test.importFileService.xml(input, function (error, stuffs) {
				//log.debug('error '+util.inspect(error));
				should.not.exists(error);

				mockDatasource.restore();

				xmlDatasourceStub.called.should.be.true;
				xmlDatasourceStub.restore();

				repositoryStub.called.should.be.true;
				repositoryStub.restore();

				done();
			});
		});

	});
});
