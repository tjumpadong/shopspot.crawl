var async = require('async'),
    util = require('util'),
    path = require('path'),
    fs = require('fs');

var ScrapeService = require('../services/ScrapeService'),
    JsonObject = require('../modules/JsonObject');

var StringUtil = require('../modules/Util').StringUtil;

var ScrapeController = function () {
  this.scrapeService = new ScrapeService();
};

ScrapeController.prototype.scraping = function (req, res) {
  
  var scrapeService = this.scrapeService;
  var input = {
    uri: StringUtil.decode(req.query.uri),
    site: StringUtil.decode(req.query.site),
    page: req.query.page
  };

  async.waterfall([
    function examineSite (next) {
      if (input.site === 'lazada' || input.site === 'zalora') {
        next(null, scrapeService.scrapingXML);
      }
      else {
        next(null, scrapeService.scraping);
      }
    },
    function callScraping (callModule, next) {
      callModule(input, function (error, output) {
        if (error) {
          res.json(new JsonObject(error));
        }
        else {
          res.json(new JsonObject(null, output));
        }
      });
    }
  ]);
};

ScrapeController.prototype.scrapingFromFile = function (req, res) {
  
  var scrapeService = this.scrapeService;
  var input = {
    site: req.query.site,
  };

  scrapeService.scrapingFromFile(input, function (error, output) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, output));
    }
  });
};

ScrapeController.prototype.scrapingXML = function (req, res) {
  
  var scrapeService = this.scrapeService;
  var input = {
    uri: StringUtil.decode(req.query.uri),
    site: StringUtil.decode(req.query.site),
    fileName: req.query.fileName
  };

  scrapeService.scrapingXML(input, function (error, output) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, output));
    }
  });
};

ScrapeController.prototype.scraperMap = function (req, res) {
  var pathFile = path.join(__dirname, '..', 'resources', 'ScraperMapping.json');
  var scrapeMapping = JSON.parse(fs.readFileSync(pathFile));
  
  res.json(new JsonObject(null, scrapeMapping)); 
};

ScrapeController.prototype.fromXML = function (req, res) {
  
  var scrapeService = this.scrapeService;
  var input = {
    uri: StringUtil.decode(req.query.uri),
    site: StringUtil.decode(req.query.site)
  };

  scrapeService.fromXML(input, function (error, output) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, output));
    }
  });
};

module.exports = new ScrapeController();