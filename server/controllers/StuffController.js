var async = require('async'),
		util = require('util'),
		fs = require('fs'),
		path = require('path');

var StuffService = require('../services/StuffService'),
		repositories = require('../preloads/Repositories').instance(),
		JsonObject = require('../modules/JsonObject');

var StuffController = function () {
  var config = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '..', 'config.json')));
	this.stuffService = new StuffService(repositories, config);
}

StuffController.prototype.syncTmpToStuff = function (req, res) {
	var stuffService = this.stuffService;

	var input = {
		site: req.body.site
	};

	stuffService.syncTmpToStuff(input, function (error, success) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, success));
		}
	});
};

StuffController.prototype.updateProductionStuffId = function (req, res) {
	var stuffService = this.stuffService;

	var input = {
		stuffId: req.body.stuffId || '',
		productionStuffId: req.body.productionStuffId || ''
	};

	stuffService.updateProductionStuffId(input, function (error, stuff) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, stuff));
		}
	});
};

StuffController.prototype.findFromSite = function (req, res) {
  var stuffService = this.stuffService;

  var input = {
    site: req.query.site || '',
    page: req.query.page,
    limit: req.query.limit 
  };

  stuffService.findFromSite(input, function (error, stuffs, total) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, stuffs, total));
    }
  });
};

// not used now
StuffController.prototype.findFromProductionId = function (req, res) {
  var stuffService = this.stuffService;

  var input = {
    productionId: req.params.sid
  };

  stuffService.findFromProductionId(input, function (error, stuff) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, stuff));
    }
  });
};

StuffController.prototype.findFromId = function (req, res) {
  var stuffService = this.stuffService;

  var input = {
    id: req.params.sid
  };

  stuffService.findFromId(input, function (error, stuff) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      res.json(new JsonObject(null, stuff));
    }
  });
};

StuffController.prototype.updateFromRevision = function (req, res) {
	var stuffService = this.stuffService;

	var input = req.body;

	stuffService.updateFromRevision(input, function (error, stuff) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, stuff));
		}
	});
};

module.exports = new StuffController();
