var util = require('util');
var GroupCrawlDataDetailTransform = require('./GroupCrawlDataDetailTransform');
var CrawlDataDetailTransform = require('./CrawlDataDetailTransform');

var instance = null;

var TransformFactory = function () {
  instance = {
    groupCrawlDataDetail: new GroupCrawlDataDetailTransform(),
    crawlDataDetail: new CrawlDataDetailTransform()
  };
};

TransformFactory.prototype.get = function (name) {
  return instance[name];
};

module.exports = new TransformFactory();
