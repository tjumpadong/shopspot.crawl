var util = require('util');

var BaseTransform = require('./BaseTransform');

var CrawlDataDetailTransform = function () {
	BaseTransform.call(this);
};

util.inherits(CrawlDataDetailTransform, BaseTransform);

CrawlDataDetailTransform.prototype.transform = function (crawlData) {
	var tmp = crawlData;
	
	tmp.importedDate = new Date(tmp.importedDate).toDateString();
	tmp.description = encodeURIComponent(tmp.description);
	tmp.name = encodeURIComponent(tmp.name);

	if (tmp.tranform) {
		tmp.tranform.description = encodeURIComponent(tmp.tranform.description);	
	}

	return tmp;
};

module.exports = CrawlDataDetailTransform;