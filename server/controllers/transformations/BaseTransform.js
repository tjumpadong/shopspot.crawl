var BaseTransform = function () {

}

BaseTransform.prototype.apply = function(object) {

  if (object instanceof Array) {
    for (var key in object) {
      object[key] = this.transform(object[key]);
    }
  }
  else {
    object = this.transform(object);
  }

  return object;
}

module.exports = BaseTransform;