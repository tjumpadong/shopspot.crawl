var util = require('util');

var BaseTransform = require('./BaseTransform');

var GroupCrawlDataDetailTransform = function () {
  BaseTransform.call(this);  
};

util.inherits(GroupCrawlDataDetailTransform, BaseTransform);

GroupCrawlDataDetailTransform.prototype.transform = function (groupCrawData) {
	var tmp = groupCrawData;

	tmp.title = encodeURIComponent(tmp.title);
	tmp.description = encodeURIComponent(tmp.description);
	tmp.createdDate = new Date(tmp.createdDate).toDateString();

	return tmp;
};

module.exports = GroupCrawlDataDetailTransform;