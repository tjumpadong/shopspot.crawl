var async = require('async'),
		util = require('util');

var GroupCrawlDataService = require('../services/GroupCrawlDataService'),
		repositories = require('../preloads/Repositories').instance(),
		JsonObject = require('../modules/JsonObject'),
		TransformFactory = require('./transformations/TransformFactory');

var GroupCrawlDataController = function () {
	this.groupCrawlDataService = new GroupCrawlDataService(repositories);
}

GroupCrawlDataController.prototype.findFromUserId = function(req, res) {
	
	var groupCrawlDataService = this.groupCrawlDataService;
	var input = {
		userId: req.params.uid,
		page: req.query.page,
		limit: req.query.limit
	};

	if (input.userId === 'all') {
		delete input.userId;
	}

	groupCrawlDataService.findFromUserId(input, function (error, result) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			var transform = TransformFactory.get('groupCrawlDataDetail');
			var groups = transform.apply(result.groups);
			result.groups = groups;

			res.json(new JsonObject(null, result));
		}
	});
};

GroupCrawlDataController.prototype.removeFromGroupId = function(req, res) {
	
	var groupCrawlDataService = this.groupCrawlDataService;
	var input = {
		groupId: req.query.groupId,
	};

	groupCrawlDataService.removeFromGroupId(input, function (error, result) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, result));
		}
	});
};

GroupCrawlDataController.prototype.countItem = function (req, res) {
	
	var groupCrawlDataService = this.groupCrawlDataService;

	groupCrawlDataService.countItem(function (error, result) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, result));
		}
	});
};


module.exports = new GroupCrawlDataController();