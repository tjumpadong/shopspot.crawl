var async = require('async'),
		util = require('util'),
		fs = require('fs'),
		path = require('path');

var ImportFileService = require('../services/ImportFileService'),
		repositories = require('../preloads/Repositories').instance(),
		JsonObject = require('../modules/JsonObject');

var ImportFileController = function () {
	this.importFileService = new ImportFileService(repositories);
  var parserPath = path.join(__dirname, '..', '..', 'config.json');
  this.parserConfig = JSON.parse(fs.readFileSync(parserPath)).parsers;
}
/*
ImportFileController.prototype.csv = function(req, res) {
	
	var importFileService = this.importFileService;

	var input = {
		userId: req.body.userId,
		path: req.files.path ? req.files.path.path : req.body.path.path,
		fileName: req.files.path ? req.files.path.name : req.body.path.path,
		title: req.body.title ? decodeURIComponent(req.body.title) : '',
		description: req.body.description ? decodeURIComponent(req.body.description) : ''
	};

	importFileService.csv(input, function (error, crawlStuffs) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, crawlStuffs));
		}
	});
};*/

ImportFileController.prototype.csv = function(req, res) {
	
	var importFileService = this.importFileService;

	var input = {
		site: req.body.site ? decodeURIComponent(req.body.site) : '',
		path: req.files.path ? req.files.path.path : req.body.path.path
	};

	importFileService.csv(input, function (error, crawlStuffs) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, crawlStuffs));
		}
	});
};

ImportFileController.prototype.xml = function (req, res) {
	var importFileService = this.importFileService;

	var input = {
		site: req.body.site ? decodeURIComponent(req.body.site) : '',
		uri: req.body.uri ? decodeURIComponent(req.body.uri) : ''
	};

	importFileService.xml(input, function (error, createdTmpStuffs) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, createdTmpStuffs));
		}
	});
};

ImportFileController.prototype.parserMap = function (req, res) {
	var pathFile = path.join(__dirname, '..', 'resources', 'ParserMapping.json');
	var parserMapping = JSON.parse(fs.readFileSync(pathFile));
	
	res.json(new JsonObject(null, parserMapping));	
};

module.exports = new ImportFileController();
