var async = require('async'),
		util = require('util');

var CrawlDataService = require('../services/CrawlDataService'),
		repositories = require('../preloads/Repositories').instance(),
		JsonObject = require('../modules/JsonObject'),
		TransformFactory = require('./transformations/TransformFactory'),
		StringUtil = require('../modules/Util').StringUtil;
var log = require('winston').loggers.get('crawlData');

var CrawlDataController = function () {
	this.crawlDataService = new CrawlDataService(repositories);
}

CrawlDataController.prototype.get = function (req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
		crawlDataId: req.params.crawlDataId
	};

	crawlDataService.get(input, function (error, crawlData) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			var transform = TransformFactory.get('crawlDataDetail');
			crawlData = transform.apply(crawlData);
			res.json(new JsonObject(null, crawlData));
		}
	});
};

CrawlDataController.prototype.update = function (req, res) {
  
  var crawlDataService = this.crawlDataService;
  var input = {
    crawlDataId: req.params.crawlDataId,
    categories: req.body.categories,
    description: StringUtil.decode(req.body.description),
    externalLink: req.body.externalLink,
    images: req.body.images,
    market: req.body.market,
    owner: req.body.owner,
    price: req.body.price,
    tags: req.body.tags
  };
  var timeStart = new Date().getTime();
  log.debug('req update: '+util.inspect(input));

  crawlDataService.update(input, function (error, updatedCrawData) {
    if (error) {
      res.json(new JsonObject(error));
    }
    else {
      var timeEnd = new Date().getTime();
      log.debug('callback update: +' + (timeEnd - timeStart));
      var transform = TransformFactory.get('crawlDataDetail');
      updatedCrawData = transform.apply(updatedCrawData);
      res.json(new JsonObject(null, updatedCrawData));
    }
  });
};

CrawlDataController.prototype.updateStatus = function (req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
    crawlDataId: req.body.crawlDataId,
		status: req.body.status,
    stuffId: req.body.stuffId
	};
	var timeStart = new Date().getTime();
  log.debug('req update status: '+util.inspect(input));

	crawlDataService.updateStatus(input, function (error, updatedCrawData) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			var timeEnd = new Date().getTime();
			log.debug('callback update: +' + (timeEnd - timeStart));
      var transform = TransformFactory.get('crawlDataDetail');
      updatedCrawData = transform.apply(updatedCrawData);
			res.json(new JsonObject(null, updatedCrawData));
		}
	});
};

CrawlDataController.prototype.removeDatas= function (req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
		crawlDataIds: req.query.crawlDataIds
	};

	crawlDataService.removeDatas(input, function (error, result) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, result));
		}
	});
};

CrawlDataController.prototype.findFromUserId = function(req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
		userId: req.params.uid,
		page: req.query.page,
		limit: req.query.limit
	};

	crawlDataService.findFromUserId(input, function (error, results) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			var transform = TransformFactory.get('crawlDataDetail');
			results.crawlDatas = transform.apply(results.crawlDatas);	
			res.json(new JsonObject(null, results));
		}
	});
};

CrawlDataController.prototype.findFromGroupId = function(req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
		groupId: req.params.groupId,
		page: req.query.page,
		limit: req.query.limit
	};

	crawlDataService.findFromGroupId(input, function (error, results) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			var transform = TransformFactory.get('crawlDataDetail');
			results.crawlDatas = transform.apply(results.crawlDatas);	
			res.json(new JsonObject(null, results));
		}
	});
};

CrawlDataController.prototype.removeFromGroupId = function(req, res) {
	
	var crawlDataService = this.crawlDataService;
	var input = {
		groupId: req.query.groupId,
	};

	crawlDataService.removeFromGroupId(input, function (error, result) {
		if (error) {
			res.json(new JsonObject(error));
		}
		else {
			res.json(new JsonObject(null, result));
		}
	});
};

module.exports = new CrawlDataController();