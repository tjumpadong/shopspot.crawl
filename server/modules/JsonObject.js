var JsonObject = function (error, object, total) {
  var json = {};

  if (error) {
    json.action = false;
    json.error = error;
  }
  else {
    json.action = true;
    json.output = object;
    if (total) {
      json.total = total;
    }
  }

  return json;
}

module.exports = JsonObject;