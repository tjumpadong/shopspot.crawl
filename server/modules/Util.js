var Validate = {
	email: function (email) {
		var reg = /\S+@\S+\.\S+/;
    return reg.test(email);
	}
};

var StringUtil = {
  trim: function (word) {
    return word.replace(/^\s+|\s+$/, '');
  },
  split : function (seperator, string){
    var trimWord = StringUtil.trim(unescape(string));
    var result = [];
    if(trimWord.length > 0){
      var words = trimWord.split(seperator);
      for(var index in words){
        var word = words[index].trim();
        if(word.length > 0){
          result.push(word);
        }
      }
    }
    return result;   
  },
  decode: function (encodeStr) {
    var str = '';
    try {
      str = encodeStr ? decodeURIComponent(encodeStr) : '';
    }
    catch(ex) {
      str = encodeStr;
    }
    return str;
  }
};

exports.Validate = Validate;
exports.StringUtil = StringUtil;