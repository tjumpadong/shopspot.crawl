var log = require('stickyrice').loggers.get('crawl'),
		util = require('util'),
		fs = require('fs'),
		path = require('path'),
		async = require('async');

var ZaloraXMLTraversal = require('./ZaloraXMLTraversal');
var PricezaOutlet24XMLTraversal = require('./PricezaOutlet24XMLTraversal');

var XMLTraversalFactory = function () {
	this.uri = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '..', '..', 'resources', 'XMLMapping.json')));
	this.xmlTraversals = {
		zalora: ZaloraXMLTraversal,
		pricezaOutlet24: PricezaOutlet24XMLTraversal
	}
};

XMLTraversalFactory.prototype.createTraversal = function (site) {
	var TraversalClass = this.xmlTraversals[site];
	var defaultURI = this.uri[site];

	if (TraversalClass) {
		return new TraversalClass(defaultURI);	
	}
	else {
		return null;	
	}
};

module.exports = XMLTraversalFactory;