var async = require('async'),
    util = require('util');
var log = require('stickyrice').loggers.get('crawl');

var ErrorObject = require('../../Error').ErrorObject,
    ErrorCode =  require('../../Error').ErrorCode,
    ErrorDomain = require('../../Error').ErrorDomain;

var BaseXMLTraversal = function (uri) {
	this.uri = uri;
};

BaseXMLTraversal.prototype.refineData = function (data) {
  callback(new ErrorObject('Please define site map data',
                            ErrorDomain.DOMAIN_TRAVERSAL,
                            ErrorCode.TRAVERSAL_NOT_DEFINE_MAP));
};

BaseXMLTraversal.prototype.mapping = function (rowDatas, callback) {
	//log.debug(util.inspect(rowDatas));
	callback(new ErrorObject('Please define site map data',
                          ErrorDomain.DOMAIN_TRAVERSAL,
                          ErrorCode.TRAVERSAL_NOT_DEFINE_MAP));
};

BaseXMLTraversal.prototype.print = function () {
	log.info('BaseXMLTraversal : '+util.inspect(this));
};

BaseXMLTraversal.STUFF_PROPERTY = {
	PATH1: 'path1',
	PATH2: 'path2',
	PATH3: 'path3',
	PATH4: 'path4',
	NAME: 'name',
	DESCRIPTION: 'description',
	PRICE: 'price',
	SALE_PRICE: 'salePrice',
	DISCOUNT: 'discount',
	MAIN_CATEGORY: 'mainCategory',
	SUB_CATEGORY1: 'subCategory1',
	SUB_CATEGORY2: 'subCategory2',
	TAG: 'tags',
	SKU: 'sku',
	LINK: 'link',
	SITE: 'site'
}

module.exports = BaseXMLTraversal;