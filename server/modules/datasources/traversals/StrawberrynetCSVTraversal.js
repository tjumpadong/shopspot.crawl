var util = require('util'),
    fs = require('fs'),
    path = require('path');
var log = require('stickyrice').loggers.get('crawl');

var BaseCSVTraversal = require('./BaseCSVTraversal');

var StrawberrynetCSVTraversal = function () {
  BaseCSVTraversal.call(this, StrawberrynetCSVTraversal.CSV_MAPPING);
};

util.inherits(StrawberrynetCSVTraversal, BaseCSVTraversal);

StrawberrynetCSVTraversal.CSV_MAPPING = {
  'product image 1': 'path1',
  'product link url': 'link',
  'product name': 'name',
  'product description': 'description',
  'price': 'price',
  'sale price': 'salePrice',
  '% of sale': 'discount',
  'product main category': 'mainCategory',
  'product sub category 1': 'subCategory',
  'product sub category 2': 'subCategory2',
  'tag': 'tags',
  'sku': 'sku'
};

module.exports = StrawberrynetCSVTraversal;