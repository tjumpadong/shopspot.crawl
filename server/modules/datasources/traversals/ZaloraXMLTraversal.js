var util = require('util'),
    fs = require('fs'),
    path = require('path');
var log = require('stickyrice').loggers.get('crawl');

var BaseXMLTraversal = require('./BaseXMLTraversal');

var ZaloraXMLTraversal = function (uri) {
	BaseXMLTraversal.call(this, uri);
};

util.inherits(ZaloraXMLTraversal, BaseXMLTraversal);

var XMLMAP = {
  PRODUCT_ID: 'Product_Id',
  TITLE: 'Title',
  PRICE: 'Price',
  SALE_PRICE: 'Sale_Price',
  BRAND: 'Brand',
  URL: 'URL',
  CATEGORY: 'Category',
  IMAGE1: 'Image_URL_1',
  IMAGE2: 'Image_URL_2',
  IMAGE3: 'Image_URL_3',
  IMAGE4: 'Image_URL_4'
};

ZaloraXMLTraversal.prototype.refineData = function (data) {
  return data.data.record;
};

// mapping is interface
ZaloraXMLTraversal.prototype.mapping = function (rowDatas) {
  var STUFF_PROPERTY = BaseXMLTraversal.STUFF_PROPERTY;

	var stuffs = [];
	for (var key in rowDatas) {
    var xmlData = rowDatas[key];
    var stuff = {};
    var categories = xmlData[XMLMAP.CATEGORY].split('>');

    if (xmlData[XMLMAP.IMAGE1] !== '') {
      stuff[STUFF_PROPERTY.PATH1] = xmlData[XMLMAP.IMAGE1];
    }
    if (xmlData[XMLMAP.IMAGE2] !== '') {
      stuff[STUFF_PROPERTY.PATH2] = xmlData[XMLMAP.IMAGE2];
    }
    if (xmlData[XMLMAP.IMAGE3] !== '') {
      stuff[STUFF_PROPERTY.PATH3] = xmlData[XMLMAP.IMAGE3];
    }
    if (xmlData[XMLMAP.IMAGE4] !== '') {
      stuff[STUFF_PROPERTY.PATH4] = xmlData[XMLMAP.IMAGE4];
    }
    stuff[STUFF_PROPERTY.LINK] = xmlData[XMLMAP.URL];
    stuff[STUFF_PROPERTY.NAME] = xmlData[XMLMAP.TITLE];
    stuff[STUFF_PROPERTY.DESCRIPTION] = xmlData[XMLMAP.TITLE] + ' ' + xmlData[XMLMAP.BRAND];
    stuff[STUFF_PROPERTY.PRICE] = xmlData[XMLMAP.PRICE];
    if (xmlData[XMLMAP.SALE_PRICE] !== '') {
      try {
        var price = parseInt(xmlData[XMLMAP.Price]);
        var salePrice = parseInt(xmlData[XMLMAP.SALE_PRICE]);
        var discount = Math.round((price - salePrice) * 100 / price)

        stuff[STUFF_PROPERTY.DISCOUNT] = discount;
        stuff[STUFF_PROPERTY.SALE_PRICE] = salePrice;
      }
      catch (e) {}
    }
    if (categories[0]) {
      stuff[STUFF_PROPERTY.MAIN_CATEGORY] = categories[0];
    }
    if (categories[1]) {
      stuff[STUFF_PROPERTY.SUB_CATEGORY1] = categories[1];
    }
    if (categories[2]) {
      stuff[STUFF_PROPERTY.SUB_CATEGORY2] = categories[2];
    }
    stuff[STUFF_PROPERTY.SKU] = xmlData[XMLMAP.PRODUCT_ID];
    if (xmlData[XMLMAP.BRAND] !== '') {
      stuff[STUFF_PROPERTY.TAG] = xmlData[XMLMAP.BRAND];
    }
    stuffs.push(stuff);
  }

  return stuffs;
};

module.exports = ZaloraXMLTraversal;