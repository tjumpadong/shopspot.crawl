var async = require('async'),
    util = require('util'),
    _ = require('underscore');
var log = require('stickyrice').loggers.get('crawl');

var ErrorObject = require('../../Error').ErrorObject,
    ErrorCode =  require('../../Error').ErrorCode,
    ErrorDomain = require('../../Error').ErrorDomain;

var BaseCSVTraversal = function (CSV_MAPPING) {
	this.CSV_MAPPING = CSV_MAPPING || BaseCSVTraversal.CSV_MAPPING;
};

BaseCSVTraversal.prototype.mapping = function (stuffs) {

	var result = [];
	var CSV_MAPPING = this.CSV_MAPPING;
  //log.debug('BaseCSVTraversal mapping : '+util.inspect(stuffs));
	for (var key in stuffs) {
		var stuff = stuffs[key];

		var tmpStuff = {};
		var object = {};

		for (var key in stuff) {
			tmpStuff[key.trim().toLowerCase()] = stuff[key];	
		}
		// mapping
		_.map(CSV_MAPPING, function (value, key) {
			object[value] = tmpStuff[key];
		});
		result.push(object);
	}
	
	return result;
};

BaseCSVTraversal.prototype.print = function () {
	log.info('BaseCSVTraversal : '+util.inspect(this));
};

BaseCSVTraversal.prototype.validateHeader = function (crawlStuff, callback) {
	callback = callback || function () {};
	//log.info('BaseCSVTraversal : '+util.inspect(crawlStuffs))
	//log.info('mapping : '+util.inspect(this.mapping))

	var keys = [];
	var mapping = this.CSV_MAPPING;
	var keys = [];
	// lowercase key
	var tmpStuff = {};
	for (var key in crawlStuff) {
		tmpStuff[key.trim().toLowerCase()] = crawlStuff[key];
	}

	//check header
	_.map(mapping, function (value, key) {
		keys.push(key);
	});

	async.forEachSeries(keys, function (key, next) {
		if (!tmpStuff[key] && typeof tmpStuff[key] === 'undefined') {
			next(new ErrorObject('Invalid header',
										ErrorDomain.DOMAIN_VALIDATE,
										ErrorCode.VALIDATE_HEADER,
										key));
										
		}
		else {
			next();
		}
	}, function done (error) {
		if (error) {
			callback(error);
		}
		else {
			callback(null, true);		
		}
	});
};

BaseCSVTraversal.CSV_MAPPING = {
	'product image path1': 'path1',
	'product image path2': 'path2',
	'product image path3': 'path3',
	'product link url': 'link',
	'product name': 'name',
	'product description': 'description',
	'price': 'price',
	'sale price': 'salePrice',
	'% of sale': 'discount',
	'product main category': 'mainCategory',
	'product sub category': 'subCategory',
	'product sub category2': 'subCategory2',
	'tag': 'tags',
	'sku': 'sku'
};


module.exports = BaseCSVTraversal;