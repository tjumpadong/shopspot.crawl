var util = require('util'),
    fs = require('fs'),
    path = require('path');
var log = require('stickyrice').loggers.get('crawl');

var BaseXMLTraversal = require('./BaseXMLTraversal');

var PricezaOutlet24XMLTraversal = function (uri) {
	BaseXMLTraversal.call(this, uri);
};

util.inherits(PricezaOutlet24XMLTraversal, BaseXMLTraversal);

var XMLMAP = {
  PRODUCT_ID: 'id',
  NAME: 'name',
  PRICE: 'price',
  DESCRIPTION: 'description',
  CATEGORY_ID: 'category_id',
  CATEGORY_NAME: 'category_name',
  URL: 'url',
  IMAGE: 'image',
  MERCHANT_NAME: 'merchant_name'
};

PricezaOutlet24XMLTraversal.prototype.refineData = function (data) {
  return data.products.product;
};

// mapping is interface
PricezaOutlet24XMLTraversal.prototype.mapping = function (rowDatas) {
  var STUFF_PROPERTY = BaseXMLTraversal.STUFF_PROPERTY;

	var stuffs = [];
	for (var key in rowDatas) {
    var xmlData = rowDatas[key];
    var stuff = {};

    stuff[STUFF_PROPERTY.PATH1] = xmlData[XMLMAP.IMAGE];
    stuff[STUFF_PROPERTY.NAME] = xmlData[XMLMAP.NAME];
    stuff[STUFF_PROPERTY.PRICE] = xmlData[XMLMAP.PRICE];
    stuff[STUFF_PROPERTY.DESCRIPTION] = xmlData[XMLMAP.DESCRIPTION];
    stuff[STUFF_PROPERTY.MAIN_CATEGORY] = xmlData[XMLMAP.CATEGORY_NAME];
    stuff[STUFF_PROPERTY.LINK] = xmlData[XMLMAP.URL];
    stuff[STUFF_PROPERTY.SKU] = xmlData[XMLMAP.PRODUCT_ID];
    
    // different from base
    stuff['site'] = xmlData[XMLMAP.MERCHANT_NAME];

    stuffs.push(stuff);
  }

  return stuffs;
};

module.exports = PricezaOutlet24XMLTraversal;