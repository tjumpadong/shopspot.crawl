var log = require('stickyrice').loggers.get('crawl'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    async = require('async');

var StrawberrynetCSVTraversal = require('./StrawberrynetCSVTraversal');

var CSVTraversalFactory = function () {
  this.csvTraversals = {
    strawberrynet: StrawberrynetCSVTraversal
  };
};

CSVTraversalFactory.prototype.createTraversal = function (site) {
  var TraversalClass = this.csvTraversals[site];

  if (TraversalClass) {
    return new TraversalClass();
  }
  else {
    return null;
  }
};

module.exports = CSVTraversalFactory;