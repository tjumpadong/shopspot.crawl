var util = require('util'),
		async = require('async'),
		request = require('request'),
		parseString = require('xml2js').parseString;

var ErrorObject = require('../Error').ErrorObject,
    ErrorCode =  require('../Error').ErrorCode,
    ErrorDomain = require('../Error').ErrorDomain;

var log = require('stickyrice').loggers.get('crawl');
var XMLTraversalFactory = require('./traversals/XMLTraversalFactory');

var XMLDatasource = function (xmlTraversal, uri) {
	this.xmlTraversal = xmlTraversal;
	this.uri = uri;
};

XMLDatasource.prototype.convert = function (callback) {
	var self = this;
	var xmlTraversal = this.xmlTraversal;
	var uri = this.uri || xmlTraversal.uri;
	
	async.waterfall([
		function readFromURI (next) {
			self._fromURI(uri, next);
		},
		function parseData (xmlFile, next) {
			self._parse(xmlFile, next);
		},
		function traversal (rowDatas, next) {
			var stuffs = xmlTraversal.mapping(rowDatas);
			next(null, stuffs);
		}
	],
	function done (error, stuffs) {
		callback(error, stuffs);
	});
};

XMLDatasource.prototype._fromURI = function (uri, callback) {
	callback = callback || function () {};

	request(uri, function (error, response, body) {
		if (error) {
      callback(new ErrorObject('Cannot retrieve date',
                                ErrorDomain.DOMAIN_TRAVERSAL,
                                ErrorCode.TRAVERSAL_READ_URI_ERROR));
    }
    else {
      callback(null, body);
    }
	});
};

XMLDatasource.prototype._parse = function (xmlFile, callback) {
	callback = callback || function () {};
  var xmlTraversal = this.xmlTraversal;
	var options = {
    trim: true,
    explicitArray: false,
  };

  parseString(xmlFile, options, function (error, result) {
  	if (error) {
  		callback(new ErrorObject('xml2js parseString error',
                              ErrorDomain.DOMAIN_TRAVERSAL,
                              ErrorCode.TRAVERSAL_PARSE_STRING_ERROR))
  	}
  	else {
  		var records = xmlTraversal.refineData(result);
	    var rowDatas = [];
	    // case parse one object -> xml2js will not put it in array
	    if (records instanceof Array) {
	      rowDatas = records;
	    }
	    else {
	      rowDatas.push(records);
	    }

	    callback(null, rowDatas);
  	}
  });
};

module.exports = XMLDatasource;
