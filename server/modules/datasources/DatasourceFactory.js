var XMLDatasource = require('./XMLDatasource');
var CSVDatasource = require('./CSVDatasource');
var HTMLDatasource = require('./HTMLDatasource');

var DatasourceFactory = function () {};

DatasourceFactory.prototype.createXMLDatasource = function (traversal, uri) {
	return new XMLDatasource(traversal, uri);
};

DatasourceFactory.prototype.createCSVDatasource = function (traversal, csvFilePath) {
	return new CSVDatasource(traversal, csvFilePath);
};

DatasourceFactory.prototype.createHTMLDatasource = function () {
	return new HTMLDatasource();
};

module.exports = DatasourceFactory;