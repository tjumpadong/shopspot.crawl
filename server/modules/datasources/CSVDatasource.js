var util = require('util'),
		async = require('async')

var CsvManager = require('../CsvManager');

var ErrorObject = require('../Error').ErrorObject,
    ErrorCode =  require('../Error').ErrorCode,
    ErrorDomain = require('../Error').ErrorDomain;

var CSVDatasource = function (csvTraversal, csvFilePath) {
	this.csvManager = new CsvManager();
	this.csvFilePath = csvFilePath;
	this.csvTraversal = csvTraversal;
};

CSVDatasource.prototype.convert = function (callback) {
	var self = this;
	var csvTraversal = this.csvTraversal;
	var csvFilePath = this.csvFilePath;
	var csvManager = this.csvManager;

	async.waterfall([
    function validate (next) {
      if (!csvFilePath || csvFilePath === '') {
        next(new ErrorObject('Invalid csv path',
                              ErrorDomain.DOMAIN_VALIDATE,
                              ErrorCode.VALIDATE_BLANK_INPUT));
      }
      else {
        next();
      }
    },
		function csvToJson (next) {
			csvManager.readFromPath(csvFilePath, function (error, data) {
				next(error, data);
			});
		},
		function validateHeaderFile (stuffs, next) {
			var stuff = stuffs[0];
			csvTraversal.validateHeader(stuff, function (error, success) {
				next(error, stuffs);
			});
		},
		function mapping (stuffs, next) {
			var result = csvTraversal.mapping(stuffs);
			next(null, result);
		}
	],
	function done (error, stuffs) {
		callback(error, stuffs);
	});
};

module.exports = CSVDatasource;
