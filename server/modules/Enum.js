var Stuff = {
  status: {
    DELETED: 'deleted',
    UPDATED: 'updated',
    PUBLISHED: 'published',
    UNPUBLISHED: 'unpublished'
  }
};

exports.Stuff = Stuff;