var ErrorObject = function (message, domain, code, reference) {
	this.name = 'ErrorObject';
	this.message = message;
	this.domain = domain;
	this.code = code;
	this.reference = reference;
}

var ErrorCode = {
	DATABASE_ERROR: 00000,
	VALIDATE_PATH_FILE: 10000,
	VALIDATE_BLANK_INPUT: 10001,
	VALIDATE_TEXT_LENGTH: 10002,
	VALIDATE_HEADER: 10003,
	VALIDATE_INVALID_INPUT: 10004,

	PARSER_ERROR: 40000,

  SCRAPE_ERROR_GET_DATA: 50000,
  SCRAPE_ERROR_SAVE_FILE: 50001,

  TRAVERSAL_READ_URI_ERROR: 60001,
  TRAVERSAL_PARSE_STRING_ERROR: 60002,
  TRAVERSAL_NOT_DEFINE_MAP: 60003,

  STUFF_NOT_FOUND: 70000 
}

var ErrorDomain = {
	DOMAIN_DATABASE: 00000,
	DOMAIN_VALIDATE: 10000,
	DOMAIN_GROUPCRAWL: 20000,
	DOMAIN_CRAWL: 30000,
	DOMAIN_PARSER: 40000,
  DOMAIN_SCRAPE: 50000,
  DOMAIN_TRAVERSAL: 60000,
  DOMAIN_STUFF: 70000
}

exports.ErrorObject = ErrorObject;
exports.ErrorCode = ErrorCode;
exports.ErrorDomain = ErrorDomain;