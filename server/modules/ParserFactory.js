var util = require('util'),
		path = require('path'),
		fs = require('fs');

var parsers = require('../preloads/CrawlParsers').instance();
var pathFile = path.join(__dirname, '..', 'resources', 'ParserMapping.json');

var ParserFactory = function () {
	this.mapping = JSON.parse(fs.readFileSync(pathFile));
};

ParserFactory.prototype.instance = function (site) {

	//var name = ParserFactory.MAPPING[userId];
	var name = this.mapping[site.toLowerCase()];
	
	return parsers[name];
};

module.exports = ParserFactory;