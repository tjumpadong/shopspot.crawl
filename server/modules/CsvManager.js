var util = require('util'),
		async = require('async'),
		nodeCsv = require('csv');

var CsvManager = function () {
	//this.csv = nodeCsv();
	//can not use nodeCSV by singleton
};

CsvManager.prototype.readFromPath = function (path, callback) {
	callback = callback || function () {};
	var result = [];
	var names = null;
	
	nodeCsv().from(path)
		.transform(function (data, index) {
			if (index !== 0) {
				var tmp = {}

				for (var i=0; i<data.length; i++) {
					tmp[names[i]] = data[i];
				}
				result.push(tmp);
			}
			else {
				names = data;
			}
		})
		.on('end', function () {
			callback(null, result);
		});
};

module.exports = CsvManager;