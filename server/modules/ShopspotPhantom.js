var util = require('util'),
    async = require('async'),
    fs = require('fs'),
    _ = require('underscore');

var phantom = require('phantom');

var ShopspotPhantom = function (uri) {
  this.uri = uri
}

ShopspotPhantom.prototype.getPage = function (callback) {
  callback = callback || function () {};
  var uri = this.uri;

  async.waterfall([
    function createPhantom (next) {
      phantom.create(function (ph) {
        next(null, ph);
      })
    },
    function createPage (ph, next) {
      ph.createPage(function (page) {
        next(null, page);
      });
    },
    function openPage (page, next) {
      page.open(uri, function (status) {
        if (status === 'success') {
          next(null, page);
        }
        else {
          next('error');
        }
      });
    },
    function evaluatePage (page, next) {
      page.evaluate(function () {
        return document.body;
      },
      function (result) {
        next(null, result.innerHTML);
      });
    }
  ],
  function done (error, data) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, data);
    }
  });
};

module.exports = ShopspotPhantom;