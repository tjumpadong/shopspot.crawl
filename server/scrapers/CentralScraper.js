var util = require('util'),
    async = require('async'),
    request = require('request'),
    cheerio = require('cheerio');

var BaseScraper = require('./BaseScraper');

var ErrorObject = require('../modules/Error').ErrorObject,
    ErrorCode =  require('../modules/Error').ErrorCode,
    ErrorDomain = require('../modules/Error').ErrorDomain;

var CentralScraper = function () {
  BaseScraper.call(this);

  this.root = 'http://www.central.co.th';
};

util.inherits(CentralScraper, BaseScraper);

CentralScraper.prototype.getTotalPage = function (uri, callback) {
  callback = callback || function () {};

  request(uri, function (error, response, body) {
    if (error) {
      callback(new ErrorObject('Cannot retrieve date',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
    }
    else {
      var $ = cheerio.load(body);

      var pagePhase = [];
      var totalPage;
      var tmpText = $('.def_sort > .right > span').each(function () {
        pagePhase.push($(this).text());
      });

      try {
        // split with &nbsp
        var tmp = pagePhase[0].split(' ');
        totalPage = parseInt(tmp[tmp.length - 1]);
      }
      catch (e) {
        totalPage = 1;
      }

      callback(null, totalPage);
    }
  });
};

CentralScraper.prototype.getLink = function (uri, options, callback) {
  callback = callback || function () {};

  var links = [];
  var root = this.root;
  var page = options.page;

  uri = uri + '?page=' + page;

  request(uri, function (error, response, body) {
    if (error) {
      callback(new ErrorObject('Cannot retrieve date',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
    }
    else {
      var $ = cheerio.load(body);

      $('.img-hover > a').each(function () {
        var src = $(this).attr('href');
        links.push(root + src);
      });

      var totalPage = $('.def_sort > .right > span').text();
      if (!totalPage) {
        totalPage = 1;
      }

      callback(null, links, totalPage);
    }
  });
};

CentralScraper.prototype.getDetail = function (links, callback) {
  callback = callback || function () {};

  var mapping = this.getMapping();
  var stuffs = [];
  // links = links.slice(0, 5);
  var all = links.length;
  var count = 1;

  async.forEachSeries(links, function (link, subNext) {
    request(encodeURI(link), function (error, response, body) {
      try {
        var $ = cheerio.load(body);
        var stuff = {};

        var path = [],
            detail = [],
            discount = [];
            categories = [];

        var name = $('#PDetail h1').text();
        $('.imgGallery img').each(function () {
          path.push($(this).attr('src'));
        });
        $('#PDetail span').each(function () {
          detail.push($(this).text());
        });
        $('.icon_discountPD span').each(function () {
          discount.push($(this).text());
        });
        $('.container_24 .grid_24 > a').each(function () {
          categories.push($(this).text());
        });

        if (path.length === 0) {
          path.push($('a > #productImage').attr('src'));
        }

        if (path[0]) {
          stuff[mapping.path1] = 'http://' + path[0].slice(2, path[0].length).replace('tpimage', 'lpimage');
        }
        if (path[1]) {
          stuff[mapping.path2] = 'http://' + path[1].slice(2, path[1].length).replace('tpimage', 'lpimage');
        }
        if (path[2]) {
          stuff[mapping.path3] = 'http://' + path[2].slice(2, path[2].length).replace('tpimage', 'lpimage');
        }
        stuff[mapping.url] = link;
        stuff[mapping.name] = name.trim();
        stuff[mapping.description] = detail[0].trim().replace(/\r\n/g, '').replace(/\t/g, '');
        stuff[mapping.price] = detail[1].trim().replace(/[^.0-9]/g, '');
        if (detail[2]) {
          stuff[mapping.salePrice] = detail[2].trim().replace(/[^.0-9]/g, '');
        }
        if (discount[1]) {
          stuff[mapping.discount] = discount[1].trim().replace(/[^0-9]/g, '');
        }
        stuff[mapping.mainCategory] = categories[1].trim();
        if (categories[2]) {
          stuff[mapping.subCategory1] = categories[2].trim();
        }
        if (categories[3]) {
          stuff[mapping.subCategory2] = categories[3].trim();
        }

        stuffs.push(stuff);
        console.log(count++ + '/' + all);
        subNext();
      }
      catch (e) {
        console.log(count++ + '/' + all + ' error!');
        console.log(e);
        subNext();
      }
      
    });
  },
  function done (error) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuffs);
    }
  });
};

module.exports = CentralScraper;