var util = require('util'),
    async = require('async'),
    request = require('request');
    // restler = require('restler');

var BaseScraper = require('./BaseScraper');

var AsosScraper = function () {
  BaseScraper.call(this);

  this.root = 'http://www.asos.com';
};

util.inherits(AsosScraper, BaseScraper);

AsosScraper.prototype.getLink = function (uri, callback) {
  callback = callback || function () {};

  var links = [];
  var root = this.root;
// uri = 'http://www.asos.com/services/srvWebCategory.asmx/GetWebCategories'
  
  request.post({
    url: uri,
    form: {
      'cid':'11896',
      'strQuery':'',
      'strValues':'undefined',
      'currentPage':'1',
      'pageSize':'36',
      'pageSort':'-1',
      'countryId':'1',
      'maxResultCount':''
    }
  },
  function (error, data) {
    // console.log(data.body);
    callback();
  });
  /*restler.post(uri, {
    data: {
      'cid':'11896',
      'strQuery':'',
      'strValues':'undefined',
      'currentPage':'1',
      'pageSize':'36',
      'pageSort':'-1',
      'countryId':'1',
      'maxResultCount':''
    }
  }).on('complete', function onComplete (data) {
    console.log(data);
    callback('a');
  });*/
};

/*AsosScraper.prototype.getDetail = function (links, callback) {
  callback = callback || function () {};

  var mapping = this.getMapping();
  var root = this.root;
  var stuffs = [];
  links = links.slice(0, 5);
  var all = links.length;
  var count = 1;

  async.forEachSeries(links, function (link, subNext) {
    request(encodeURI(link), function (error, response, body) {
      // fs.writeFile('aaaa', body);
      var $ = cheerio.load(body);
      var stuff = {};

      var categories = [];

      var name = $('.txtB').text();
      var path = $('.Tn > a > img').attr('src');
      var description = $('.Description').text();
      var price = $('.Price').text();
      $('#Crumb > ul > li > a').each(function () {
        categories.push($(this).text());
      });

      stuff[mapping.path1] = root + path.slice(2, path.length);
      stuff[mapping.url] = link;
      stuff[mapping.name] = name.trim();
      stuff[mapping.description] = description.trim().replace(/\s{2,}/g, ' ');
      stuff[mapping.price] = price.trim().replace(/[^.0-9]/g, '');
      stuff[mapping.mainCategory] = categories[1].trim();
      if (categories[2]) {
        stuff[mapping.subCategory1] = categories[2].trim();
      }

      stuffs.push(stuff);
      console.log(count++ + '/' + all);
      subNext();
    });
  },
  function done (error) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuffs);
    }
  });
};*/

module.exports = AsosScraper;