var async = require('async'),
    request = require('request'),
    progress = require('request-progress'),
    util = require('util');

var ErrorObject = require('../modules/Error').ErrorObject,
    ErrorCode =  require('../modules/Error').ErrorCode,
    ErrorDomain = require('../modules/Error').ErrorDomain;

var BaseScraper = function () {
  // this.mapping = mapping;
};

BaseScraper.prototype.getField = function () {
  var mapping = BaseScraper.MAPPING
  var attrs = [];

  for (var key in mapping) {
    attrs.push(mapping[key]);
  }

  return attrs;
};

BaseScraper.prototype.getMapping = function () {
  return BaseScraper.MAPPING
};

BaseScraper.prototype.getXML = function (uri, callback) {
  callback = callback || function () {};

  async.waterfall([
    function getFile (next) {
      progress(request(uri, function (error, response, body) {
        if (error) {
          next(new ErrorObject('Cannot retrieve date',
                                    ErrorDomain.DOMAIN_SCRAPE,
                                    ErrorCode.SCRAPE_ERROR_GET_DATA));
        }
        else {
          next(null, body);
        }
      }), {
        throttle: 2000,
        delay: 1000
      }).on('progress', function (state) {
        console.log(state.received + '/' + state.total + ' (' + state.percent + '%)');
      });
    }
  ],
  function done (error, xmlFile) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, xmlFile);
    }
  });
};

BaseScraper.MAPPING = {
  'path1': 'Product Image Path1',
  'path2': 'Product Image Path2',
  'path3': 'Product Image Path3',
  'path4': 'Product Image Path4',
  'url': 'Product link url',
  'name': 'Product Name',
  'description': 'Product Description',
  'price': 'Price',
  'salePrice': 'Sale Price',
  'discount': '% of Sale',
  'mainCategory': 'Product Main Category',
  'subCategory1': 'Product Sub Category',
  'subCategory2': 'Product Sub Category2',
  'tag': 'Tag',
  'sku': 'SKU',
  'link': 'Affiliate Link'
};

module.exports = BaseScraper;