var util = require('util'),
    async = require('async'),
    request = require('request'),
    cheerio = require('cheerio');

var TIMEOUT = 8000;

var BaseScraper = require('./BaseScraper');

var ErrorObject = require('../modules/Error').ErrorObject,
    ErrorCode =  require('../modules/Error').ErrorCode,
    ErrorDomain = require('../modules/Error').ErrorDomain;

var ShopspotPhantom = require('../modules/ShopspotPhantom');

var IndexScraper = function () {
  BaseScraper.call(this);

  this.root = 'http://www.indexlivingmall.com';
};

util.inherits(IndexScraper, BaseScraper);

IndexScraper.prototype.getTotalPage = function (uri, callback) {
  callback = callback || function () {};

  uri = uri.replace('list', 'ajax-list').replace('?', '?startrow=0&')

  request(uri, function (error, response, body) {
    if (error) {
      callback(new ErrorObject('Cannot retrieve date',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
    }
    else {
      var $ = cheerio.load(body);

      var totalPage;
      var pagePhase = $('.PageNo').text();

      try {
        var tmp = pagePhase.match(/[0-9]+/g);
        totalPage = parseInt(tmp[tmp.length - 1]);
      }
      catch (e) {
        totalPage = 1;
      }
      
      callback(null, totalPage);
    }
  });
};

IndexScraper.prototype.getLink = function (uri, options, callback) {
  callback = callback || function () {};

  var links = [];
  var start = (options.page - 1) * 15;
  var root = this.root;
  uri = uri.replace('list', 'ajax-list').replace('?', '?startrow=' + start + '&')

  var phantom = new ShopspotPhantom(uri);

  var html = phantom.getPage(function (error, html) {
    setTimeout(function () {
      if (error) {
        callback(new ErrorObject('Cannot retrieve date',
                                  ErrorDomain.DOMAIN_SCRAPE,
                                  ErrorCode.SCRAPE_ERROR_GET_DATA));
      }
      else {
        var $ = cheerio.load(html);

        $('.ProductBox > .Tn > a').each(function () {
          var src = $(this).attr('href');

          links.push(root + src.slice(2, src.length));
        });

        callback(null, links);
      }
    }, TIMEOUT);
  });
};

IndexScraper.prototype.getDetail = function (links, callback) {
  callback = callback || function () {};

  var mapping = this.getMapping();
  var root = this.root;
  var stuffs = [];
  var all = links.length;
  var count = 1;

  async.forEachSeries(links, function (link, subNext) {
    request(encodeURI(link), function (error, response, body) {
      var $ = cheerio.load(body);
      var stuff = {};

      var categories = [];

      var name = $('.txtB').text();
      var path = $('.Tn > a > img').attr('src');
      var description = $('.Description').text();
      var price = $('.Price').text();
      $('#Crumb > ul > li').each(function () {
        categories.push($(this).text());
      });

      stuff[mapping.path1] = root + path.slice(2, path.length);
      stuff[mapping.url] = link;
      stuff[mapping.name] = name.trim();
      stuff[mapping.description] = description.trim().replace(/\s{2,}/g, ' ');
      stuff[mapping.price] = price.trim().replace(/[^.0-9]/g, '');
      stuff[mapping.mainCategory] = categories[1].trim();
      if (categories[2]) {
        stuff[mapping.subCategory1] = categories[2].trim();
      }
      if (categories[3]) {
        stuff[mapping.subCategory2] = categories[3].trim();
      }

      stuffs.push(stuff);
      console.log(count++ + '/' + all);
      subNext();
    });
  },
  function done (error) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuffs);
    }
  });
};

module.exports = IndexScraper;