var util = require('util'),
    fs = require('fs'),
    path = require('path');

var parseString = require('xml2js').parseString,
    xml2js = require('xml2js');

var BaseScraper = require('./BaseScraper');

var ZaloraScraper = function () {
  BaseScraper.call(this);
};

util.inherits(ZaloraScraper, BaseScraper);

var XMLMap = {
  PRODUCT_ID: 'Product_Id',
  TITLE: 'Title',
  PRICE: 'Price',
  SALE_PRICE: 'Sale_Price',
  BRAND: 'Brand',
  URL: 'URL',
  CATEGORY: 'Category',
  IMAGE1: 'Image_URL_1',
  IMAGE2: 'Image_URL_2',
  IMAGE3: 'Image_URL_3',
  IMAGE4: 'Image_URL_4'
};

ZaloraScraper.prototype.getDetail = function (xmlFile, callback) {
  callback = callback || function () {};

  var mapping = this.getMapping();
  var parser = new xml2js.Parser();
  var options = {
    trim: true,
    explicitArray: false,

  };

  parseString(xmlFile, options, function (error, result) {
    var records = result.data.record;
    var stuffs = [];
    var xmlStuffs = [];
    // case parse one object -> xml2js will not put it in array
    if (records instanceof Array) {
      xmlStuffs = records;
    }
    else {
      xmlStuffs.push(records);
    }
    
    for (var key in xmlStuffs) {
      var xmlStuff = xmlStuffs[key];
      var stuff = {};
      if (xmlStuff[XMLMap.IMAGE1] !== '') {
        stuff[mapping.path1] = xmlStuff[XMLMap.IMAGE1];
      }
      if (xmlStuff[XMLMap.IMAGE2] !== '') {
        stuff[mapping.path2] = xmlStuff[XMLMap.IMAGE2];
      }
      if (xmlStuff[XMLMap.IMAGE3] !== '') {
        stuff[mapping.path3] = xmlStuff[XMLMap.IMAGE3];
      }
      if (xmlStuff[XMLMap.IMAGE4] !== '') {
        stuff[mapping.path4] = xmlStuff[XMLMap.IMAGE4];
      }
      stuff[mapping.url] = xmlStuff[XMLMap.URL];
      stuff[mapping.name] = xmlStuff[XMLMap.TITLE];
      stuff[mapping.description] = xmlStuff[XMLMap.TITLE] + ' ' + xmlStuff[XMLMap.BRAND];
      stuff[mapping.price] = xmlStuff[XMLMap.PRICE];
      if (xmlStuff[XMLMap.SALE_PRICE] !== '') {
        try {
          var price = parseInt(xmlStuff[XMLMap.Price]);
          var salePrice = parseInt(xmlStuff[XMLMap.SALE_PRICE]);
          var discount = Math.round((price - salePrice) * 100 / price)

          stuff[mapping.discount] = discount;
          stuff[mapping.salePrice] = salePrice;
        }
        catch (e) {}
      }
      stuff[mapping.mainCategory] = xmlStuff[XMLMap.CATEGORY];
      stuff[mapping.sku] = xmlStuff[XMLMap.PRODUCT_ID];
      if (xmlStuff[XMLMap.BRAND] !== '') {
        stuff[mapping.tag] = xmlStuff[XMLMap.BRAND];
      }
      stuffs.push(stuff);
    }

    callback(null, stuffs);
  });
  
};

module.exports = ZaloraScraper;