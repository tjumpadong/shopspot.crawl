var util = require('util'),
    xml2js = require('xml2js'),
    fs = require('fs'),
    path = require('path');

var BaseScraper = require('./BaseScraper');

var LazadaScraper = function () {
  BaseScraper.call(this);
};

util.inherits(LazadaScraper, BaseScraper);

LazadaScraper.prototype.getDetail = function (xmlFile, callback) {
  callback = callback || function () {};

  var mapping = this.getMapping();
  var parser = new xml2js.Parser();
  // var dataFilePath = path.join(__dirname, '..', 'resources', 'xml', fileName + '.xml');

  // fs.readFile(dataFilePath, function (error, data) {
    parser.parseString(xmlFile, function (error, result) {
      var oldStuffs = result.Products.Product;
      var stuffs = [];

      for (var key in oldStuffs) {
        var oldStuff = oldStuffs[key];
        var stuff = {};
        if (oldStuff['zoom_picture_url'][0] !== '') {
          stuff[mapping.path1] = oldStuff['zoom_picture_url'][0];
        }
        else {
          stuff[mapping.path1] = oldStuff['picture_url'][0];
        }
        if (oldStuff['image_url_2'][0] !== '') {
          stuff[mapping.path1] = oldStuff['image_url_2'][0];
        }
        if (oldStuff['image_url_3'][0] !== '') {
          stuff[mapping.path1] = oldStuff['image_url_3'][0];
        }
        stuff[mapping.url] = oldStuff['URL'][0];
        stuff[mapping.name] = oldStuff['product_name'][0];
        stuff[mapping.description] = oldStuff['description'][0];
        stuff[mapping.price] = oldStuff['price'][0];
        if (oldStuff['discount'][0] !== '') {
          try {
            var price = parseInt(oldStuff['price'][0]);
            var salePrice = parseInt(oldStuff['discounted_price'][0]);
            var discount = Math.round((price - salePrice) * 100 / price)

            stuff[mapping.discount] = discount;
            stuff[mapping.salePrice] = salePrice;
          }
          catch (e) {}
        }
        stuff[mapping.mainCategory] = oldStuff['cat_m'][0];
        stuff[mapping.sku] = oldStuff['simple_sku'][0];
        if (oldStuff['brand'][0] !== '') {
          stuff[mapping.tag] = oldStuff['brand'][0];
        }

        stuffs.push(stuff);
      }

      callback(null, stuffs);
    });
  // });
};

module.exports = LazadaScraper;