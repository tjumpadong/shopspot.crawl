var util = require('util'),
		async = require('async');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var CrawlDataTranformValidator = function () {};

CrawlDataTranformValidator.prototype.validate = function (input, callback) {
	callback = callback || function () {};

	async.waterfall([
		function validateCategories (next) {
			if (!input.categories || input.categories.length === 0) {
				next(new ErrorObject('Categories should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateDescription (next) {
			if (!input.description) {
				next(new ErrorObject('Description should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateExternalLink (next) {
			if (!input.externalLink) {
				next(new ErrorObject('Link should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateImages (next) {
			if (!input.images || input.images.length === 0) {
				next(new ErrorObject('Image should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}	
		},
		function validateMarket (next) {
			if (!input.market) {
				next(new ErrorObject('Market should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateOwner (next) {
			if (!input.owner) {
				next(new ErrorObject('Owner should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validatePrice (next) {
			if (!input.price) {
				next(new ErrorObject('Price should not blank',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else if (isNaN(input.price)) {
				next(new ErrorObject('Price should be numbers',
									ErrorDomain.DOMAIN_VALIDATE,
									ErrorCode.VALIDATE_INVALID_INPUT));
			}
			else {
				next();
			}
		}
	],
	function done (error) {
		if (error) {
			callback(error);
		}
		else {
			callback(null, true);
		}
	});
};

module.exports = CrawlDataTranformValidator;