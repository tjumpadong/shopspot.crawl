var util = require('util'),
		async = require('async');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var ImportFileValidator = function () {

};

ImportFileValidator.prototype.validate = function(input, callback) {
	callback = callback || function () {};

	async.waterfall([
		function validatePath (next) {
			if (!input.path) {
				next(new ErrorObject('Path should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_PATH_FILE));
			}
			else {
				next();
			}
		},
		function validateOwner (next) {
			if (!input.userId) {
				next(new ErrorObject('userId should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateGroupName (next) {
			if (!input.title) {
				next(new ErrorObject('Title should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		},
		function validateDiscription (next) {
			if (!input.description) {
				next(new ErrorObject('Description should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else if (input.description.length > 500) {
				next(new ErrorObject('Description should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_TEXT_LENGTH));
			}
			else {
				next();
			}
		},
		function validateParser (next) {
			if (!input.parser) {
				next(new ErrorObject('Parser should not be blank',
								ErrorDomain.DOMAIN_VALIDATE,
								ErrorCode.VALIDATE_BLANK_INPUT));
			}
			else {
				next();
			}
		}
	],
	function done (error) {
		if (error) {
			callback(error);
		}
		else {
			callback(null, true);
		}
	});
};

module.exports = ImportFileValidator;