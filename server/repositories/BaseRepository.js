var assert = require('assert'),
    async = require('async'),
    util = require('util'),
    ObjectID = require('mongodb').ObjectID,
    log = require('winston').loggers.get('shopspot');

var BaseRepository = function (client, table, options) {
  var self = this;
  
  options = options || {};
  self.modelClass = options.modelClass;
  self.index = options.index;
  
  var dbOptions = {};

  if (options.capped) {
    dbOptions.capped = options.capped;
  }
  if (options.max) {
    dbOptions.max = options.max; 
  }

  client.collection(table, dbOptions, function (error, collection) {
    self.collection = collection;

    if (self.index) {
      self.index(collection);
    }
  });
}

BaseRepository.prototype.create = function (object, callback) {
  callback = callback || function () {};
  
  var collection = this.collection;
  assert.ok(collection, 'Collection must create before call repository');
  
  var modelClass = this.modelClass;
  
  collection.insert(object, { safe: true }, function (error, output) {
    
    var createdObject = output[0];
    if (modelClass) {
      createdObject = new modelClass(output[0]);
    }
    
    //log.debug ('Created object: ' + util.inspect(createdObject));
    
    callback(error, createdObject);
    
  });
}

BaseRepository.prototype.count = function (callback) {
  callback = callback || function () {};
  
  var collection = this.collection;
  assert.ok(collection, 'Collection must create before call repository');
  
  collection.count(function (error, total) {
    
    //log.debug ('Count total object: ' + total);
    callback (error, total);
    
  });
}

BaseRepository.prototype.get = function (id, callback) {
  callback = callback || function () {};
  
  var collection = this.collection;
  assert.ok(collection, 'Collection must create before call repository');
  
  var modelClass = this.modelClass;
  
  if (id) {
    collection.findOne({ _id: new ObjectID(id.toString()) }, function (error, object) {
    
      var gotObject = object;
      if (modelClass) {
        gotObject = new modelClass(object);
      }
    
      //log.debug ('Get an object: ' + util.inspect(gotObject));
      callback (error, gotObject);
      
    });
  }
  else {
    callback('can not get this object');
   /* callback(new FrameworkError('Invalid object id',
                                ErrorDomain.DOMAIN_DATABASE,
                                ErrorCode.DATABASE_ERROR,
                                id));*/
  }
  
}

BaseRepository.prototype.update = function (object, callback) {
  callback = callback || function () {};

  //log.info (object);
  
  var collection = this.collection;
  assert.ok(collection, 'Collection must create before call repository');
  assert.ok(object._id, 'Object must have an id');
  
  var modelClass = this.modelClass;
  
  collection.save(object, function (error) {
    
    var updatedObject = object;
    if (modelClass) {
      updatedObject = new modelClass(object);
    }
    
    //log.debug ('Updated object: ' + util.inspect(updatedObject));
    callback (error, updatedObject);
    
  });
  
}

BaseRepository.prototype.remove = function (id, callback) {
  callback = callback || function () {};
  
  var collection = this.collection;
  assert.ok(collection, 'Collection must create before call repository');

  collection.remove({ '_id': new ObjectID(id.toString()) }, function (error, object) {
    
    //log.debug ('Removed object: ' + object);

    if (error) {
      callback (error, false);
    }
    else {
      callback (null, true);  
    }
  });
}

exports.BaseRepository = BaseRepository;