var util =require('util'),
		async = require('async');

var BaseRepository = require('./BaseRepository').BaseRepository;

var CrawlDataRepository = function (client) {
	BaseRepository.call(this, client, 'crawlDatas');
};

util.inherits(CrawlDataRepository, BaseRepository);

CrawlDataRepository.prototype.findFromUserId = function (userId, offset, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var limit = parseInt(offset.limit || 10);
	var skip = parseInt(offset.page || 0) * limit;
	var query = {};

	if (userId) { query.userId = userId; }

	async.parallel({
		count: function (next) {
			collection.count(query, function (error, total) {
				if (total == 0) {
					callback(null, 0, []);
				}
				else {
					next(error, total);
				}
			});
		},
		get: function (next) {
			collection.find(query)
								.skip(skip)
								.limit(limit)
								.sort({ _id: -1})
								.toArray(function (error, crawlDatas) {
									next(error, crawlDatas);
								});
		}
	},
	function done (error, results) {
		callback(error, results.count, results.get);
	});
};

CrawlDataRepository.prototype.findFromGroupId = function (groupId, offset, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var limit = parseInt(offset.limit || 10);
	var skip = parseInt(offset.page || 0) * limit;
	var query = {
		groupId: groupId
	};

	async.parallel({
		count: function (next) {
			collection.count(query, function (error, total) {
				if (total == 0) {
					callback(null, 0, []);
				}
				else {
					next(error, total);
				}
			});
		},
		get: function (next) {
			collection.find(query)
								.skip(skip)
								.limit(limit)
								.sort({ _id: -1})
								.toArray(function (error, crawlDatas) {
									next(error, crawlDatas);
								});
		}
	},
	function done (error, results) {
		callback(error, results.count, results.get);
	});
};

CrawlDataRepository.prototype.removeFromGroupId = function (groupId, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var query = {
		'groupId': groupId
	};

	collection.remove(query, function (error, result) {
		callback(error, result);
	});
};

module.exports = CrawlDataRepository;