var util =require('util'),
		async = require('async');

var BaseRepository = require('./BaseRepository').BaseRepository;

var StuffRepository = function (client) {
	BaseRepository.call(this, client, 'stuffs');
};

util.inherits(StuffRepository, BaseRepository);

StuffRepository.prototype.findFromSKU = function (sku, userId, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var query = {
		sku: sku,
		owner: userId
	};

	collection.find(query)
						.toArray(function (error, stuffs) {
              callback(error, stuffs[0] || null);
						});
};

StuffRepository.prototype.findFromOwner = function (userId, options, callback) {
	var collection = this.collection;
	var limit = parseInt(options.limit || 10);
	var skip = parseInt(options.page || 0) * limit;
  var countStuff = options.countStuff;
	var query = {
		owner: userId
	};

  var cursor = collection.find(query);

  async.waterfall([
    function countStuffFromSite (next) {
      if (countStuff) {
        cursor.count(function (error, total) {
          next(error, total);
        });
      }
      else {
        next(null, -1);
      }
    },
    function listStuffFromSite (total, next) {
      cursor.skip(skip)
            .limit(limit)
            .toArray(function (error, stuffs) {
              next(error, stuffs, total);
            });
    }
  ],
  function done (error, stuffs, total) {
    callback(error, stuffs, total);
  });
};

StuffRepository.prototype.findFromProductionId = function (id, callback) {
  var collection = this.collection;

  collection.find({ productionId: id })
            .toArray(function (error, stuffs) {
              callback(error, stuffs[0] || null);
            });
};

/*StuffRepository.prototype.updateFromProductionId = function (id, callback) {
  var collection = this.collection;

  collection.find({ productionId: id })
            .toArray(function (error, stuffs) {
              callback(error, stuffs[0] || null);
            });
};*/

module.exports = StuffRepository;