var util =require('util'),
		async = require('async');

var BaseRepository = require('./BaseRepository').BaseRepository;

var GroupCrawlDataRepository = function (client) {
	BaseRepository.call(this, client, 'groupCrawlDatas');
};

util.inherits(GroupCrawlDataRepository, BaseRepository);

GroupCrawlDataRepository.prototype.findFromUserId = function(userId, offset, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var limit = parseInt(offset.limit || 10);
	var skip = parseInt(offset.page || 0) * limit;
	var query = {};

	if (userId) { query.userId = userId; }
	
	async.parallel({
		count: function (next) {
			collection.count(query, function (error, total) {
				if (total == 0) {
					callback(null, 0, []);
				}
				else {
					next(error, total);
				}
			});
		},
		get: function (next) {
			collection.find(query)
								.skip(skip)
								.limit(limit)
								.sort({ _id: -1})
								.toArray(function (error, groups) {
									next(error, groups);
								});
		}
	},
	function done (error, results) {
		callback(error, results.count, results.get);
	});
	
};

GroupCrawlDataRepository.prototype.countItem = function (callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var pipeline = [{
		$group: {
			_id: null,
			total: { $sum: '$amount' }			
		}
	}];

	collection.aggregate(pipeline, {}, function (error, totals) {
		callback(error, totals)
	});
};

module.exports = GroupCrawlDataRepository;