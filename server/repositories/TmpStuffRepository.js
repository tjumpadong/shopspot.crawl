var util =require('util'),
		async = require('async');

var BaseRepository = require('./BaseRepository').BaseRepository;

var TmpStuffRepository = function (client) {
	var options = {
		capped: true,
		max: 100000
	};

	BaseRepository.call(this, client, 'tmpStuffs', options);
};

util.inherits(TmpStuffRepository, BaseRepository);

TmpStuffRepository.prototype.findFromSite = function (site, options, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var limit = parseInt(options.limit || 10000);
	var skip = parseInt(options.page || 0) * limit;
	var countTmpStuff = options.countTmpStuff;
  var query = {
    site: site,
    read: { $ne: true }
  };
	
	var cursor = collection.find(query);

	async.waterfall([
		function callTmpFromSite (next) {
			if (countTmpStuff) {
        cursor.count(function (error, total) {
          next(error, total);
        });
      }
      else {
        next(null, -1);
      }
		},
		function listTmpFormType (total, next) {
			cursor.skip(skip)
						.limit(limit)
						.toArray(function (error, stuffs) {
							next(error, stuffs, total);
						});		
		}
	],
	function done (error, stuffs, total) {
		callback(error, stuffs, total);
	});
	
};

TmpStuffRepository.prototype.findFromSKU = function (sku, site, callback) {
	callback = callback || function () {};

	var collection = this.collection;
	var query = {
		sku: sku,
		site: site
	};

	collection.findOne(query, function (error, stuff) {
		callback(error, stuff);
	});
};

module.exports = TmpStuffRepository;