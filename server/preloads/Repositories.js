var log = require('winston').loggers.get('shopspot'),
		util = require('util'),
		mongodb = require('mongodb');

var Database = require('./Database');
var CrawlDataRepository = require('../repositories/CrawlDataRepository');
var GroupCrawlDataRepository = require('../repositories/GroupCrawlDataRepository');
var StuffRepository = require('../repositories/StuffRepository');
var TmpStuffRepository = require('../repositories/TmpStuffRepository');
var _repositories;

module.exports = {
	setup: function (config, done) {
	  if (!_repositories) {
	    var client = Database.client();

	    _repositories = {
	      crawlData: new CrawlDataRepository(client),
	      groupCrawlData: new GroupCrawlDataRepository(client),
	      stuff: new StuffRepository(client),
	      tmpStuff: new TmpStuffRepository(client)
	    };
	  }
	  log.info('Repositories setup completed');
	  done(); 	
	},

	instance: function () {
		return _repositories;
	}
}
