var log = require('winston').loggers.get('shopspot'),
		util = require('util'),
		fs = require('fs'),
		path = require('path'),
		async = require('async');

var pathDir = path.join(__dirname, '..', 'resources', 'categories');

var WearYouWant = require('../parsers/WearYouWantParser');
var Index = require('../parsers/IndexParser');
var Central = require('../parsers/CentralParser');
var Zalora = require('../parsers/ZaloraParser');
var PricezaOutlet24 = require('../parsers/PricezaOutlet24Parser');
var Lazada = require('../parsers/LazadaParser');
var Strawberrynet = require('../parsers/StrawberrynetParser');
var crawlParsers;

module.exports = {
	setup: function (config, done) {
	  if (!crawlParsers) {
	  	this.getCategory(function (error, categories) {
	  		//console.log('categories : '+ util.inspect(categories));
	  		if (error) {
	  			log.info('CrawlParsers get category fail.');
	  		}
	  		else {
	  			var parsers = config.parsers;
	  			crawlParsers = {
		       	wearyouwant: new WearYouWant(categories, parsers),
            index: new Index(categories, parsers),
            central: new Central(categories, parsers),
            zalora: new Zalora(categories, parsers),
            lazada: new Lazada(categories, parsers),
            strawberrynet: new Strawberrynet(categories, parsers),
            pricezaOutlet24: new PricezaOutlet24(categories, parsers)
		     	};
		     	
		     	log.info('crawlParsers setup completed.');
		     	done();
	  		}
	  	});
	  }
	  else {
	  	log.info('crawlParsers have instance');
	  	done();
	  }
	},

	getCategory: function (callback) {
		callback = callback || function () {};
		var categories = {};

		async.waterfall([
			function getFiles (next) {
				fs.readdir(pathDir, function (error, files) {
  				next(error, files)
  			});
			},
			function getCategoryFromFile (files, next) {
				async.forEachSeries(files, function (file, subNext) {
					var pathFile = path.join(pathDir, file);
					var category = JSON.parse(fs.readFileSync(pathFile));
					var name = file.replace('.json','');
					categories[name] = category;
					subNext();
				},
				function done () {
					next()
				});
			}
		],
		function done (error) {
			callback(error, categories);
		});
	},

	instance: function () {
		return crawlParsers;
	},

	get: function (site) {
		return crawlParsers[site];
	}
}
