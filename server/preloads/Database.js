var log = require('winston').loggers.get('shopspot'),
		util = require('util'),
		mongodb = require('mongodb'),
		async = require('async');

var instance = null;

module.exports = {
	
	setup: function (config, done) {
		
		this.setupDB(config.database, 'admin', function (error, openedClient) {
			if (!error) {
				instance = openedClient;
				done();
			}
		});
	},

	setupDB: function (mongoConfig, message, callback) {
		callback = callback || function () {};

		if (mongoConfig.server) {
			var server = new mongodb.Server(mongoConfig.server, mongoConfig.port, mongoConfig.options);	
		}
		else {
			log.error('Invalid mongo config');
		}
		
		var database = new mongodb.Db(mongoConfig.database.name, server, mongoConfig.database.options);

		database.open(function (error, openedClient) {
			if (error) {
				var errorMessage = 'Can not open database for '+message+'. May be aws security group';
				log.error(errorMessage);
				process.exit(-1);
				callback(errorMessage);
			}
			else {
				var username = mongoConfig.authentication.username;
      	var password = mongoConfig.authentication.password;	

      	if (username || password) {
	        database.authenticate(username, password,
	          function (error, data) {

	            if (error) {
	              logger.error ('Database authentication fail');
	              process.exit(-1);
	            }
	            else {
	              logger.info ('Database '+message+'authenticate success');
	              callback(null, openedClient);
	            }

	          });
	      }
	      else {
	      	log.info('Database '+message+' setup completed: on Port : '+mongoConfig.port);
	        callback(null, openedClient);
	      }
			}
		});
	},

	client: function () {
		return instance;
	}
}
