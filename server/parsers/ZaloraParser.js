var util = require('util'),
    async = require('async');

var BaseParser = require('./BaseParser');

var ZaloraParser = function (categories, parsers) {
  var site = 'zalora';
  this.site = site;
  this.category = categories ? categories[site] : '';
  this.userId = parsers ? parsers[site] : '';

  BaseParser.call(this, ZaloraParser.MAPPING);
};

util.inherits(ZaloraParser, BaseParser);

ZaloraParser.prototype.parse = function (crawlStuffs) {
  var stuffs = [];

  for (var key in crawlStuffs) {
    var crawlStuff = crawlStuffs[key];
    //var stuff = this._mapping(crawlStuff);
    crawlStuff.site = this.site;
    crawlStuff.transform = this.transform(crawlStuff);

    stuffs.push(crawlStuff);
  }

  return stuffs;
};

ZaloraParser.prototype.mapCategory = function (stuff) {
  var mainCategory = (stuff.mainCategory || '').toLowerCase();
  var subCategory = (stuff.subCategory1 || '').toLowerCase();
  var subCategory2 = (stuff.subCategory2 || '').toLowerCase();

  try {
    return this.category[mainCategory][subCategory][subCategory2];  
  }
  catch(e) {
    return null;
  }
};

ZaloraParser.prototype.transform = function (stuff) {
  var self = this;

  var discountPhase = '';
  if (stuff.discount && stuff.salePrice && parseFloat(stuff.salePrice) > 0) {
    discountPhase = 'SALE '+stuff.discount+' %\n'+
                    'From '+stuff.price+' to '+stuff.salePrice+'\n\n';
  }
  var description = stuff.name+'\n'+stuff.description+'\n\n';
  var tags = stuff.tags ? stuff.tags.split(' ') : null;

  var images = [];
  for (var i=1 ; i<=4 ; i++) {
    if(stuff['path'+i]) images.push(stuff['path'+i]);
  };

  return {
    images: images,
    externalLink: stuff.link,
    price: parseFloat(stuff.price),
    salePrice: stuff.salePrice ? parseFloat(stuff.salePrice) : null,
    tags: tags,
    description: discountPhase + description,
    market: 'TH',
    owner: this.userId,
    // categories: [stuff.mainCategory],
    categories: this.mapCategory(stuff),
    name: stuff.name,
    sku: stuff.sku
  };
}

module.exports = ZaloraParser;