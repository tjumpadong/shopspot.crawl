var util = require('util'),
    async = require('async');

var BaseParser = require('./BaseParser');

var IndexParser = function (categories) {
  BaseParser.call(this, IndexParser.MAPPING);

  this.category = categories.index;
};

util.inherits(IndexParser, BaseParser);

IndexParser.prototype.parse = function (crawlStuffs) {

  var stuffs = [];

  for (var key in crawlStuffs) {
    var crawlStuff = crawlStuffs[key];
    var stuff = this._mapping(crawlStuff);

    stuff.tranform = this.tranform(stuff);

    stuffs.push(stuff);
  }

  return stuffs;
};

IndexParser.prototype.mapCategory = function (stuff) {
  var mainCategory = (stuff.mainCategory || '').toLowerCase();
  var subCategory = (stuff.subCategory || '').toLowerCase();
  var subCategory2 = (stuff.subCategory2 || '').toLowerCase();

  try {
    return this.category[mainCategory][subCategory][subCategory2];  
  }
  catch(e) {
    return null;
  }
  
};

IndexParser.prototype.tranform = function (stuff) {
  var self = this;

  var discountPhase = '';
  if (stuff.discount) {
    discountPhase = 'SALE '+stuff.discount+' %\n'+
                    'From '+stuff.price+' to '+stuff.salePrice+'\n\n';
  }
  var description = stuff.name+'\n'+stuff.description+'\n\n';
  var tags = stuff.tags ? stuff.tags.split(',') : null;

  /*if (tags) {
    for (var i=0; i<tags.length; i++) {
      description += '#'+tags[i];
    }

    description += '\n\n';
  }*/

  return {
    images: [ stuff.path1, stuff.path2, stuff.path3 ],
    externalLink: stuff.link,
    price: self.refinePrice(stuff.price),
    tags: stuff.tags,
    description: discountPhase + description,
    market: 'TH',
    owner: 'owner',
    categories: this.mapCategory(stuff)
  };
};

IndexParser.MAPPING = {
  'product image path1': 'path1',
  'product image path2': 'path2',
  'product image path3': 'path3',
  'product link url': 'link',
  'product name': 'name',
  'product description': 'description',
  'price': 'price',
  'sale price': 'salePrice',
  '% of sale': 'discount',
  'product main category': 'mainCategory',
  'product sub category': 'subCategory',
  'product sub category2': 'subCategory2',
  'tag': 'tags',
  'sku': 'sku'
};

module.exports = IndexParser;