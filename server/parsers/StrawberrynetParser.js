var util = require('util'),
		async = require('async');

var BaseParser = require('./BaseParser');

var StrawberrynetParser = function (categories, parsers) {
	var site = 'strawberrynet';
	this.site = site;
  this.category = categories ? categories[site] : '';  
  this.userId = parsers ? parsers[site] : '';

	BaseParser.call(this);
};

util.inherits(StrawberrynetParser, BaseParser);

StrawberrynetParser.prototype.parse = function(crawlStuffs) {
	var stuffs = [];

	for (var key in crawlStuffs) {
		var crawlStuff = crawlStuffs[key];
		crawlStuff.site = this.site;
		crawlStuff.transform = this.transform(crawlStuff);
		stuffs.push(crawlStuff);
	}

	return stuffs;
};

StrawberrynetParser.prototype.mapCategory = function (stuff) {
	var mainCategory = (stuff.mainCategory || '');

	try {
		return this.category[mainCategory];
	}
	catch(e) {
		return null;
	}
	
};

StrawberrynetParser.prototype.transform = function (stuff) {
	
	var discountPhase = '';
	if (stuff.discount && stuff.salePrice && parseFloat(stuff.salePrice) > 0) {
		discountPhase = 'SALE '+stuff.discount+' %\n'+
                    'From '+stuff.price+' to '+stuff.salePrice+'\n\n';
	}
	var description = stuff.name+'\n'+stuff.description+'\n\n';
	var strawberrynetPhase = '*** ราคาอาจมีการเปลี่ยนแปลงตามอัตราแลกเปลี่ยน\n\n';
	var tags = this._convertTagStringToArray(stuff);
	var tagPhase = this._composeTags(tags);
  var self = this;

	return {
		images: this.setImage(stuff),
		externalLink: stuff.link,
		price: stuff.price,
		tags: tags,
		description: discountPhase + description + strawberrynetPhase + tagPhase,
		market: 'TH',
		owner: this.userId,
		categories: this.mapCategory(stuff),
		name: stuff.name,
    sku: stuff.sku
	};
};

module.exports = StrawberrynetParser;
