var util = require('util'),
		async = require('async');

var BaseParser = require('./BaseParser');

var WearYouWantParser = function (categories) {
	BaseParser.call(this, WearYouWantParser.MAPPING);

	this.category = categories.wearyouwant;
};

util.inherits(WearYouWantParser, BaseParser);

WearYouWantParser.prototype.parse = function(crawlStuffs) {

	var stuffs = [];

	for (var key in crawlStuffs) {
		var crawlStuff = crawlStuffs[key];
		var stuff = this._mapping(crawlStuff);
		
		stuff.tranform = this.tranform(stuff);
		stuffs.push(stuff);
	}

	return stuffs;
};

WearYouWantParser.prototype.mapCategory = function (stuff) {
	var mainCategory = (stuff.mainCategory || '').toLowerCase();
	var subCategory = (stuff.subCategory || '').toLowerCase();
	var subCategory2 = (stuff.subCategory2 || '').toLowerCase();

	try {
		return this.category[mainCategory][subCategory][subCategory2];	
	}
	catch(e) {
		return null;
	}
	
};

WearYouWantParser.prototype.tranform = function (stuff) {
	var discountPhase = '';
	if (stuff.discount) {
		discountPhase = 'SALE '+stuff.discount+' %\n'+
										'From '+stuff.price+' to '+stuff.salePrice;
	}
	var description = stuff.name+'\n'+stuff.description+'\n\n';
	var tags = stuff.tags ? stuff.tags.split(',') : null;

	if (tags) {
		for (var i=0; i<tags.length; i++) {
			description += '#'+tags[i];
		}

		description += '\n\n';
	}

	var buynowPhase = 'BUY NOW:\n'+stuff.link;

	return {
		images: [ stuff.path1, stuff.path2, stuff.path3 ],
		externalLink: stuff.link,
		price: stuff.price,
		tags: stuff.tags,
		description: discountPhase + description + buynowPhase,
		market: 'TH',
		owner: 'owner',
		categories: this.mapCategory(stuff)
	};
};

WearYouWantParser.MAPPING = {
	'product image path1': 'path1',
	'product image path2': 'path2',
	'product image path3': 'path3',
	'product link url': 'link',
	'product name': 'name',
	'product description': 'description',
	'price': 'price',
	'sale price': 'salePrice',
	'% of sale': 'discount',
	'product main category': 'mainCategory',
	'product sub category': 'subCategory',
	'product sub category2': 'subCategory2',
	'tag': 'tags',
	'sku': 'sku'
};

module.exports = WearYouWantParser;