var util = require('util'),
    async = require('async');

var BaseParser = require('./BaseParser');

var PricezaOutlet24Parser = function (categories, parsers) {
  var site = 'pricezaOutlet24';
  this.site = site;
  this.category = categories ? categories[site] : '';
  this.userId = parsers ? parsers[site] : '';

  BaseParser.call(this);
};

util.inherits(PricezaOutlet24Parser, BaseParser);

PricezaOutlet24Parser.prototype.parse = function (crawlStuffs) {
  var stuffs = [];

  for (var key in crawlStuffs) {
    var crawlStuff = crawlStuffs[key];
    crawlStuff.site = this.site;
    crawlStuff.transform = this.transform(crawlStuff);

    stuffs.push(crawlStuff);
  }

  return stuffs;
};

PricezaOutlet24Parser.prototype.transform = function (stuff) {
  var self = this;

  return {
    images: [ stuff.path1 ],
    externalLink: stuff.link,
    price: this._convertPrice(stuff.price),
    description: stuff.description,
    market: 'TH',
    owner: this.userId,
    categories: [ stuff.mainCategory ],
    name: stuff.name,
    sku: stuff.sku
  };
}

module.exports = PricezaOutlet24Parser;