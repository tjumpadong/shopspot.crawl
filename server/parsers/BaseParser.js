var util = require('util'),
		async = require('async'),
		_ = require('underscore');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var BaseParser = function (mapping) {
	this.mapping = mapping || BaseParser.CSV_MAPPING;
};

BaseParser.prototype.setImage = function (stuff) {
	var images = [];
  for (var i=1 ; i<=4 ; i++) {
    if(stuff['path'+i]) images.push(stuff['path'+i]);
  };

  return images;
};

BaseParser.prototype._composeTags = function (tags) {
	var tagPhase = '';
	for (var i=0; i<tags.length; i++) {
		tagPhase += '#'+tags[i]+' ';
	}

  return tagPhase;
};

BaseParser.prototype._convertTagStringToArray = function (stuff) {
	var reg = /,(?:\s*)|\s/; // check comma and space
	return stuff.tags ? stuff.tags.replace('.', '').trim().split(reg) : [];
}

BaseParser.prototype._convertPrice = function (price) {
	var tmpPrice = price;

	if (isNaN(price)) {
		tmpPrice = price.replace(/,/g, '');
	}

	return parseFloat(tmpPrice);
};
/*

BaseParser.prototype.refinePrice = function (price) {
  var position = price.indexOf('.');
  return position === -1 ? price : price.slice(0, position);
};
*/
BaseParser.CSV_MAPPING = {
	'product image path1': 'path1',
	'product image path2': 'path2',
	'product image path3': 'path3',
	'product link url': 'link',
	'product name': 'name',
	'product description': 'description',
	'price': 'price',
	'sale price': 'salePrice',
	'% of sale': 'discount',
	'product main category': 'mainCategory',
	'product sub category': 'subCategory',
	'product sub category2': 'subCategory2',
	'tag': 'tags',
	'sku': 'sku'
};

module.exports = BaseParser;