var async = require('async'),
		util = require('util');

var log = require('stickyrice').loggers.get('crawl');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var ImportFileValidator = require('../validator/ImportFileValidator');
var crawlParsers = require('../preloads/CrawlParsers');
var DatasourceFactory = require('../modules/datasources/DatasourceFactory'),
    XMLTraversalFactory = require('../modules/datasources/traversals/XMLTraversalFactory'),
		CSVTraversalFactory = require('../modules/datasources/traversals/CSVTraversalFactory');

var ImportFileService = function (repositories) {
	this.repositories = repositories;
	this.importFileValidator = new ImportFileValidator();
	this.crawlParsers = crawlParsers;
	this.datasourceFactory = new DatasourceFactory();

	this.traversals = {
		xml: new XMLTraversalFactory(),
    csv: new CSVTraversalFactory()
	};
};
/*
ImportFileService.prototype.csv = function(input, callback) {
	callback = callback || function () {};

	var self = this;
	var path = input.path;
	var fileName = input.fileName;
	var userId = input.userId;
	var validator = this.importFileValidator;
	var csvManager = this.csvManager;
	var parser = this.parserFactory.instance(userId);

	var crawlDataReposiotry = this.repositories.crawlData;
	var groupCrawlDataReposiotry = this.repositories.groupCrawlData;

	async.waterfall([
		function validate (next) {
			input.parser = parser;
			validator.validate(input, function (error, result) {
				next(error);
			});
			
		},
		function csvToJson (next) {
			csvManager.readFromPath(path, function (error, data) {
				next(error, data);
			});
		},
		function validateHeader (crawlStuffs, next) {
			parser.validateHeader(crawlStuffs[0], function (error, data) {
				if (error) {
					next(error);
				}
				else {
					next(null, crawlStuffs);
				}
			});
		},
		function parseObject (crawlStuffs, next) {
			var data = parser.parse(crawlStuffs);
			
			if (data && data.length === 0) {
				next(new ErrorObject('Can not parse data. Please check header file',
															ErrorDomain.DOMAIN_PARSER,
															ErrorCode.PARSER_ERROR));
			}
			else {
				next(null, data);	
			}
		},
		function createGroupCrawl (data, next) {
			var groupCrawl = {
				title: input.title,
				description: input.description,
				createdDate: new Date().getTime(),
				fileName: input.fileName,
				user: userId 
			};

			groupCrawlDataReposiotry.create(groupCrawl, function (error, groupCrawl) {
				if (error) {
					next(new ErrorObject('Can not create group crawl data',
															ErrorDomain.DOMAIN_DATABASE,
															ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, data, groupCrawl);
				}
			});
		},
		function createCrawlStuff (crawlStuffs, group, next) {
			var createdCrawlStuffs = [];

			async.forEachSeries(crawlStuffs, function (stuff, subNext) {
				stuff.userId = userId;
				stuff.tranform.owner = userId;
				stuff.groupId = group._id.toString();
				stuff.importedDate = new Date().getTime();
				stuff.status = 'waiting';
				stuff.feedName = fileName;
				crawlDataReposiotry.create(stuff, function (error, data) {
					if (!error) createdCrawlStuffs.push(data);
					subNext();
				});
			},
			function done () {
				next(null, createdCrawlStuffs, group);
			});
		},
		function updateTotalStuff (createdCrawlStuffs, group, next) {
			group.amount = createdCrawlStuffs.length;
			groupCrawlDataReposiotry.update(group, function (error, updatedGroupCrawl) {
				if (error) {
					next(new ErrorObject('Can not update group crawl data',
															ErrorDomain.DOMAIN_DATABASE,
															ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, createdCrawlStuffs);
				}
			});
		}
	],
	function done (error, createdCrawlStuffs) {
		callback(error, createdCrawlStuffs);
	});
};*/

ImportFileService.prototype.csv = function (input, callback) {
	callback = callback || function () {};

	var self = this;
	var type = 'csv';
	var site = input.site;
	var path = input.path || '';

	var tmpStuffRepository = this.repositories.tmpStuff;
	var	traversal = this.traversals[type].createTraversal(site);
	var	csvDatasource = this.datasourceFactory.createCSVDatasource(traversal, path);
	var	parser = this.crawlParsers.get(site);

	this._run(csvDatasource, parser, callback);
/*
	waterfall([
		function validate (next) {
			if (csvDatasource && parser) {
				next();
			}
			else {
				next(new ErrorObject('Site validate not match',
															ErrorDomain.DOMAIN_VALIDATE,
															ErrorCode.VALIDATE_BLANK_INPUT))
			}
		},
		function getData (next) {
			csvDatasource.convert(function (error, stuffs) {
				next(error, stuffs);
			});
		},
		function parseData (stuffs, next) {
			var stuffsTransform = parser.parse(stuffs);
			next(null, stuffsTransform);
		},
		function createTmpStuff () {

		}
	]);*/
};

ImportFileService.prototype.xml = function (input, callback) {
	callback = callback || function () {};

	var self = this;
	var site = input.site;
	var uri = input.uri || '';
 	var type = 'xml';

	var tmpStuffRepository = this.repositories.tmpStuff;
	var	traversal = this.traversals[type].createTraversal(site);
	var	xmlDatasource = this.datasourceFactory.createXMLDatasource(traversal, uri);
	var	parser = this.crawlParsers.get(site);
	
	async.waterfall([
		function validate (next) {
			log.debug('_convert1');
			if (xmlDatasource && parser) {
				next();
			}
			else {
				next(new ErrorObject('Site validate not match',
															ErrorDomain.DOMAIN_VALIDATE,
															ErrorCode.VALIDATE_BLANK_INPUT))
			}
		},
		function getData (next) {
			log.debug('_convert2');
			xmlDatasource.convert(function (error, stuffs) {
				next(error, stuffs);
			});
		},
		function parseData (stuffs, next) {
			log.debug('_convert3');
			var stuffsTransform = parser.parse(stuffs);
			next(null, stuffsTransform);
		},
		function createTmpStuff (stuffs, next) {
			log.debug('_convert4');
			async.forEachSeries(stuffs, function (stuff, subNext) {
				tmpStuffRepository.create(stuff, function (error, createdStuffs) {
					if (error) {
						log.error('error create stuff : '+stuff.sku);	
					}
					
					subNext();
				});
			},
			function done (error) {
				next(null, stuffs);
			});
		}
	],
	function (error, stuffs) {
		callback(error, stuffs);
	});
};

ImportFileService.prototype._run = function (datasource, parser, callback) {
	callback = callback || function () {};

	var tmpStuffRepository = this.repositories.tmpStuff;

	async.waterfall([
		function validate (next) {
			if (datasource && parser) {
				next();
			}
			else {
				next(new ErrorObject('Site validate not match',
															ErrorDomain.DOMAIN_VALIDATE,
															ErrorCode.VALIDATE_BLANK_INPUT))
			}
		},
		function getData (next) {
			datasource.convert(function (error, stuffs) {
				next(error, stuffs);
			});
		},
		function parseData (stuffs, next) {
			log.debug('getData : '+util.inspect(stuffs));
			var stuffsTransform = parser.parse(stuffs);
			log.debug('parseData : '+util.inspect(stuffsTransform));
			next(null, stuffsTransform);
		},
		function createTmpStuff (stuffs, next) {
			async.forEachSeries(stuffs, function (stuff, subNext) {
				tmpStuffRepository.create(stuff, function (error, createdStuffs) {
					if (error) {
						log.error('error create stuff : '+stuff.sku);	
					}
					
					subNext();
				});
			},
			function done (error) {
				next(null, stuffs);
			});
		}
	],
	function (error, stuffs) {
		callback(error, stuffs);
	});
};



module.exports = ImportFileService;
