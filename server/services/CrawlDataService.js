var async = require('async'),
		util = require('util');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var CrawlDataTranformValidator = require('../validator/CrawlDataTranformValidator');

var _root = 'http://shopspotapp.com/stuff/'

var CrawlDataService = function (repositories) {
	this.repositories = repositories;
	this.crawlDataTranformValidator = new CrawlDataTranformValidator();
};

CrawlDataService.prototype.get = function (input, callback) {
	callback = callback || function () {};

	var crawlDataId = input.crawlDataId;
	var crawlDataRepository = this.repositories.crawlData;

	crawlDataRepository.get(crawlDataId, function (error, crawlData) {
		if (error || !crawlData) {
			callback(new ErrorObject('Can not find from crawlDataId',
															ErrorDomain.DOMAIN_CRAWL,
															ErrorCode.DATABASE_ERROR));
		}
		else {
			callback(null, crawlData);
		}
	});
};

CrawlDataService.prototype.update = function (input, callback) {
	callback = callback || function () {};

	var self = this;
	var crawlDataTranformValidator = this.crawlDataTranformValidator;
	var crawlDataRepository = this.repositories.crawlData;

	var crawlDataId = input.crawlDataId;
	var tranform = {
		categories: input.categories,
		description: input.description,
		externalLink: input.externalLink,
		images: input.images,
		market: input.market,
		owner: input.owner,
		price: input.price,
		tags: input.tags
	};

	if (typeof tranform.categories === 'string') {
		tranform.categories = [tranform.categories];
	}

	if (typeof tranform.images === 'string') {
		tranform.images = [tranform.images];
	}

	async.waterfall([
		function validate (next) {
			crawlDataTranformValidator.validate(tranform, function (error, results) {
				if (error) {
					next(error);
				}
				else {
					next();
				}
			});
		},
		function get (next) {
			crawlDataRepository.get(crawlDataId, function (error, crawlData) {
				if (error) {
					next(new ErrorObject('can not find from crawlDataId',
																	ErrorDomain.DOMAIN_CRAWL,
																	ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, crawlData);
				}
			});
		},
		function update (crawlData, next) {
			crawlData.tranform = tranform;

			crawlDataRepository.update(crawlData, function (error, updatedCrawlData) {
				if (error) {
					next(new ErrorObject('can not update crawl data',
																	ErrorDomain.DOMAIN_CRAWL,
																	ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, updatedCrawlData);
				}
			});
		}
	],
	function done (error, updatedCrawlData) {
		if (error) {
			callback(error);
		}
		else {
			callback(null, updatedCrawlData);
		}
	});
};

CrawlDataService.prototype.updateStatus = function (input, callback) {
  callback = callback || function () {};

  var crawlDataId = input.crawlDataId;
  var status = input.status;
  var stuffId = input.stuffId;

  var crawlDataRepository = this.repositories.crawlData;

  async.waterfall([
    function validate (next) {
      if (!crawlDataId) {
        next(new ErrorObject('require crawl data id',
                              ErrorDomain.DOMAIN_VALIDATE,
                              ErrorCode.VALIDATE_BLANK_INPUT));
      }
      else {
        next();
      }
    },
    function getData (next) {
      crawlDataRepository.get(crawlDataId, function (error, crawlData) {
        if (error) {
          next(new ErrorObject('can not find from crawlDataId',
                                  ErrorDomain.DOMAIN_CRAWL,
                                  ErrorCode.DATABASE_ERROR));
        }
        else {
          next(null, crawlData);
        }
      });
    },
    function updateData (crawlData, next) {
      crawlData.status = status;
      crawlData.shopspotLink = _root + stuffId;

      crawlDataRepository.update(crawlData, function (error, updatedCrawlData) {
        if (error) {
          next(new ErrorObject('can not update crawl data',
                                  ErrorDomain.DOMAIN_CRAWL,
                                  ErrorCode.DATABASE_ERROR));
        }
        else {
          next(null, updatedCrawlData);
        }
      });
    }
  ],
  function done (error, updatedData) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, updatedData);
    }
  });
};

CrawlDataService.prototype.removeDatas = function (input, callback) {
	callback = callback || function () {};

	var crawlDataIds = input.crawlDataIds || [];
	if (typeof crawlDataIds === 'string') {
		crawlDataIds = [crawlDataIds];
	}

	var crawlDataRepository = this.repositories.crawlData;

	if (crawlDataIds.length === 0) {
		callback(new ErrorObject('Crawl data id should not blank',
															ErrorDomain.DOMAIN_VALIDATE,
															ErrorCode.VALIDATE_BLANK_INPUT,
															crawlDataIds));
	}
	else {
		async.forEachSeries(crawlDataIds, function (crawlDataId, next) {
			crawlDataRepository.remove(crawlDataId, function (error, removedData) {
				next();
			});
		},
		function done () {
			callback(null, true);
		});
	}
};

CrawlDataService.prototype.findFromUserId = function(input, callback) {
	callback = callback || function () {};
	var userId = input.userId || '';
	var offset = {
		page: input.page || 0,
		limit: input.limit || 10
	};

	var crawlDataRepository = this.repositories.crawlData;

	crawlDataRepository.findFromUserId(userId, offset, function (error, crawlDataCount, crawlDatas) {
		if (error) {
			callback(new ErrorObject('can not find from userId',
															ErrorDomain.DOMAIN_CRAWL,
															ErrorCode.DATABASE_ERROR,
															userId));
		}
		else {
			var results = {
				crawlDataCount: crawlDataCount,
				crawlDatas: crawlDatas
			};

			callback(null, results);
		}
	});
};

CrawlDataService.prototype.findFromGroupId = function (input, callback) {
	callback = callback || function () {};

	var groupId = input.groupId;
	var offset = {
		page: input.page || 0,
		limit: input.limit || 10
	};

	var crawlDataRepository = this.repositories.crawlData;

	crawlDataRepository.findFromGroupId(groupId, offset, function (error, crawlDataCount, crawlDatas) {
		if (error) {
			callback(new ErrorObject('can not find from groupId',
															ErrorDomain.DOMAIN_CRAWL,
															ErrorCode.DATABASE_ERROR));
		}
		else {
			var results = {
				crawlDataCount: crawlDataCount,
				crawlDatas: crawlDatas
			};

			callback(null, results);
		}
	});
};

CrawlDataService.prototype.removeFromGroupId = function (input, callback) {
	callback = callback || function () {};

	var groupId = input.groupId;
	var crawlDataRepository = this.repositories.crawlData;
	
	crawlDataRepository.removeFromGroupId(groupId, function (error, removedCrawlData) {
		if(error) {
			callback(new ErrorObject('can not remove from groupId',
															ErrorDomain.DOMAIN_CRAWL,
															ErrorCode.DATABASE_ERROR));
		}
		else {
			callback(null, true);
		}
	});
};

module.exports = CrawlDataService;