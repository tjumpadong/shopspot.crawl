var async = require('async'),
		util = require('util');

var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var GroupCrawlDataService = function (repositories) {
	this.repositories = repositories;
};

GroupCrawlDataService.prototype.findFromUserId = function(input, callback) {
	callback = callback || function () {};
	var userId = input.userId || '';
	var offset = {
		page: input.page || 0,
		limit: input.limit || 10
	};

	var groupCrawlDataRepository = this.repositories.groupCrawlData;

	groupCrawlDataRepository.findFromUserId(userId, offset, function (error, total, groups) {
		if (error) {
			callback(new ErrorObject('can not find from userId',
															ErrorDomain.DOMAIN_GROUPCRAWL,
															ErrorCode.DATABASE_ERROR,
															userId));
		}
		else {
			var result = {
				total: total,
				groups: groups
			};

			callback(null, result);
		}
	});
};

GroupCrawlDataService.prototype.removeFromGroupId = function (input, callback) {
	callback = callback || function () {};
	var groupId = input.groupId;

	var groupCrawlDataRepository = this.repositories.groupCrawlData;

	groupCrawlDataRepository.remove(groupId, function (error, result) {
		if (error) {
			callback(new ErrorObject('can not delete from groupId',
															ErrorDomain.DOMAIN_GROUPCRAWL,
															ErrorCode.DATABASE_ERROR,
															groupId));
		}
		else {
			callback(null, true);
		}
	});
};

GroupCrawlDataService.prototype.countItem = function (callback) {
	callback = callback || function () {};

	var groupCrawlDataRepository = this.repositories.groupCrawlData;
	groupCrawlDataRepository.countItem(function (error, result) {
		if (error) {
			callback(new ErrorObject('can get count items',
															ErrorDomain.DOMAIN_GROUPCRAWL,
															ErrorCode.DATABASE_ERROR,
															''));
		}
		else {
			var total = result ? result[0] ? result[0].total : 0 : 0; 
			callback(null, total);
		}
	});
};

module.exports = GroupCrawlDataService;