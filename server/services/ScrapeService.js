var async = require('async'),
    util = require('util'),
    cheerio = require('cheerio'),
    request = require('request'),
    json2csv = require('json2csv'),
    fs = require('fs'),
    path = require('path');

var ErrorObject = require('../modules/Error').ErrorObject,
    ErrorCode =  require('../modules/Error').ErrorCode,
    ErrorDomain = require('../modules/Error').ErrorDomain;

var CrawlParser = require('../preloads/CrawlParsers');

var ScrapeService = function () {
  this.scraperFactory = {};
  this.crawlParser = CrawlParser.instance();
};

ScrapeService.prototype.getTotalPage = function (input, callback) {
  callback = callback || function () {};
  var site = input.site;
  var uri = input.uri;

  var scraper = this.scraperFactory.instance(site);

  scraper.getTotalPage(uri, function (error, totalPage) {
    if (error) {
      callback(new ErrorObject());
    }
    else {
      callback(null, totalPage);
    }
  });
};

ScrapeService.prototype.scraping = function (input, callback) {
  callback = callback || function () {};
  var self = this;
  var site = input.site;
  var uri = input.uri;
  var page = input.page ? parseInt(input.page) : null;

  var options = {
    page: page
  };

  var scraper = this.scraperFactory.instance(site);
  var fileName = site;

  async.waterfall([
    function validate (next) {
      if (!uri || !site) {
        next(new ErrorObject('invalid site or uri',
                              ErrorDomain.DOMAIN_VALIDATE,
                              ErrorCode.VALIDATE_BLANK_INPUT));
      }
      else {
        next();
      }
    },
    function getLink (next) {
      uri = encodeURI(uri);

      scraper.getLink(uri, options, function (error, links) {
        if (error) {
          next(new ErrorObject('Can not get link',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
        }
        else {
          next(null, links);
        }
      });
    },
    function getDetail (links, next) {
      scraper.getDetail(links, function (error, stuffs) {
        if (error) {
          next(new ErrorObject('Can not get detail',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
        }
        else {
          var mapping = scraper.getMapping();
          if (stuffs[0]) {
            var stuff = stuffs[0];
            var category;
            if (stuff[mapping.subCategory2]) {
              category = stuff[mapping.subCategory2];
            }
            else if (stuff[mapping.subCategory1]) {
              category = stuff[mapping.subCategory1];
            }
            else {
              category = stuff[mapping.mainCategory];
            }

            fileName = fileName + '_' + category.replace(' ', '_').replace(/\//g, '_') + '_' + page;
          }

          next(null, stuffs);
        }
      });
    },
    function exportToCsv (stuffs, next) {
      var object = {
        data: stuffs,
        fields: scraper.getField()
      };

      json2csv(object, function (error, csv) {
        if (error) {
          next(new ErrorObject('Can not convert to csv file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, csv);
        }
      });
    },
    function saveFile (csv, next) {
      var dataFilePath = path.join(__dirname, '..', 'csv', fileName + '.csv');
      fs.writeFile(dataFilePath, csv, function (error) {
        if (error) {
          next(new ErrorObject('Can not save file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, dataFilePath);
        }
      })
    }
  ], function done (error, output) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, output);
    }
  });
};

ScrapeService.prototype.scrapingXML = function (input, callback) {
  callback = callback || function () {};

  var site = input.site;
  var uri = input.uri;
  var fileName = input.fileName;

  var scraper = this.scraperFactory.instance(site);

  async.waterfall([
    function getXML (next) {
      scraper.getXML(uri, function (error, xmlFile) {
        if (error) {
          next(error);
        }
        else {
          next(null, xmlFile);
        }
      });
    },
    function getDetail (xmlFile, next) {
      scraper.getDetail(xmlFile, function (error, stuffs) {
        if (error) {
          next(new ErrorObject('Can not get detail',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
        }
        else {
          next(null, stuffs);
        }
      });
    },
    function exportToCsv (stuffs, next) {
      var object = {
        data: stuffs,
        fields: scraper.getField()
      };

      json2csv(object, function (error, csv) {
        if (error) {
          next(new ErrorObject('Can not convert to csv file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, csv);
        }
      });
    },
    function saveFile (csv, next) {
      var dataFilePath = path.join(__dirname, '..', 'csv', fileName + '.csv');
      fs.writeFile(dataFilePath, csv, function (error) {
        if (error) {
          next(new ErrorObject('Can not save file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, dataFilePath);
        }
      })
    }
  ], function done (error, output) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, output);
    }
  });
};


ScrapeService.prototype._scrapingAllPage = function (input, callback) {
  callback = callback || function () {};
  var self = this;

  async.waterfall([
    function getTotalPage (next) {
      self.getTotalPage(input, function (error, totalPage) {
        if (error) {
          next(new ErrorObject());
        }
        else {
          next(null, totalPage);
        }
      });
    },
    function scraping (totalPage, next) {
      input.page = 1;

      var repeater = function (page) {
        if (input.page <= totalPage) {
          console.log(input.uri);
          console.log('page: '+input.page);
          self.scraping(input, function (error, output) {
            if (error) {
              next(new ErrorObject());
            }
            else {
              repeater(input.page++);
            }
          });
        }
        else {
          next(null, true);
        }
      };

      repeater(1);
    }
  ],
  function done (error, output) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, output);
    }
  });
};

ScrapeService.prototype.scrapingFromFile = function (input, callback) {
  var self = this;

  var filePath = path.join(__dirname, '..', 'resources', 'scraperSourceFile');
  var lines = fs.readFileSync(filePath).toString().split('\n');

  var site = input.site;

  async.forEachSeries(lines, function (line, subNext) {
    var tmpInput = {
      site: site,
      uri: line
    };

    self._scrapingAllPage(tmpInput, function (error, success) {
      subNext();
    });
  },
  function done (error) {
    if (error) {
      callback(error);
    }
    else {
      callback(null);
    }
  });
}

ScrapeService.prototype.fromXML = function (input, callback) {
  callback = callback || function () {};

  var site = input.site.toLowerCase();
  var uri = input.uri;

  var scraper = this.scraperFactory.instance(site);

  async.waterfall([
    function getXML (next) {
      scraper.getXML(uri, function (error, xmlFile) {
        if (error) {
          next(error);
        }
        else {
          next(null, xmlFile);
        }
      });
    },
    function getDetail (xmlFile, next) {
      scraper.getDetail(xmlFile, function (error, stuffs) {
        if (error) {
          next(new ErrorObject('Can not get detail',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_GET_DATA));
        }
        else {
          next(null, stuffs);
        }
      });
    },
    function tranform (stuffs, next) {
      var parser = crawlParser[site];

      var tmpStuffs = parser.parse(stuffs);

      console.log('tmpStuffs : '+util.inspect(tmpStuffs));
      next(null, stuffs);
    }
    /*
    function exportToCsv (stuffs, next) {
      var object = {
        data: stuffs,
        fields: scraper.getField()
      };

      json2csv(object, function (error, csv) {
        if (error) {
          next(new ErrorObject('Can not convert to csv file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, csv);
        }
      });
    },
    function saveFile (csv, next) {
      var dataFilePath = path.join(__dirname, '..', 'csv', fileName + '.csv');
      fs.writeFile(dataFilePath, csv, function (error) {
        if (error) {
          next(new ErrorObject('Can not save file',
                                ErrorDomain.DOMAIN_SCRAPE,
                                ErrorCode.SCRAPE_ERROR_SAVE_FILE));
        }
        else {
          next(null, dataFilePath);
        }
      })
    }*/
  ], function done (error, output) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, output);
    }
  });
};

module.exports = ScrapeService;