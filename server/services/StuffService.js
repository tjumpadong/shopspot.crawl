var async = require('async'),
		util = require('util'),
    _ = require('underscore');
var log = require('stickyrice').loggers.get('crawl');
var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var stuffStatus = require('../modules/Enum').Stuff.status;

var StuffService = function (repositories, config) {
	this.repositories = repositories;
	this.parserConfig = config.parsers;
};

StuffService.prototype.syncTmpToStuff = function (input, callback) {
	callback = callback || function () {};
  var self = this;

	var site = input.site;
  var limit = input.limit;
  var userId = this.parserConfig[site];

	var tmpStuffRepository = this.repositories.tmpStuff;
	var stuffRepository = this.repositories.stuff;
	var errorStuff = [];

	async.waterfall([
		function getStuffFromSite (next) {
      var options = {
        limit: limit
      };

			tmpStuffRepository.findFromSite(site, options, function (error, tmpStuffs) {
				if (error) {
          next(new ErrorObject('Can not find any stuff from this site',
                                ErrorDomain.DOMAIN_DATABASE,
                                ErrorCode.STUFF_NOT_FOUND));
				}
				else if (tmpStuffs && tmpStuffs.length === 0) {
          next(new ErrorObject('There is no stuff',
                                ErrorDomain.DOMAIN_STUFF,
                                ErrorCode.STUFF_NOT_FOUND));
				}
				else {
					next(null, tmpStuffs);
				}
			});
		},
		function updateStuff (tmpStuffs, next) {
			async.forEachSeries(tmpStuffs, function (tmpStuff, subNext) {
				stuffRepository.findFromSKU(tmpStuff.sku, userId, function (error, stuff) {
					if (error) {
						errorStuff.push(tmpStuff.sku);
						subNext();
					}
					else if (!stuff) {
            var mockStuff = _.clone(tmpStuff.transform);
            mockStuff.status = stuffStatus.UNPUBLISHED;

						stuffRepository.create(mockStuff, function (error, createdStuff) {
              if (error) {
                subNext(new ErrorObject('Can not create new stuff',
                                        ErrorDomain.DOMAIN_DATABASE,
                                        ErrorCode.DATABASE_ERROR));
              }
              else {
                subNext();
              }
            });
					}
					else {
            if (stuff.status === stuffStatus.PUBLISHED) {
              self._updateDuplicate(tmpStuff.transform, stuff, function (error, updatedStuff) {
                subNext();
              });
            }
            else {
              var mockStuff = _.clone(stuff);
              for (var key in tmpStuff.transform) {
                mockStuff[key] = tmpStuff.transform[key];
              }
              mockStuff.status = stuffStatus.UNPUBLISHED;

              stuffRepository.update(mockStuff, function (error, updatedStuff) {
                if (error) {
                  subNext(error);
                }
                else {
                  subNext();
                }
              })
            }
					}	
				});
			},
			function done () {
				async.forEachSeries(tmpStuffs, function (tmpStuff, subNext) {
          tmpStuff.read = true;
          tmpStuffRepository.update(tmpStuff, function (error, updatedTmpStuff) {
            subNext();
          });
        },
        function done () {
          next();
        });
			});
		}
	],
	function (error) {
		if (error) {
			callback(error);
		}
		else {
			callback(null, true)
		}
	});
};


StuffService.prototype.updateProductionStuffId = function (input, callback) {
	callback = callback || function () {};

	var stuffId = input.stuffId;
	var productionStuffId = input.productionStuffId;

	var stuffRepository = this.repositories.stuff;

	async.waterfall([
		function validate (next) {
			if (!productionStuffId) {
				next(new ErrorObject('ProductionStuffId should not be blank',
                          ErrorDomain.DOMAIN_VALIDATE,
                          ErrorCode.VALIDATE_BLANK_INPUT))
			}
			else {
				next();
			}
		},
		function getStuff (next) {
			stuffRepository.get(stuffId, function (error, stuff) {
				if (error) {
					next(new ErrorObject('Can not get stuff',
                                  ErrorDomain.DOMAIN_DATABASE,
                                  ErrorCode.DATABASE_ERROR))
				}
				else if (!stuff) {
					next(new ErrorObject('Do not have stuff in database',
                                        ErrorDomain.DOMAIN_DATABASE,
                                        ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, stuff);
				}
			});
		},
		function updateStuff (stuff, next) {
			stuff.productionStuffId = productionStuffId;
			stuff.status = stuffStatus.PUBLISHED;

			stuffRepository.update(stuff, function (error, updatedStuff) {
				if (error) {
					next(new ErrorObject('Can not update stuff',
                            ErrorDomain.DOMAIN_DATABASE,
                            ErrorCode.DATABASE_ERROR));
				}
				else {
					next(null, updatedStuff);
				}
			});
		}
	],
	function done (error, stuff) {
		callback(error, stuff);
	});
};

StuffService.prototype._updateDuplicate = function (newStuff, oldStuff, callback) {
  callback = callback || function () {};

  var stuffRepository = this.repositories.stuff;
  var isEqual = function (newStuff, oldStuff) {
    for (var key in newStuff) {
      if (key !== '_id') {
        if (newStuff[key] !== oldStuff[key]) {
          return false;
        }
      }
    }

    return true;
  };

  if (isEqual(newStuff, oldStuff)) {
    callback(null, oldStuff);
  }
  else {
    oldStuff.status = stuffStatus.UPDATED;
    if (oldStuff.revision && oldStuff.revision.length >= 3) {
      oldStuff.splice(0, 1);
    }
    else {
      oldStuff.revision = [];
    }
    oldStuff.revision.push(newStuff);

    stuffRepository.update(oldStuff, function (error, stuff) {
      if (error) {
        callback();
      }
      else {
        callback(null, stuff);
      }
    });
  }
};

StuffService.prototype.findFromSite = function (input, callback) {
	callback = callback || function () {};

	var site = input.site;
	var options = {
		page: input.page || 0,
		limit: input.limit || 10,
    countStuff: true
	};
	var userId = this.parserConfig[site];
	var stuffRepository = this.repositories.stuff;

	async.waterfall([
		function validate (next) {
			if (!userId) {
				next(new ErrorObject('Invalid site. Can not get ownerId',
                            ErrorDomain.DOMAIN_VALIDATE,
                            ErrorCode.VALIDATE_INVALID_INPUT,
                            site));
			}
			else {
				next();
			}
		},
		function findStuff (next) {
			stuffRepository.findFromOwner(userId, options, function (error, stuffs, total) {
				if (error) {
					next(new ErrorObject('Can not find stuff',
                                  ErrorDomain.DOMAIN_DATABASE,
                                  ErrorCode.DATABASE_ERROR))
				}
				else {
					next(null, stuffs, total);
				}
			});
		}
	],
	function done (error, stuffs, total) {
		callback(error, stuffs, total);
	});
};

StuffService.prototype.findFromProductionId = function (input, callback) {
  callback = callback || function () {};

  var productionId = input.productionId;

  var stuffRepository = this.repositories.stuff;

  stuffRepository.findFromProductionId(productionId, function (error, stuff) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuff);
    }
  });
};

StuffService.prototype.findFromId = function (input, callback) {
  callback = callback || function () {};

  var id = input.id;

  var stuffRepository = this.repositories.stuff;

  stuffRepository.get(id, function (error, stuff) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuff);
    }
  })
}

StuffService.prototype.updateFromRevision = function (input, callback) {
  callback = callback || function () {};

  var stuffRepository = this.repositories.stuff;
  var stuffId = input._id;
  
  async.waterfall([
    function validate (next) {
      if (!stuffId) {
        next(new ErrorObject('Invalid stuff id',
                              ErrorDomain.DOMAIN_VALIDATE,
                              ErrorCode.VALIDATE_INVALID_INPUT));
      }
      else {
        next();
      }
    },
    function findStuff (next) {
      stuffRepository.get(stuffId, function (error, stuff) {
        if (error) {
          next(new ErrorObject('Can not get stuff',
                                ErrorDomain.DOMAIN_DATABASE,
                                ErrorCode.DATABASE_ERROR));
        }
        else {
          next(null, stuff);
        }
      });
    },
    function updateStuff (stuff, next) {
      if (input.images) {
        stuff.images = input.images;
      }
      if (input.externalLink) {
        stuff.externalLink = input.externalLink;
      }
      if (input.price) {
        stuff.price = input.price;
      }
      if (input.tags) {
        stuff.tags = input.tags;
      }
      if (input.description) {
        stuff.description = input.description;
      }
      if (input.market) {
        stuff.market = input.market;
      }
      if (input.categories) {
        stuff.categories = input.categories;
      }
      if (input.name) {
        stuff.name = input.name;
      }

      stuffRepository.update(stuff, function (error, updatedStuff) {
        if (error) {
          callback(error);
        }
        else {
          callback(null, updatedStuff);
        }
      });
    }
  ],
  function done (error, stuff) {
    if (error) {
      callback(error);
    }
    else {
      callback(null, stuff);
    }
  });
};

module.exports = StuffService;
