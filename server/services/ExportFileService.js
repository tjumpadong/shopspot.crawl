var util = require('util'),
		async = require('async'),
		path = require('path'),
		fs = require('fs'),
		_ = require('underscore'),
		log = require('winston').loggers.get('shopspot');

var CsvManager = require('../modules/CsvManager');
var ErrorObject = require('../modules/Error').ErrorObject,
		ErrorCode =  require('../modules/Error').ErrorCode,
		ErrorDomain = require('../modules/Error').ErrorDomain;

var ExportFileService = function () {
	this.csvManager = new CsvManager();
}

ExportFileService.prototype.categoryToJsonWYW = function(input, callback) {
  callback = callback || function () {};

  var pathFile = input.path || '';
  var csvManager = this.csvManager;

  var category = {
    main: 'WYW Main Cat',
    sub1: 'WYW SubCat2',
    sub2: 'WYW SubCat3',
    ssMain: 'ShopSpot MainCat',
    ssSub: 'ShopSpot SubCat'
  };

  async.waterfall([
    function validate (next) {
      if (!pathFile) {
        next(new ErrorObject('Path not found',
                          ErrorDomain.DOMAIN_PARSER,
                          ErrorCode.PARSER_ERROR));
      }
      else {
        next();
      }
    },
    function readCategoryFromCsv (next) {
      csvManager.readFromPath(pathFile, function (error, data) {
        next(error, data);
      });
    },
    function createCategory (categories, next) {
      var createCategory = {};
      
      for (var key in categories) {
        var tmpCategory = categories[key];
        var valueMainCategory = tmpCategory[category.main].toLowerCase();
        var valueSubCategory1 = tmpCategory[category.sub1].toLowerCase();
        var valueSubCategory2 = tmpCategory[category.sub2].toLowerCase();
        
        //console.log(valueMainCategory+ ', '+valueSubCategory1+', '+valueSubCategory2);
        if (!createCategory[valueMainCategory]) {
          createCategory[valueMainCategory] = {}
        }
        
        if (createCategory[valueMainCategory] && !createCategory[valueMainCategory][valueSubCategory1]) {
          createCategory[valueMainCategory][valueSubCategory1] = {};
        }

        if (createCategory[valueMainCategory] && createCategory[valueMainCategory][valueSubCategory1] 
          && !createCategory[valueMainCategory][valueSubCategory1][valueSubCategory2]) {
          var main = tmpCategory[category.ssMain].toLowerCase();
          var sub = tmpCategory[category.ssSub].toLowerCase();
          createCategory[valueMainCategory][valueSubCategory1][valueSubCategory2] = [ main, sub ];
        }

      }

      next(null, createCategory);
    },
    function writeToFile (data, next) {
      fs.writeFile('exportCategory.json', JSON.stringify(data), function (err) {
        if (err) throw err;
        log.debug('It\'s saved!');
        next();
      });
    }
  ],
  function done (error) {
    callback(error);
  });
};

ExportFileService.prototype.categoryToJson = function(input, callback) {
	callback = callback || function () {};

	var pathFile = input.path || '';
	var csvManager = this.csvManager;

	var category = {
		main: 'Zalora Main Cat',
		sub1: 'Zalora SubCat2',
		sub2: 'Zalora SubCat3',
		ssMain: 'ShopSpot MainCat',
		ssSub: 'ShopSpot SubCat'
	};

	async.waterfall([
		function validate (next) {
			if (!pathFile) {
				next(new ErrorObject('Path not found',
													ErrorDomain.DOMAIN_PARSER,
													ErrorCode.PARSER_ERROR));
			}
			else {
				next();
			}
		},
		function readCategoryFromCsv (next) {
			csvManager.readFromPath(pathFile, function (error, data) {
				next(error, data);
			});
		},
		function createCategory (categories, next) {
			var createCategory = {};

			for (var key in categories) {
				var tmpCategory = categories[key];
				var valueMainCategory = tmpCategory[category.main].toLowerCase();
				var valueSubCategory1 = tmpCategory[category.sub1].toLowerCase();
				var valueSubCategory2 = tmpCategory[category.sub2].toLowerCase();
				
				//console.log(valueMainCategory+ ', '+valueSubCategory1+', '+valueSubCategory2);
				if (!createCategory[valueMainCategory]) {
					createCategory[valueMainCategory] = {}
				}
				
				if (createCategory[valueMainCategory] && !createCategory[valueMainCategory][valueSubCategory1]) {
					createCategory[valueMainCategory][valueSubCategory1] = {};
				}

				if (createCategory[valueMainCategory] && createCategory[valueMainCategory][valueSubCategory1] 
					&& !createCategory[valueMainCategory][valueSubCategory1][valueSubCategory2]) {
					var main = tmpCategory[category.ssMain].toLowerCase();
					var sub = tmpCategory[category.ssSub].toLowerCase();
					createCategory[valueMainCategory][valueSubCategory1][valueSubCategory2] = [ main, sub ];
				}

			}

			next(null, createCategory);
		},
		function writeToFile (data, next) {
			fs.writeFile('exportCategory.json', JSON.stringify(data), function (err) {
			  if (err) throw err;
			  log.debug('It\'s saved!');
			  next();
			});
		}
	],
	function done (error) {
		callback(error);
	});
};

module.exports = ExportFileService;