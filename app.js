var path = require('path');

var StickyRice = require('stickyrice').StickyRice;

var stickyRice = new StickyRice(path.join(__dirname, 'server'), path.join(__dirname, 'config.json'));
stickyRice.run();