var util = require('util'),
    should = require('should');

var CentralScraper = require('../../server/scrapers/CentralScraper');

var test;

describe('FunctionalTestCentralScraper', function () {
  before(function (done) {
    test = {
      scraper: new CentralScraper()
    };

    done();
  });

  describe('#getTotalPage', function () {
    it('should success', function (done) {
      var scraper = test.scraper;

      var uri = 'http://www.central.co.th/d/home-home-care-electronic';

      scraper.getTotalPage(uri, function (error, page) {
        page.should.be.equal(1);

        done();
      });
    });
  });
});