var should = require('should'),
		sinon = require('sinon'),
		util = require('util'),
		path = require('path'),
		fs = require('fs');

var WearYouWantParser = require('../../server/parsers/WearYouWantParser');
var test = null;
var pathFixture = path.join(__dirname, 'MockCrawlDataFixtures.json');
var pathCategories = path.join(__dirname, '..', '..', 'server', 'resources', 'categories', 'wearyouwant.json');

describe('FunctionalTestWearYouWantParser', function () {
	before(function (done) {

		var fixtures = JSON.parse(fs.readFileSync(pathFixture));
		var categories = {
      wearyouwant: JSON.parse(fs.readFileSync(pathCategories))
    };
		
		test = {
			fixtures: fixtures.wearyouwant,
      parser: new WearYouWantParser(categories)
		};

		done();
	});

	describe('#mapCategory', function () {
		it('should success', function () {
			var stuff = {
				mainCategory: 'men',
				subCategory: 'shoes'
			};

			var categories = test.parser.mapCategory(stuff);

			categories[0].should.equal('men_fashion');
			categories[1].should.equal('men_shoes');
		});
	});

	describe('#parse', function () {
		it('should success', function () {
			var crawlStuffs = test.fixtures;
			var stuffs = test.parser.parse(crawlStuffs);

			stuffs.length.should.be.above(0);
			for (var key in stuffs) {	
				var stuff = stuffs[key];
				stuff.should.have.property('path1');
				stuff.should.have.property('path2');
				stuff.should.have.property('path3');
				stuff.should.have.property('link');
				stuff.should.have.property('name');
				stuff.should.have.property('description');
				stuff.should.have.property('price');
				stuff.should.have.property('salePrice');
				stuff.should.have.property('discount');
				stuff.should.have.property('mainCategory');
				stuff.should.have.property('subCategory');
				stuff.should.have.property('tags');

				var tranform = stuff.tranform;

				tranform.should.have.property('images');
				tranform.should.have.property('externalLink');
				tranform.should.have.property('price');
				tranform.should.have.property('tags');
				tranform.should.have.property('description');
				tranform.should.have.property('market');
				tranform.should.have.property('owner');
				tranform.should.have.property('categories');
			}
		});
	});
});