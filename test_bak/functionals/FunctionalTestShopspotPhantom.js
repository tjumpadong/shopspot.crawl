var should = require('should'),
    sinon = require('sinon'),
    util = require('util');

var ShopspotPhantom = require('../../server/modules/ShopspotPhantom');
var test = {};

describe('FunctionalTestShopspotPhantom', function () {
  before(function (done) {
    //var url = 'http://www.asos.com/women/blazers/cat/pgecategory.aspx?cid=11896&via=top#parentID=-1&pge=1&pgeSize=60&sort=-1';
    var url = 'http://www.google.co.th';
    test.shopspotPhantom = new ShopspotPhantom(url);

    done();
  });

  describe('#getPage', function () {
    it('should success', function (done) {
      this.timeout(10000);
      test.shopspotPhantom.getPage(function (error, data) {
        //console.log(data)
        console.log(util.inspect(error))
        should.not.exist(error);
        done();
      });
    });
  });
});