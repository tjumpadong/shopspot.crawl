var should = require('should'),
    request = require('request'),
    async = require('async');

var MockDatabase = require('./MockDatabase');
var MockRequest = require('./MockRequest');

var mockRequest = new MockRequest();

var rootUrl = 'http://127.0.0.1:9002/crawl';

describe('FunctionalTestScraper', function () {
  before(function (done) {
    var mockDatabase = new MockDatabase();
    var database = mockDatabase.setup();

    async.waterfall([
      function testServer (next) {
        request.get(rootUrl, function(error, response, data) {
          next()
        })
      },
      function openDatabase (next) {
        database.open(function (error, openedClient) {
          next(null, openedClient);
        });
      },
      function initDatabase (openedClient, next) {
        mockDatabase.init(openedClient, function (error) {
          next(error);
        }); 
      }
    ],
    function (error) {
      if (error) {
        console.log('Can not setup : '+ error);
      }
      else {
        console.log('FunctionalTest setup completed');
        done();
      }
    });
  });

  /*describe('#ScrapingFromFile', function () {
    it('should success', function (done) {
      this.timeout(1000000);
      var url = rootUrl + '/scrape/file';
      var options = {
        query: {
          // site: 'central'
          site: 'index'
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          if (!data.action) {
            console.log(util.inspect(data));
          }

          data.action.should.be.true;

          done();
        }
      ]);
    });
  });

  describe('#ScrapingXML', function () {
    it('should success', function (done) {
      this.timeout(10000000);
      var url = rootUrl + '/scrape/xml';
      var options = {
        query: {
          uri: 'http://zalorasea.go2feeds.org/feed.php?subdomain=zalorasea&aff_id=1079&offer_id=14&file_id=3579&feed=aHR0cDovL3d3dy5zZW10cmFjay5kZS9l&i=641fc0efc841a187c79b3ba7ae170adc655aec17',
          site: 'zalora',
          fileName: 'zalora'
        }
      };

      async.waterfall([
        function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
        },
        function testcase (data) {
          if (!data.action) {
            console.log(util.inspect(data));
          }

          data.action.should.be.true;

          done();
        }
      ]);
    })
  })*/
})