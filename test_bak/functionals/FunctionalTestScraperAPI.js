var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');

describe('FunctionalTestScraperAPI', function () {
	before(function (done) {
		
		helper.setupDatabase(function (error) {
			if (error) {
				log.error('setup database error');
			}
			else {
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#fromXML', function () {
		var url = rootUrl + '/scrape/fromxml';
		var options = {
      data: {
        uri: 'http://zalorasea.go2feeds.org/feed.php?subdomain=zalorasea&aff_id=1079&offer_id=14&file_id=3579&feed=aHR0cDovL3d3dy5zZW10cmFjay5kZS9l&i=641fc0efc841a187c79b3ba7ae170adc655aec17',
        site: 'zalora'
      }
    };

    async.waterfall([
			function callService (next) {
        mockRequest.post(url, options, function (response, data) {
          next(null, data);
        });
			},
			function testcase (data) {
				data.action.should.be.true;

				done();
			}
		]);
	});
});