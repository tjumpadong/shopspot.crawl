var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');

describe('FunctionalTestCrawlDataAPI', function () {
	before(function (done) {
		
		helper.setupDatabase(function (error) {
			if (error) {
				log.error('setup database error');
			}
			else {
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#findFromGroupId', function () {
		it('should success', function (done) {
			var groupId = '4f48e47c8c47fd6507000011';
			var url = rootUrl + '/find/group/' + groupId;

      var options = {
        query: {
          page: 0,
          limit: 4
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlDatas = data.output.crawlDatas;

					data.output.crawlDataCount.should.equal(3);
					crawlDatas.length.should.be.within(0,3);
					for (var key in crawlDatas) {
						var crawlData = crawlDatas[key];
						crawlData.groupId.should.equal(groupId);
					}
					done();
				}
			]);
		});
	});

	describe('#findFromUserId', function () {
		it('should success', function (done) {
			var userId = '4f48e3f18c47fd650700000a';
			var url = rootUrl + '/find/' + userId;

      var options = {
        query: {
          page: 0,
          limit: 4
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlDatas = data.output.crawlDatas;

					data.output.crawlDataCount.should.equal(4);
					crawlDatas.length.should.be.within(0,4);
					for (var key in crawlDatas) {
						var crawlData = crawlDatas[key];
						crawlData.userId.should.equal(userId);
					}
					done();
				}
			]);
		});
	});

	describe('#get', function () {
		it('should success', function (done) {
			var crawlDataId = '501235973b59a3123e000010';
			var url = rootUrl + '/data/' + crawlDataId;

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, {}, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlData = data.output;

					done();
				}
			]);
		});
	});

	describe('#update', function () {
		it('should success', function (done) {
			var crawlDataId = '501235973b59a3123e000010';
			var url = rootUrl + '/data/' + crawlDataId;

      var options = {
        data: {
          categories: ['men', 'men_other'],
          description: 'description',
          externalLink: 'link',
          images: ['path1', 'path2', 'path3'],
          market: 'TH',
          owner: '4f48e3f18c47fd650700000a',
          price: '100',
          tags: 'tags'
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.put(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlData = data.output;

					done();
				}
			]);
		});
	});

	describe('#removeDatas', function () {
		it('should success', function (done) {
			var url = rootUrl + '/data';

      var options = {
        query: {
          crawlDataIds: ['501235973b59a3123e000014']
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.del(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlData = data.output;

					done();
				}
			]);
		});
	});
	
	describe('#removeFromGroupId', function () {
		it('should success', function (done) {
			var url = rootUrl + '/data/fromGroupId';

      var options = {
        query: {
          groupId: '4f48e47c8c47fd6507000012'
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.del(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlData = data.output;

					done();
				}
			]);
		});
	});

});