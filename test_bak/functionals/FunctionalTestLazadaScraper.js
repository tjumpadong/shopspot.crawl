var util = require('util'),
    should = require('should');

var LazadaScraper = require('../../server/scrapers/LazadaScraper');

var test;

describe('FunctionalTestLazadaScraper', function () {
  before(function (done) {
    test = {
      scraper: new LazadaScraper()
    };

    done();
  });

  describe('#getDetail', function () {
    it('should success', function (done) {
      var scraper = test.scraper;
      var fileName = 'lazada';

      scraper.getDetail(fileName, function () {
        done();
      });
    });
  });
});