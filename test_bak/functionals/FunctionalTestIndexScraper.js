var util = require('util'),
    should = require('should');

var IndexScraper = require('../../server/scrapers/IndexScraper');

var test;

describe('FunctionalTestIndexScraper', function () {
  before(function (done) {
    test = {
      scraper: new IndexScraper()
    };

    done();
  });

  describe('#getTotalPage', function () {
    it('should success', function (done) {
      this.timeout(5000);
      var scraper = test.scraper;

      var uri = 'http://www.indexlivingmall.com/product/list.php?L1=52&L2=53&L3=54';

      scraper.getTotalPage(uri, function (error, page) {
        console.log(util.inspect(page))
        page.should.be.equal(2);

        done();
      });
    });
  });
});