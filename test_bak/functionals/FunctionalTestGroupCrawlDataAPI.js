var should = require('should'),
		util = require('util'),
    fs = require('fs'),
		async = require('async'),
		path = require('path');

var helper = require('../Util').helper;
var mockRequest = helper.mockRequest;

var rootUrl = helper.config.host;
var log = require('winston');

describe('FunctionalTestGroupCrawlDataAPI', function () {
	before(function (done) {
		
		helper.setupDatabase(function (error) {
			if (error) {
				log.error('setup database error');
			}
			else {
				log.info('setup database success');
				done();
			}
		});
	});

	describe('#findFromUserId', function () {
		it('should success', function (done) {
			var userId = '4f48e3f18c47fd650700000a';
			var url = rootUrl + '/group/find/'+userId;

      var options = {
        query: {
          page: 0,
          limit: 4
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var groups = data.output.groups;
					
					groups.length.should.be.within(0,4);
					for (var key in groups) {
						var group = groups[key];
						group.userId.should.equal(userId);
					}
					done();
				}
			]);
		});
	});

	describe('#removeFromGroupId', function () {
		it('should success', function (done) {
			var url = rootUrl + '/data/fromGroupId';

      var options = {
        query: {
          groupId: '4f48e47c8c47fd6507000012'
        }
      };

			async.waterfall([
				function callService (next) {
          mockRequest.del(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					var crawlData = data.output;

					done();
				}
			]);
		});
	});
	
	describe('#countItem', function () {
		it('should success', function (done) {
			var url = rootUrl + '/group/countItem';

      var options = {};

			async.waterfall([
				function callService (next) {
          mockRequest.get(url, options, function (response, data) {
            next(null, data);
          });
				},
				function testcase (data) {
					data.action.should.be.true;
					data.output.should.equal(6);

					done();
				}
			]);
		});
	});

});