var fs = require('fs'),
		path = require('path'),
		util = require('util');

var MockDatabase = require('shopspot.testutil').MockDatabase;
var MockRequest = require('shopspot.testutil').MockRequest;

var config = JSON.parse(fs.readFileSync(path.join(__dirname, 'MockConfig.json')));
var fixturePath = path.join(__dirname, 'fixture.json');

var Helper = function () {
	this.config = config;
	this.mockRequest = new MockRequest();
};

Helper.prototype.setupDatabase = function (callback) {
	
	var mockDatabase = new MockDatabase(this.config.database, fixturePath);

	mockDatabase.setupWithData(callback);
};

exports.helper = new Helper();

